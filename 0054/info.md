=Spiral Matrix=

Given an `m x n` matrix, return all elements of the matrix in spiral order.

**Example 1:**<br>
*Input:* `matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]`<br>
*Output:* `[1, 2, 3, 6, 9, 8, 7, 4, 5]`<br>

**Example 2:**<br>
*Input:* `matrix = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]`<br>
*Output:* `[1, 2, 3, 4, 8, 12, 11, 10, 9, 5, 6, 7]`<br>
 
**Constraints:**<br>
- `m == matrix.length`
- `n == matrix[i].length`
- `1 <= m, n <= 10`
- `-100 <= matrix[i][j] <= 100`



#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

std::vector<int> spiralOrder(std::vector<std::vector<int>> matrix)
{
    int n = matrix.size() - 1;
    int m = matrix[0].size();
    std::vector<int> result;
    result.reserve(m * n);
    int sign = 1;
    for (int x = 0, y = -1; (m > 0) && (n >= 0); --m, --n)
    {
        y += sign;
        for (int i = 0; i < m; ++i, y += sign)
            result.push_back(matrix[x][y]);
        y -= sign;
        
        x += sign;
        for (int i = 0; i < n; ++i, x += sign)
            result.push_back(matrix[x][y]);
        x -= sign;
        
        sign = -sign;
    }
    return result;
}

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    for (int i = 0; i < n; ++i)
        if (left[i] != right[i]) return false;
    return true;
}

std::ostream& operator<<(std::ostream &out, const std::vector<int> &vec)
{
    out << '{';
    bool next = false;
    for (int v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

void test(std::vector<std::vector<int> > matrix, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = spiralOrder(matrix);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2, 3},
          {4, 5, 6},
          {7, 8, 9}}, {1, 2, 3, 6, 9, 8, 7, 4, 5}, trials);
    test({{ 1,  2,  3,  4},
          { 5,  6,  7,  8},
          { 9, 10, 11, 12}}, {1, 2, 3, 4, 8, 12, 11, 10, 9, 5, 6, 7}, trials);
    test({{ 1,  2,  3,  4},
          { 5,  6,  7,  8},
          { 9, 10, 11, 12},
          {13, 14, 15, 16}}, {1, 2, 3, 4, 8, 12, 16, 15, 14, 13, 9, 5, 6, 7, 11, 10}, trials);
    test({{ 1,  2,  3,  4,  5},
          { 6,  7,  8,  9, 10},
          {11, 12, 13, 14, 15},
          {16, 17, 18, 19, 20},
          {21, 22, 23, 24, 25}}, {1, 2, 3, 4, 5, 10, 15, 20, 25, 24, 23, 22, 21, 16, 11, 6, 7, 8, 9, 14, 19, 18, 17, 12, 13}, trials);
    test({{ 1,  2,  3,  4,  5}}, {1, 2, 3, 4, 5}, trials);
    test({{1}, {2}, {3}, {4}, {5}}, {1, 2, 3, 4, 5}, trials);
    test({{ 1,  2,  3,  4,  5},
          { 6,  7,  8,  9, 10}}, {1, 2, 3, 4, 5, 10, 9, 8, 7, 6}, trials);
    test({{1,  2},
          {3,  4},
          {5,  6},
          {7,  8},
          {9, 10}}, {1, 2, 4, 6, 8, 10, 9, 7, 5, 3}, trials);
    return 0;
}

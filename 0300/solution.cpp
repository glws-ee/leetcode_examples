#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <map>
#include <limits>

using namespace std;

int lengthOfLIS(std::vector<int> nums)
{
    const int n = nums.size();
    int buffer[n];
    int size = 1;
    buffer[0] = nums[0];
    for(int i = 1; i < n; ++i)
    {
        auto pos = std::lower_bound(buffer, buffer + size, nums[i]) - buffer;
        if (pos >= size) buffer[size++] = nums[i];
        else buffer[pos] = nums[i];
        
    }
    return size;
}

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = lengthOfLIS(nums);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 9, 2, 5, 3, 7, 101, 18}, 4, trials);

    //test({10, 9, 2, 5, 3, 7, 101, 18}, 4, trials);
    // 10->101
    //  +->18
    // 9->101
    // +->18
    // 2->3->7->101
    // |     +->18
    // +->5->101
    //    +->18
    // 10
    // 9
    // 2->3
    // 5
    // 3
    test({0, 1, 0, 3, 2, 3}, 4, trials);
    test({7, 7, 7, 7, 7, 7, 7}, 1, trials);
    test({101, 102, 103, 104, 105, 1, 2, 3, 4, 5, 6, 7}, 7, trials);
    return 0;
}

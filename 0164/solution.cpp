#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <limits>

using namespace std;

int maximumGap(std::vector<int> &nums)
{
    if (nums.size() < 2) return 0;
    if (nums.size() == 2) return std::abs(nums[0] - nums[1]);
    const int n = nums.size();
    int min_value = nums[0], max_value = nums[0];
    for (auto v : nums)
    {
        if (v < min_value) min_value = v;
        if (v > max_value) max_value = v;
    }
    int histogram[n], histogram_min[n], histogram_max[n];
    int maximum_gap;
    for (int range = max_value - min_value + 1, step = 2; step > 1;)
    {
        for (int k = 0; k < n; ++k)
        {
            histogram[k] = histogram_max[k] = 0;
            histogram_min[k] = std::numeric_limits<int>::max();
        }
        int prev_step = step;
        step = range / n + ((range % n) != 0);
        if (step == prev_step) break;
        //std::cout << range << ' ' << step << '\n';
        for (auto v : nums)
        {
            if ((v >= min_value) && (v <= max_value))
            {
                int bin = (v - min_value) / step;
                if (v > histogram_max[bin]) histogram_max[bin] = v;
                if (v < histogram_min[bin]) histogram_min[bin] = v;
                ++histogram[bin];
            }
        }
        maximum_gap = 0;
        for (int k = 0, previous = -1; k < n; ++k)
        {
            if (histogram[k] > 0)
            {
                maximum_gap = std::max(maximum_gap, histogram_max[k] - histogram_min[k]);
                if (previous != -1)
                    maximum_gap = std::max(maximum_gap, histogram_min[k] - previous);
                previous = histogram_max[k];
            }
        }
        min_value = max_value = -1;
        for (int k = 0, previous = -1; k < n; ++k)
        {
            if (histogram[k] > 0)
            {
                int gap = histogram_max[k] - histogram_min[k];
                if (gap == maximum_gap)
                {
                    if (min_value = -1)
                        min_value = histogram_min[k];
                    max_value = histogram_max[k];
                }
                if (previous != -1)
                {
                    gap = histogram_min[k] - previous;
                    if (gap == maximum_gap)
                    {
                        if (min_value = -1)
                            min_value = previous;
                        max_value = histogram_min[k];
                    }
                }
                previous = histogram_max[k];
            }
        }
        range = max_value - min_value + 1;
        //std::cout << maximum_gap << ' ' << min_value << ' ' << max_value << '\n';
        //for (int k = 0; k < n; ++k)
        //    std::cout << histogram[k] << ':' << histogram_min[k] << ':' << histogram_max[k] << ' ';
        //std::cout << '\n';
    }
    return maximum_gap;
}

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumGap(nums);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 6, 9, 1}, 3, trials);
    test({10}, 0, trials);
    return 0;
}

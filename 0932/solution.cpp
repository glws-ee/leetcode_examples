#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

std::vector<int> beautifulArray(int n)
{
    std::vector<int> result[2];
    result[0].resize(n);
    result[1].resize(n);
    result[0][0] = 1;
    bool selected = false;
    for (int s = 1; s < n; selected = !selected)
    {
        int o = 0;
        for (int i = 0; i < s; ++i)
            if (int val = result[selected][i] * 2 - 1; val <= n)
                result[!selected][o++] = val;
        for (int i = 0; i < s; ++i)
            if (int val = result[selected][i] * 2; val <= n)
                result[!selected][o++] = val;
        s = o;
    }
    return result[selected];
}

std::ostream& operator<<(std::ostream &out, const std::vector<int> &vec)
{
    out << '{';
    bool next = false;
    for (int v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    for (int i = 0; i < n; ++i)
        if (left[i] != right[i]) return false;
    return true;
}

void test(int n, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = beautifulArray(n);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {2, 1, 4, 3}, trials);
    test(5, {3, 1, 2, 5, 4}, trials);
    return 0;
}

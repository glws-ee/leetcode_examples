#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <unordered_map>

using namespace std;

constexpr int null = -1000;

std::vector<int> sortArrayByParityII(std::vector<int> nums)
{
    const int n = nums.size();
    for (int even = 0, odd = 1; true;)
    {
        while ((even < n) && (nums[even] % 2 == 0)) even += 2;
        while ((odd < n) && (nums[odd] % 2 == 1)) odd += 2;
        if ((even < n) && (odd < n))
            std::swap(nums[even], nums[odd]);
        else break;
    }
    return nums;
}

bool checkOrder(const std::vector<int> &nums)
{
    bool even = true;
    for (int n : nums)
    {
        if (even != (n % 2 == 0)) return false;
        even = !even;
    }
    return true;
}

std::ostream& operator<<(std::ostream &out, const std::vector<int> &nums)
{
    out << '{';
    bool next = false;
    for (int n : nums)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << n;
    }
    out << '}';
    return out;
}

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    std::unordered_map<int, int> histogram;
    for (int l : left)
        histogram[l]++;
    for (int r : right)
    {
        if (auto it = histogram.find(r); it != histogram.end())
        {
            if (!it->second) return false;
            --it->second;
            if (!it->second) histogram.erase(it);
        }
        else return false;
    }
    return !histogram.size() && checkOrder(right);
}

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = sortArrayByParityII(nums);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 2, 5, 7}, {4, 5, 2, 7}, trials);
    test({2, 3}, {2, 3}, trials);
    return 0;
}

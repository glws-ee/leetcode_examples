#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

std::string reverseOnlyLetters(std::string s)
{
    auto valid = [](char c)
    {
        return ((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z'));
    };
    for (int i = 0, j = s.size() - 1; i < j;)
    {
        bool vi = valid(s[i]);
        bool vj = valid(s[j]);
        if (vi && vj) std::swap(s[i++], s[j--]);
        else if (vi) --j;
        else ++i;
    }
    return s;
}

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = reverseOnlyLetters(s);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("ab-cd", "dc-ba", trials);
    test("a-bC-dEf-ghIj", "j-Ih-gfE-dCba", trials);
    test("Test1ng-Leet=code-Q!", "Qedo1ct-eeLg=ntse-T!", trials);
    return 0;
}

#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <stack>

using namespace std;

int evalRPN(std::vector<std::string>& tokens)
{
#if 1
    std::stack<int> values;
    for (const auto &t : tokens)
    {
        if (t.size() == 1 && ((t[0] == '+') || (t[0] == '-') || (t[0] == '*') || (t[0] == '/')))
        {
            int vr = values.top();
            values.pop();
            int vl = values.top();
            values.pop();
            switch (t[0])
            {
            case '+':
                vl += vr;
                break;
            case '-':
                vl -= vr;
                break;
            case '*':
                vl *= vr;
                break;
            case '/':
                vl /= vr;
                break;
            }
            values.push(vl);
        }
        else values.push(std::stoi(t));
    }
    return values.top();
#else
    std::stack<int> values;
    for (const auto &t : tokens)
    {
        if      (t == "+")
        {
            int r = values.top();
            values.pop();
            r = values.top() + r;
            values.pop();
            values.push(r);
        }
        else if (t == "-")
        {
            int r = values.top();
            values.pop();
            r = values.top() - r;
            values.pop();
            values.push(r);
        }
        else if (t == "*")
        {
            int r = values.top();
            values.pop();
            r = values.top() * r;
            values.pop();
            values.push(r);
        }
        else if (t == "/")
        {
            int r = values.top();
            values.pop();
            r = values.top() / r;
            values.pop();
            values.push(r);
        }
        else values.push(std::stoi(t));
    }
    return values.top();
#endif
}

void test(std::vector<std::string> tokens, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = evalRPN(tokens);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"2", "1", "+", "3", "*"}, 9, trials);
    test({"4", "13", "5", "/", "+"}, 6, trials);
    test({"10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"}, 22, trials);
    return 0;
}

#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

struct ListNode
{
    int val = 0;
    ListNode * next = nullptr;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
    ~ListNode(void) { delete next; }
};

ListNode * addTwoNumbers(ListNode * l1, ListNode * l2)
{
    const ListNode * p1 = l1, * p2 = l2;
    int remainder = 0;
    ListNode * result = new ListNode();
    ListNode * current = result;
    for (; (p1 != nullptr) && (p2 != nullptr); p1 = p1->next, p2 = p2->next)
    {
        int sum = p1->val + p2->val + remainder;
        remainder = sum / 10;
        current = current->next = new ListNode(sum - remainder * 10);
    }
    for (; p1 != nullptr; p1 = p1->next)
    {
        int sum = p1->val + remainder;
        remainder = sum / 10;
        current = current->next = new ListNode(sum - remainder * 10);
    }
    for (; p2 != nullptr; p2 = p2->next)
    {
        int sum = p2->val + remainder;
        remainder = sum / 10;
        current = current->next = new ListNode(sum - remainder * 10);
    }
    if (remainder)
        current->next = new ListNode(remainder);
    current = result;
    result = result->next;
    current->next = nullptr;
    delete current;
    return result;
}

ListNode * vec2list(const std::vector<int> &vec)
{
    if (vec.size() == 0) return nullptr;
    else
    {
        const int n = vec.size();
        ListNode * l = new ListNode(vec[0]);
        ListNode * current = l;
        for (int i = 1; i < n; ++i)
            current = current->next = new ListNode(vec[i]);
        return l;
    }
}

std::vector<int> list2vec(const ListNode * l)
{
    int n = 0;
    for (const ListNode * current = l; current != nullptr; ++n, current = current->next);
    std::vector<int> vec(n);
    n = 0;
    for (const ListNode * current = l; current != nullptr; ++n, current = current->next)
        vec[n] = current->val;
    return vec;
}

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    for (int i = 0; i < n; ++i)
        if (left[i] != right[i]) return false;
    return true;
}

std::ostream& operator<<(std::ostream &out, const std::vector<int> &vec)
{
    out << '{';
    bool next = false;
    for (int v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

void test(std::vector<int> v1, std::vector<int> v2, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * l1 = vec2list(v1);
        ListNode * l2 = vec2list(v2);
        ListNode * lr = addTwoNumbers(l1, l2);
        result = list2vec(lr);
        delete l1;
        delete l2;
        delete lr;
    }
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 4, 3}, {5, 6, 4}, {7, 0, 8}, trials);
    test({0}, {0}, {0}, trials);
    test({9, 9, 9, 9, 9, 9, 9}, {9, 9, 9, 9}, {8, 9, 9, 9, 0, 0, 0, 1}, trials);
    return 0;
}

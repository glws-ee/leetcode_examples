#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <set>
#include <utility>

using namespace std;

int maxSumSubmatrix(std::vector<std::vector<int>> matrix, int k)
{
#if 1
    int n = matrix.size();
    int m = matrix[0].size();
    int result = std::numeric_limits<int>::lowest();
    int sum[m];
    for (int i = 0; i < n; ++i)
    {
        std::memset(sum, 0, sizeof(sum));
        for (int j = i; j < n; ++j)
        {
            int max_val = std::numeric_limits<int>::lowest();
            for (int cur = 0, p = 0; p < m; ++p)
            {
                cur += sum[p] += matrix[j][p];
                max_val = std::max(max_val, cur);
                cur = std::max(cur, 0);
            }
            if (max_val == k) return k;
            if (max_val > k)
            {
                std::set<int> cumset{0};
                for (int p = 0, cur = 0; p < m; ++p)
                {
                    cur += sum[p];
                    auto it = cumset.lower_bound(cur - k);
                    if (it != cumset.end())
                    {
                        result = std::max(result, cur - *it);
                        if (result == k) return k;
                    }
                    cumset.insert(cur);
                }
            }
            else result = std::max(result, max_val);
        }
    }
    return result;
#else
    const int n = matrix.size();
    const int m = matrix[0].size();
    int accum[n][m + 1];
    for (int i = 0; i < n; ++i)
    {
        accum[i][0] = 0;
        accum[i][1] = matrix[i][0];
        for (int j = 1; j < m; ++j)
            accum[i][j + 1] = matrix[i][j] + accum[i][j];
    }
    int result = std::numeric_limits<int>::lowest();
    for (int i = 1; i <= m; ++i)
    {
        for (int j = i; j <= m; ++j)
        {
            std::set<int> cumset{0};
            for (int p = 0, cum = 0; p < n; ++p)
            {
                cum += accum[p][j] - accum[p][i - 1];
                auto it = cumset.lower_bound(cum - k);
                if (it != cumset.end())
                {
                    result = std::max(result, cum - *it);
                    if (result == k) return k;
                }
                cumset.insert(cum);
            }
        }
    }
    return result;
#endif
}

void test(std::vector<std::vector<int> > matrix, int k, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxSumSubmatrix(matrix, k);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 0, 1}, {0, -2, 3}}, 2, 2, trials);
    test({{0, 1}, {-2, 3}}, 3, 3, trials);
    return 0;
}

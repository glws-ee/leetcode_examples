#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

std::string addStrings(std::string num1, std::string num2)
{
    std::vector<char> addition;
    addition.reserve(std::max(num1.size(), num2.size()) + 2);
    bool carry = false;
    int idx1, idx2;
    for (idx1 = num1.size() - 1, idx2 = num2.size() - 1; idx1 >= 0 && idx2 >= 0; --idx1, --idx2)
    {
        char sum = (num1[idx1] - '0') + (num2[idx2] - '0') + carry;
        carry = sum > 9;
        if (carry) sum -= 10;
        sum += '0';
        addition.push_back(sum);
    }
    for (; idx1 >= 0; --idx1)
    {
        char sum = (num1[idx1] - '0') + carry;
        carry = sum > 9;
        if (carry) sum -= 10;
        sum += '0';
        addition.push_back(sum);
    }
    for (; idx2 >= 0; --idx2)
    {
        char sum = (num2[idx2] - '0') + carry;
        carry = sum > 9;
        if (carry) sum -= 10;
        sum += '0';
        addition.push_back(sum);
    }
    if (carry) addition.push_back('1');
    std::reverse(addition.begin(), addition.end());
    return std::string(addition.begin(), addition.end());
}

void test(std::string num1, std::string num2, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = addStrings(num1, num2);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("123", "11", "134", trials);
    test("11", "123", "134", trials);
    test("456", "77", "533", trials);
    test("77", "456", "533", trials);
    test("9", "9", "18", trials);
    test("9999", "1", "10000", trials);
    test("1", "9999", "10000", trials);
    test("0", "0", "0", trials);
    return 0;
}

#include <vector>
#include <iostream>

using namespace std;

int maxProfitAssignment(vector<int>& difficulty, vector<int>& profit, vector<int>& worker)
{
#if 1
    constexpr int max_difficulty = 100001;
    int histogram[max_difficulty]{};
    const int n = difficulty.size();
    for (int i = 0; i < n; ++i)
        histogram[difficulty[i]] = std::max(histogram[difficulty[i]], profit[i]);
    for (int i = 1; i < max_difficulty; ++i)
        histogram[i] = std::max(histogram[i], histogram[i - 1]);
    int result = 0;
    for (auto &w : worker)
        result += histogram[w];
    return result;
#else
    std::multimap<int, int> dp;
    for (int i = 0; i < difficulty.size(); ++i)
        dp.insert({difficulty[i], profit[i]});
    std::vector<int> worker_copy(worker);
    std::sort(worker_copy.begin(), worker_copy.end());
    int result = 0;
    auto it = dp.begin();
    int best_profit = 0;
    for (auto w : worker_copy)
    {
        for (; (it != dp.end()) && (w >= it->first); ++it)
            best_profit = std::max(best_profit, it->second);
        result += best_profit;
    }
    return result;
#endif
}

void test(std::vector<int> difficulty, vector<int> profit, vector<int> worker, int solution)
{
    int result = maxProfitAssignment(difficulty, profit, worker);
    if (result == solution) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
    test({2, 4, 6, 8, 10}, {10, 20, 30, 40, 50}, {4, 5, 6, 7}, 100);
    return 0;
}


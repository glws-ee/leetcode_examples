#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

std::string toLowerCase(std::string s)
{
    for (auto &c : s)
        if ((c >= 'A') && (c <= 'Z'))
            c = c - 'A' + 'a';
    return s;
}

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = toLowerCase(s);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("Hello", "hello", trials);
    test("here", "here", trials);
    test("LOVELY", "lovely", trials);
    return 0;
}

#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <algorithm>

using namespace std;

void solveNQueens(int row,
                  int n,
                  int n2,
                  char * grid,
                  std::vector<std::vector<std::string> > &results)
{
    if (row == n) // Done.
    {
        results.push_back({});
        for (int i = 0; i < n; ++i)
        {
            std::string current_row(n, '.');
            //std::cout << i << '\n';
            for (int j = 0; j < n; ++j, ++grid)
            {
                //std::cout << i << ' ' << j << ' ' << static_cast<int>(*grid) << '\n';
                if (*grid == 2)
                {
                    //std::cout << "hola\n";
                    current_row[j] = 'Q';
                }
            }
            results.back().push_back(current_row);
        }
    }
    else
    {
        char * grid_row = grid + n * row;
        for (int c = 0; c < n; ++c)
        {
            if (grid_row[c] == 1)
            {
                char * dst = grid + n2;
                std::memcpy(dst, grid, n2);
                for (int i = row + 1, j = 1; i < n; ++i, ++j)
                {
                    dst[n * i + c] = 0; // Mark the current column as occupied.
                    if (c - j >= 0)
                        dst[n * i + c - j] = 0;
                    if (c + j <  n)
                        dst[n * i + c + j] = 0;
                }
                dst[n * row + c] = 2; // Put the queen.
                //std::cout << "==> " << n * row + c << '\n';
                solveNQueens(row + 1, n, n2, dst, results);
            }
        }
    }
}

std::vector<std::vector<std::string> > solveNQueens(int n)
{
    std::vector<std::vector<std::string> > results;
    
    char grid[n * n * (n + 1)];
    std::memset(grid, 1, sizeof(grid));
    solveNQueens(0, n, n * n, grid, results);
    
    return results;
}

std::ostream& operator<<(std::ostream &out, const std::vector<std::string> &object)
{
    out << '{';
    bool rest = false;
    for (const auto &o : object)
    {
        if (rest) out << ", ";
        rest = true;
        out << o;
    }
    out << '}';
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<std::vector<std::string> > &object)
{
    out << '{';
    bool rest = false;
    for (const auto &o : object)
    {
        if (rest) out << ", ";
        rest = true;
        out << o;
    }
    out << '}';
    return out;
}

void test(const int n, const std::vector<std::vector<std::string> > solution, unsigned int trials = 1)
{
    std::cout << "Number of queens: " << n << '\n';
    std::vector<std::vector<std::string> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = solveNQueens(n);
    if (solution.size() == result.size())
    {
        const int m = solution.size();
        std::vector<std::string> solutions_strs;
        std::vector<std::string> results_strs;
        
        for (int i = 0; i < m; ++i)
        {
            std::string current;
            for (int j = 0; j < n; ++j)
                current += solution[i][j];
            solutions_strs.push_back(current);
            current.clear();
            for (int j = 0; j < n; ++j)
                current += result[i][j];
            results_strs.push_back(current);
        }
        std::sort(solutions_strs.begin(), solutions_strs.end());
        std::sort(results_strs.begin(), results_strs.end());
        bool same = true;
        for (int i = 0; same && (i < m); ++i)
            same = solutions_strs[i] == results_strs[i];
        if (same) std::cout << "[SUCCESS] ";
        else std::cout << "[FAILURE] ";
    }
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(4, {{".Q..", "...Q", "Q...", "..Q."}, {"..Q.", "Q...", "...Q", ".Q.."}}, trials);
    test(1, {{"Q"}}, trials);
    return 0;
}

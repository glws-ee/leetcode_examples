The **n-queens** puzzle is the problem of placing `n` queens on an `n x n` chessboard such that no two queens attack each other.

Given an integer `n`, return _all distinct solutions to the_ ***n-queens puzzle***.

Each solution contains a distinct board configuration of the n-queens' placement, where `'Q'` and `'.'` both indicate a queen and an empty space, respectively.

 
**Example 1:**

|:---:|:---:|:---:|:---:|
|     |:crown:|   |     |
|     |      |  |:crown:|
|:crown:|   |     |     |
|     |     |:crown:|   |

*Input:* `n = 4`<br>
*Output:* `[[".Q..", "...Q", "Q...",, "..Q."], ["..Q.", "Q...", "...Q", ".Q.."]]`<br>
*Explanation:* There exist two distinct solutions to the 4-queens puzzle as shown above.<br>

**Example 2:**
*Input:* `n = 1`<br>
*Output:* `[["Q"]]`

**Constraints:**
    - `1 <= n <= 9`


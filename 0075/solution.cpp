#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

void sortColors(vector<int> &nums)
{
#if 1
    int begin = 0, end = nums.size() - 1;
    for (int i = 0; i <= end;)
    {
        if (nums[i] == 0)
        {
            std::swap(nums[begin], nums[i]);
            ++begin;
            ++i;
        }
        else if (nums[i] == 1) ++i;
        else if (nums[i] == 2)
        {
            std::swap(nums[end], nums[i]);
            --end;
        }
    }
#else
    int histogram[3] = {0, 0, 0};
    for (int n : nums)
        ++histogram[n];
    for (int i = 0, j = 0; i < 3; ++i)
        for (int k = 0; k < histogram[i]; ++k, ++j)
            nums[j] = i;
#endif
}

std::ostream& operator<<(std::ostream &out, const std::vector<int> &vec)
{
    out << '{';
    bool next = false;
    for (int v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    for (int i = 0; i < n; ++i)
        if (left[i] != right[i])
            return false;
    return true;
}

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result = nums;
        sortColors(result);
    }
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 0, 2, 1, 1, 0}, {0, 0, 1, 1, 2, 2}, trials);
    test({2, 0, 1}, {0, 1, 2}, trials);
    test({0}, {0}, trials);
    test({1}, {1}, trials);
    test({}, {}, trials);
    return 0;
}

#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <unordered_map>
#include <set>

using namespace std;

constexpr int null = -1000;

std::vector<int> nextGreaterElement(std::vector<int> nums1, std::vector<int> nums2)
{
    const int n = nums1.size();
    std::vector<int> result(n, -1);
    std::set<int> previous;
    std::unordered_map<int, int> lut;
    for (int v : nums2)
    {
        previous.insert(v);
        auto end = previous.lower_bound(v);
        for (auto begin = previous.begin(); begin != end; ++begin)
            lut[*begin] = v;
        previous.erase(previous.begin(), end);
    }
    for (int i = 0; i < n; ++i)
        if (auto it = lut.find(nums1[i]); it != lut.end())
            result[i] = it->second;
    return result;
}

std::ostream& operator<<(std::ostream &out, const std::vector<int> &vec)
{
    out << '{';
    bool next = false;
    for (int v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    for (int i = 0; i < n; ++i)
        if (left[i] != right[i])
            return false;
    return true;
}

void test(std::vector<int> nums1, std::vector<int> nums2, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = nextGreaterElement(nums1, nums2);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 1, 2}, {1, 3, 4, 2}, {-1, 3, -1}, trials);
    test({2, 4}, {1, 2, 3, 4}, {3, -1}, trials);
    return 0;
}

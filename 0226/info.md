=Invert Binary Tree=

Given the root of a binary tree, invert the tree, and return its root.

**Example 1:**<br>
*The input tree...*<br>
```mermaid 
A((4))-->B((2))
A-->C((7))
B-->D((1))
B-->E((3))
C-->F((6))
C-->G((9))
style A fill:#FFF
style B fill:#FFF
style C fill:#BDF
style D fill:#FCA
style E fill:#CAF
style F fill:#FAA
style G fill:#9C9
```
*is converted into the following tree:*<br>
```mermaid 
A((4))-->B((7))
A-->C((2))
B-->D((9))
B-->E((6))
C-->F((3))
C-->G((1))
style A fill:#FFF
style C fill:#FFF
style B fill:#BDF
style G fill:#FCA
style F fill:#CAF
style E fill:#FAA
style D fill:#9C9
```
*Input:* `root = [4, 2, 7, 1, 3, 6, 9]`<br>
*Output:* `[4, 7, 2, 9, 6, 3, 1]`<br>

**Example 2:**<br>
*Input...*<br>
```mermaid 
A((2))-->B((1))
A-->C((3))
style A fill:#FFF
style B fill:#FFF
style C fill:#BDF
```
*...becomes...*<br>
```mermaid 
A((2))-->B((3))
A-->C((1))
style A fill:#FFF
style C fill:#FFF
style B fill:#BDF
```
*Input:* `root = [2, 1, 3]`<br>
*Output:* `[2, 3, 1]`<br>

**Example 3:**<br>
*Input:* `root = []`<br>
*Output:* `[]`<br>
 
**Constraints:**<br>
- The number of nodes in the tree is in the range `[0, 100]`.
- `-100 <= Node.val <= 100`




Given an array of strings `products` and a string `searchWord`. We want to design a system that suggests at most three product names from `products` after each character of `searchWord` is typed. Suggested products should have common prefix with the `searchWord`. If there are more than three products with a common prefix return the three lexicographically minimums products.

Return *list of lists* of the suggested `products` after each character of `searchWord` is typed. 

 

**Example 1:**<br>
*Input:* `products = ["mobile", "mouse", "moneypot", "monitor", "mousepad"], searchWord = "mouse"`<br>
*Output:* `[
["mobile","moneypot","monitor"],
["mobile","moneypot","monitor"],
["mouse","mousepad"],
["mouse","mousepad"],
["mouse","mousepad"]
]`<br>
*Explanation:* products sorted lexicographically = `["mobile", "moneypot", "monitor", "mouse", "mousepad"]`<br>
After typing `m` and `mo` all products match and we show user `["mobile", "moneypot", "monitor"]`<br>
After typing `mou`, `mous` and `mouse` the system suggests `["mouse", "mousepad"]`<br>

**Example 2:**<br>
*Input:* `products = ["havana"], searchWord = "havana"`<br>
*Output:* `[["havana"],["havana"],["havana"],["havana"],["havana"],["havana"]]`<br>

**Example 3:**<br>
*Input:* `products = ["bags","baggage","banner","box","cloths"], searchWord = "bags"`<br>
*Output:* `[["baggage","bags","banner"],["baggage","bags","banner"],["baggage","bags"],["bags"]]`<br>

**Example 4:**<br>
*Input:* `products = ["havana"], searchWord = "tatiana"`<br>
*Output:* `[[],[],[],[],[],[],[]]`<br>

**Constraints:**
    - `1 <= products.length <= 1000`
    - There are no repeated elements in products.
    - `1 <= Σ products[i].length <= 2 * 10^4`
    - All characters of `products[i]` are lower-case English letters.
    - `1 <= searchWord.length <= 1000`
    - All characters of `searchWord` are lower-case English letters.

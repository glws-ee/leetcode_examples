#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <unordered_set>
#include <algorithm>

using namespace std;

#if 1
int openLock(vector<string> &deadends, string target)
{
    bool available[10000];
    std::memset(available, true, sizeof(available));
    for (const auto &s : deadends)
    {
        short value = 0;
        value = (s[0] - '0') * 1000
              + (s[1] - '0') *  100
              + (s[2] - '0') *   10
              + (s[3] - '0') *    1;
        if (value == 0) return -1;
        available[value] = false;
    }
    short target_int = (target[0] - '0') * 1000
                     + (target[1] - '0') *  100
                     + (target[2] - '0') *   10
                     + (target[3] - '0') *    1;
    
    short q[10000];
    q[0] = 0;
    for (short steps = 0, b = 0, e = 1; b != e; ++steps)
    {
        for (short ec = e; b < ec; ++b)
        {
            if (q[b] == target_int) return steps;
            for (short d = 0, product = 1; d < 4; ++d, product *= 10)
            {
                short value_dim = (q[b] / product) % 10;
                short neighbor_base = q[b] - value_dim * product;
                short neighbor;
                if (value_dim == 9) neighbor = neighbor_base;
                else neighbor = neighbor_base + (value_dim + 1) * product;
                if (available[neighbor])
                {
                    available[neighbor] = false;
                    q[e++] = neighbor;
                }
                if (value_dim == 0) neighbor = neighbor_base + 9 * product;
                else neighbor = neighbor_base + (value_dim - 1) * product;
                if (available[neighbor])
                {
                    available[neighbor] = false;
                    q[e++] = neighbor;
                }
            }
        }
    }
    return -1;
}
#else
std::vector<std::string> neighbors(const std::string &code)
{
    std::vector<std::string> result;
    for (int d = 0; d < 4; ++d)
    {
        for (int diff = -1; diff <= 1; diff += 2)
        {
            string neighbor = code;
            neighbor[d] = (neighbor[d] - '0' + diff + 10) % 10 + '0';
            result.push_back(neighbor);
        }
    }
    return result;
}

int openLock(vector<string>& deadends, string target)
{
    std::unordered_set<std::string> deadSet(deadends.begin(), deadends.end());
    if (deadSet.count("0000")) return -1;
    
    std::queue<std::string> q({"0000"});
    for (int steps = 0; !q.empty(); ++steps)
    {
        for (int i = q.size(); i > 0; --i)
        {
            auto current = q.front();
            q.pop();
            if (current == target) return steps;
            for (auto neighbor : neighbors(current))
            {
                if (deadSet.count(neighbor)) continue;
                deadSet.insert(neighbor);
                q.push(neighbor);
            }
        }
    }
    return -1;
}
#endif

void test(std::vector<std::string> deadends, std::string target, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = openLock(deadends, target);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10000000;
    constexpr unsigned int trials = 1000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"0201", "0101", "0102", "1212", "2002"}, "0202", 6, trials);
    test({"8888"}, "0009", 1, trials);
    test({"8887", "8889", "8878", "8898", "8788", "8988", "7888", "9888"}, "8888", -1, trials);
    test({"0000"}, "8888", -1, trials);
    return 0;
}

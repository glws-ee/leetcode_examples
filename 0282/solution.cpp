#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <functional>
#include <unordered_set>
#include <cassert>

using namespace std;

constexpr int null = -1000;

std::vector<std::string> addOperators(const std::string &num, int target)
{
#if 1
    const int size = num.size();
    std::vector<string> result;
    std::function<void(int, std::string, long, long) > dfs =
        [&](int index, std::string path, long res, long prev)
    {
        if ((index == size) && (res == target))
        {
            result.push_back(path);
            return;
        }
        long number = 0;
        std::string sub_string = "";
        for (int i = index; i < size; ++i)
        {
            sub_string += num[i];
            number = (number * 10) + num[i] - '0';
            
            if ((sub_string.size() > 1) && (sub_string[0] == '0'))
                break;
            if (index == 0)
                dfs(i + 1, sub_string, number, number);
            else
            {
                dfs(i + 1, path + "+" + sub_string, res + number, number);
                dfs(i + 1, path + "-" + sub_string, res - number, -number);
                dfs(i + 1, path + "*" + sub_string, (res - prev) + (prev * number), prev * number);
            }
        }
    };
    dfs(0, "", 0, 0);
    return result;
#else
    // Conversion from infix to RPN at the end.
    // At a certain position, add one of the three symbols.
    // num op num op num op
    const int size = num.size();
    std::vector<long> buffer;
    buffer.reserve(size * 2);
    std::vector<std::string> result;
    std::stack<long> rpn_stack;
    std::queue<std::pair<bool, long> > rpn_q;
    
    auto compute = [&]() -> int
    {
        rpn_q.push({false, buffer[0]});
        for (int i = 1; i < buffer.size(); i += 2)
        {
            bool negative = buffer[i] == 1;
            if (buffer[i] == 2) // Multiplication;
            {
                // Flush all additions out of the stack.
                while ((!rpn_stack.empty()) && (rpn_stack.top()))
                {
                    rpn_q.push({true, rpn_stack.top()});
                    rpn_stack.pop();
                }
            }
            else // Addition.
            {
                // With addition flush the stack because all operators have the
                // same precedence or more.
                while (!rpn_stack.empty())
                {
                    rpn_q.push({true, rpn_stack.top()});
                    rpn_stack.pop();
                }
            }
            rpn_stack.push(buffer[i] == 2);
            rpn_q.push({false, (negative)?-buffer[i + 1]:buffer[i + 1]});
        }
        // Flush the remaining operators from the stack.
        while (!rpn_stack.empty())
        {
            rpn_q.push({true, rpn_stack.top()});
            rpn_stack.pop();
        }
        // Use the RPN queue to compute the value of the expression.
        while (!rpn_q.empty())
        {
            auto [op, number] = rpn_q.front();
            rpn_q.pop();
            if (op)
            {
                long first = rpn_stack.top();
                rpn_stack.pop();
                long second = rpn_stack.top();
                rpn_stack.pop();
                rpn_stack.push((number)?(first * second):(first + second));
            }
            else rpn_stack.push(number);
        }
        int ans = rpn_stack.top();
        rpn_stack.pop();
        assert(rpn_q.size() == 0);
        return ans;
    };
    std::function<void(int)> tokenize = [&](int position) -> void
    {
        for (int p = position; p < size; ++p)
        {
            const int nsize = p - position + 1;
            if ((num[position] == '0') && (nsize > 1)) continue; // Avoid numbers beginning with 0?
            long number = std::stol(num.substr(position, nsize));
            if (p + 1 == size)
            {
                buffer.push_back(number); // Add the number.
                if (compute() == target)
                {
                    std::string current;
                    current += std::to_string(buffer[0]);
                    for (int i = 1; i < buffer.size(); i += 2)
                    {
                        if      (buffer[i] == 0) current += '+';
                        else if (buffer[i] == 1) current += '-';
                        else current += '*';
                        current += std::to_string(buffer[i + 1]);
                    }
                    result.push_back(current);
                }
                buffer.pop_back(); // Remove the number.
            }
            else
            {
                buffer.push_back(number); // Add the number.
                for (int i = 0; i < 3; ++i)
                {
                    buffer.push_back(i); // Add the operator.
                    tokenize(p + 1);
                    buffer.pop_back(); // Remove the operator.
                }
                buffer.pop_back(); // Remove the number.
            }
        }
    };
    tokenize(0);
    return result;
#endif
}

std::ostream& operator<<(std::ostream &out, const std::vector<std::string> &str)
{
    out << '{';
    bool next = false;
    for (const std::string &s : str)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << s;
    }
    out << '}';
    return out;
}

bool operator==(const std::vector<std::string> &left, const std::vector<std::string> &right)
{
    if (left.size() != right.size()) return false;
    std::unordered_set<std::string> lut(left.begin(), left.end());
    for (const std::string &str : right)
    {
        if (auto it = lut.find(str); it != lut.end())
            lut.erase(it);
        else return false;
    }
    return lut.empty();
}

void test(std::string num, int target, std::vector<std::string> solution, unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = addOperators(num, target);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("123", 6, {"1*2*3", "1+2+3"}, trials);
    test("232", 8, {"2*3+2", "2+3*2"}, trials);
    test("105", 5, {"1*0+5", "10-5"}, trials);
    test("00", 0, {"0+0", "0*0", "0-0"}, trials);
    test("3456237490", 9191, {}, trials);
    test("2147483647", 9000, {"2+1*4*748*3-6+4*7", "2*1+4*748*3-6+4*7", "21+4*748*3+6+4-7"}, trials);
    return 0;
}

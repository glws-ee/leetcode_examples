#include <vector>
#include <cstring>
#include <iostream>

using namespace std;

int maxScore(vector<int> &cardPoints, int k)
{
#if 1
    const int window_size = cardPoints.size() - k;
    if (window_size == 0)
    {
        int total = 0;
        for (const auto &v : cardPoints)
            total += v;
        return total;
    }
    int window_value = 0, total = 0, idx;
    for (idx = 0; idx < window_size; ++idx)
        window_value += cardPoints[idx];
    int min_value = total = window_value;
    const int size = cardPoints.size();
    for (int previous = 0; idx < size; ++previous, ++idx)
    {
        window_value += (total += cardPoints[idx]) - cardPoints[previous];
        if (window_value < min_value)
            min_value = window_value;
    }
    return total - min_value;
#else
    const int window_size = cardPoints.size() - k;
    int total = 0;
    for (const auto &v : cardPoints)
        total += v;
    if (window_size == 0) return total;
    int window_value = 0, idx;
    for (idx = 0; idx < window_size; ++idx)
        window_value += cardPoints[idx];
    int min_value = window_value;
    const int size = cardPoints.size();
    for (int previous = 0; idx < size; ++previous, ++idx)
    {
        window_value += cardPoints[idx] - cardPoints[previous];
        if (window_value < min_value)
            min_value = window_value;
    }
    return total - min_value;
#endif
}

void test(std::vector<int> cardPoints, int k, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxScore(cardPoints, k);
    if (result == solution) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    constexpr unsigned int trials = 2'000'000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5, 6, 1}, 3, 12, trials);
    test({2, 2, 2}, 2, 4, trials);
    test({9, 7, 7, 9, 7, 7, 9}, 7, 55, trials);
    test({1, 1000, 1}, 1, 1, trials);
    test({1, 79, 80, 1, 1, 1, 200, 1}, 3, 202, trials);
    return 0;
}


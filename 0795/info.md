We are given an array `nums` of positive integers, and two positive integers `left` and `right` (`left <= right`).

Return the number of (contiguous, non-empty) subarrays such that the value of the maximum array element in that subarray is at least `left` and at most `right`.

**Example:**<br>
*Input:*<br>
`nums = [2, 1, 4, 3]`<br>
`left = 2`<br>
`right = 3`<br>
*Output:* `3`<br>
*Explanation:* There are three subarrays that meet the requirements: `[2], [2, 1], [3]`.<br>

**Note:**<br>
- `left`, `right`, and `nums[i]` will be an integer in the range `[0, 10^9]`.
- The length of `nums` will be in the range of `[1, 50000]`.

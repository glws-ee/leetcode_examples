#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

int numSubarrayBoundedMax(vector<int> nums, int left, int right)
{
#if 1
    // This is done in reverse, adding elements as we find more valid positions.
    // Like adding from the left instead of adding from the right.
    const int n = nums.size();
    int result = 0;
    for (int i = 0, count = 0, previous = -1; i < n; ++i)
    {
        if (nums[i] > right) { previous = i; count = 0; }
        else if (nums[i] < left) result += count;
        else result += (count = i - previous);
    }
    return result;
#else
    const int n = nums.size();
    int in_range[50000];
    int number_in_range = 0;
    int result = 0, begin = 0, i = 0;
    for (; i < n; ++i)
    {
        if (nums[i] > right)
        {
            // |--##-#-----|
            // |--x--------| 3 * 9 = 27 different sub-arrays.
            // |  x        | -x        |--x        |
            // |  x-       | -x-       |--x-       |
            // |  x--      | -x--      |--x--      |
            // |  x---     | -x---     |--x---     |
            // |  x----    | -x----    |--x----    |
            // |  x-----   | -x-----   |--x-----   |
            // |  x------  | -x------  |--x------  |
            // |  x------- | -x------- |--x------- |
            // |  x--------| -x--------|--x--------|
            // |---x-------| 4 * 8 = 32 different sub-arrays
            //               distance with previous is 1 -> 3 - 2 = 1
            //               ==> 1 * 8 = 8 extra sub-arrays.
            //             |  x-       | -x-       |--x-       |
            //             |  x--      | -x--      |--x--      |
            //             |  x---     | -x---     |--x---     |
            //             |  x----    | -x----    |--x----    |
            //             |  x-----   | -x-----   |--x-----   |
            //             |  x------  | -x------  |--x------  |
            //             |  x------- | -x------- |--x------- |
            //             |  x--------| -x--------|--x--------|
            // |   x       |  -x       | --x       |---x       |
            // |   x-      |  -x-      | --x-      |---x-      |
            // |   x--     |  -x--     | --x--     |---x--     |
            // |   x---    |  -x---    | --x---    |---x---    |
            // |   x----   |  -x----   | --x----   |---x----   |
            // |   x-----  |  -x-----  | --x-----  |---x-----  |
            // |   x------ |  -x------ | --x------ |---x------ |
            // |   x-------|  -x-------| --x-------|---x-------|
            // | (UNIQUE)  | (REPEATED)| (REPEATED)| (REPEATED)|
            // |-----x-----| 6 * 6 = 36 different sub-arrays.
            //               distance with the previous is 2 -> 5 - 3 = 2
            //               ==> 2 * 6 = 12 extra sub-arrays?
            //                                     |  x---     | -x---     |--x---     |
            //                                     |  x----    | -x----    |--x----    |
            //                                     |  x-----   | -x-----   |--x-----   |
            //                                     |  x------  | -x------  |--x------  |
            //                                     |  x------- | -x------- |--x------- |
            //                                     |  x--------| -x--------|--x--------|
            //                         |   x--     |  -x--     | --x--     |---x--     |
            //                         |   x---    |  -x---    | --x---    |---x---    |
            //                         |   x----   |  -x----   | --x----   |---x----   |
            //                         |   x-----  |  -x-----  | --x-----  |---x-----  |
            //                         |   x------ |  -x------ | --x------ |---x------ |
            //                         |   x-------|  -x-------| --x-------|---x-------|
            // |     x     |    -x     |   --x     |  ---x     | ----x     |-----x     |
            // |     x-    |    -x-    |   --x-    |  ---x-    | ----x-    |-----x-    |
            // |     x--   |    -x--   |   --x--   |  ---x--   | ----x--   |-----x--   |
            // |     x---  |    -x---  |   --x---  |  ---x---  | ----x---  |-----x---  |
            // |     x---- |    -x---- |   --x---- |  ---x---- | ----x---- |-----x---- |
            // |     x-----|    -x-----|   --x-----|  ---x-----| ----x-----|-----x-----|
            // | (UNIQUE)  | (UNIQUE)  | REPEATED*1| REPEATED*2| REPEATED*2| REPEATED*2|
            for (int j = 0; j < number_in_range; ++j)
            {
                int nl = in_range[j] - begin + 1;
                int nr = i - in_range[j];
                result += nl * nr;
                begin = in_range[j] + 1;
            }
            // The range is between [begin, i).
            begin = i + 1;
            number_in_range = 0;
        }
        else if ((nums[i] >= left) && (nums[i] <= right))
                in_range[number_in_range++] = i;
    }
    // Check the last sub-string if any.
    for (int j = 0; j < number_in_range; ++j)
    {
        int nl = in_range[j] - begin + 1;
        int nr = i - in_range[j];
        result += nl * nr;
        begin = in_range[j] + 1;
    }
    return result;
#endif
}

void test(std::vector<int> nums, int left, int right, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = numSubarrayBoundedMax(nums, left, right);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 3, 2, 1, 3, 1, 1, 1, 1, 1}, 2, 3, 47, trials);
    test({1, 1, 3, 2, 1, 3, 1, 1, 1, 1, 1, 4}, 2, 3, 47, trials);
    test({4, 1, 1, 3, 2, 1, 3, 1, 1, 1, 1, 1, 4}, 2, 3, 47, trials);
    test({1, 1, 4, 1, 1, 3, 2, 1, 3, 1, 1, 1, 1, 1, 4}, 2, 3, 47, trials);
    test({4, 1, 1, 3, 2, 1, 3, 1, 1, 1, 1, 1, 4, 1, 1}, 2, 3, 47, trials);
    test({4, 1, 1, 3, 2, 1, 3, 1, 1, 1, 1, 1, 4, 1, 1, 4, 2, 1, 4, 3}, 2, 3, 50, trials);
    test({2, 1, 4, 3}, 2, 3, 3, trials);
    return 0;
}

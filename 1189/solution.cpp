#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

int maxNumberOfBalloons(string text)
{
    short histogram[128];
    std::memset(histogram, 0, sizeof(histogram));
    for (char c : text)
        ++histogram[c];
    // balloon -> b:1 a:1 l:2 o:2 n:1
    short result = std::min<short>(histogram['a'], histogram['b']);
    result = std::min<short>(result, histogram['n']);
    result = std::min<short>(result, std::min<short>(histogram['l'], histogram['o']) / 2);
    return result;
}

void test(std::string text, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxNumberOfBalloons(text);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("nlaebolko", 1, trials);
    test("loonbalxballpoon", 2, trials);
    test("leetcode", 0, trials);
    return 0;
}

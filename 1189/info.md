=Maximum Number of Balloons=

Given a string `text`, you want to use the characters of `text` to form as many instances of the word **"balloon"** as possible.

You can use each character in `text` **at most once**. Return the maximum number of instances that can be formed.

**Example 1:**<br>
*Input:* `text = "nlaebolko"`<br>
*Output:* `1`<br>

**Example 2:**<br>
*Input:* `text = "loonbalxballpoon"`<br>
*Output:* `2`<br>

**Example 3:**<br>
*Input:* `text = "leetcode"`
*Output:* `0`
 
**Constraints:**<br>
- `1 <= text.length <= 10^4`
- text consists of lower case English letters only.




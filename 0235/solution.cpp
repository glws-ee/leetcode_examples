#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

struct TreeNode
{
    int val = 0;
    TreeNode * left = nullptr;
    TreeNode * right = nullptr;
    TreeNode(void) = default;
    TreeNode(int v) : val(v), left(nullptr), right(nullptr) {}
    TreeNode(int v, const TreeNode * l, const TreeNode * r) :
        val(v),
        left((l != nullptr)?new TreeNode(*l):nullptr),
        right((r != nullptr)?new TreeNode(*r):nullptr) {}
    TreeNode(const TreeNode &other) :
        val(other.val),
        left((other.left != nullptr)?new TreeNode(*other.left):nullptr),
        right((other.right != nullptr)?new TreeNode(*other.right):nullptr) {}
    TreeNode(TreeNode &&other) :
        val(other.val),
        left(std::exchange(other.left, nullptr)),
        right(std::exchange(other.right, nullptr)) {}
    ~TreeNode(void) { delete left; delete right; }
    TreeNode& operator=(const TreeNode &other)
    {
        if (this != &other)
        {
            delete left;
            delete right;
            val = other.val;
            left = (other.left != nullptr)?new TreeNode(*other.left):nullptr;
            right = (other.right != nullptr)?new TreeNode(*other.right):nullptr;
        }
        return *this;
    }
    TreeNode& operator=(TreeNode &&other)
    {
        if (this != &other)
        {
            val = other.val;
            left = std::exchange(other.left, nullptr);
            right = std::exchange(other.right, nullptr);
        }
        return *this;
    }
};

TreeNode * vec2tree(const std::vector<int> &tree)
{
    std::queue<TreeNode *> active_nodes;
    
    TreeNode * result = nullptr;
    bool left = true;
    for (const auto &v : tree)
    {
        if (v != null)
        {
            if (result == nullptr)
            {
                result = new TreeNode(v);
                active_nodes.push(result);
                active_nodes.push(result);
            }
            else
            {
                TreeNode * current = new TreeNode(v);
                if (left) active_nodes.front()->left = current;
                else active_nodes.front()->right = current;
                active_nodes.pop();
                active_nodes.push(current);
                active_nodes.push(current);
                left = !left;
            }
        }
        else
        {
            active_nodes.pop();
            left = !left;
        }
    }
    return result;
}

std::vector<int> tree2vec(TreeNode * root)
{
    if (root == nullptr) return {};
    std::vector<int> result;
    std::queue<TreeNode *> active_nodes;
    active_nodes.push(root);
    result.push_back(root->val);
    while (!active_nodes.empty())
    {
        TreeNode * current = active_nodes.front();
        active_nodes.pop();
        if (current->left != nullptr)
        {
            active_nodes.push(current->left);
            result.push_back(current->left->val);
        }
        else result.push_back(null);
        if (current->right != nullptr)
        {
            active_nodes.push(current->right);
            result.push_back(current->right->val);
        }
        else result.push_back(null);
    }
    while (result.back() == null) result.pop_back();
    return result;
}

std::ostream& operator<<(std::ostream &out, const std::vector<int> &vec)
{
    out << '{';
    bool next = false;
    for (const auto &v : vec)
    {
        if (next) out << ", ";
        out << v;
        next = true;
    }
    out << '}';
    return out;
}

TreeNode * search(TreeNode * root, int value)
{
    if (root == nullptr) return nullptr;
    if (root->val == value) return root;
    TreeNode * result = search(root->left, value);
    if (result != nullptr) return result;
    return search(root->right, value);
}

// ################################################################################################
// ################################################################################################

TreeNode * lowestCommonAncestor(TreeNode * root, TreeNode * p, TreeNode * q)
{
    if (root == nullptr) return nullptr;
    if ((root == p) || (root == q)) return root;
    TreeNode * search_left = ((root->val > p->val) || (root->val > q->val))?lowestCommonAncestor(root->left, p, q):nullptr;
    TreeNode * search_right = ((root->val < p->val) || (root->val < q->val))?lowestCommonAncestor(root->right, p, q):nullptr;
    if (((search_left == p) || (search_right == p))
     && ((search_left == q) || (search_right == q))) return root;
    return (search_left)?search_left:search_right;
    //bool search_p = ((root == p) || (search_left == p) || (search_right == p));
    //bool search_q = ((root == q) || (search_left == q) || (search_right == q));
    //if (search_p && search_q) return root;
    //if ((root == p) || (root == q)) return root;
}

// ################################################################################################
// ################################################################################################

void test(std::vector<int> tree, int p, int q, int solution, unsigned int trials = 1)
{
    //std::cout << "INPUT: " << tree << '\n';
    int result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        TreeNode * node_p = search(root, p);
        TreeNode * node_q = search(root, q);
        TreeNode * node_result = lowestCommonAncestor(root, node_p, node_q);
        result = (node_result != nullptr)?node_result->val:null;
        delete root;
    }
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({6, 2, 8, 0, 4, 7, 9, null, null, 3, 5}, 2, 8, 6, trials);
    test({6, 2, 8, 0, 4, 7, 9, null, null, 3, 5}, 2, 4, 2, trials);
    test({2, 1}, 2, 1, 2, trials);
    return 0;
}

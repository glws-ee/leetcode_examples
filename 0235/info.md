Given a binary search tree (BST), find the lowest common ancestor (LCA) of two given nodes in the BST.

According to the definition of LCA on Wikipedia: "The lowest common ancestor is defined between two nodes p and q as the lowest node in T that has both `p` and `q` as descendants (where we allow **a node to be a descendant of itself**)."

**Example 1:**<br>
```mermaid 
graph TB
a[6]-->b[2]
a-->c[8]
b-->d[0]
b-->e[4]
e-->f[3]
e-->g[5]
c-->h[7]
c-->i[9]
```

*Input:* `root = [6,2,8,0,4,7,9,null,null,3,5], p = 2, q = 8`<br>
*Output:* `6`<br>
*Explanation:* `The LCA of nodes 2 and 8 is 6.`<br>

**Example 2:**<br>
```mermaid 
graph TB
a[6]-->b[2]
a-->c[8]
b-->d[0]
b-->e[4]
e-->f[3]
e-->g[5]
c-->h[7]
c-->i[9]
```
*Input:* `root = [6, 2, 8, 0, 4, 7, 9, null, null, 3, 5], p = 2, q = 4`<br>
*Output:* `2`<br>
*Explanation:* `The LCA of nodes 2 and 4 is 2, since a node can be a descendant of itself according to the LCA definition.`<br>

**Example 3:**<br>
*Input:* `root = [2, 1], p = 2, q = 1`<br>
*Output:* `2`<br>
 
**Constraints:**<br>
- The number of nodes in the tree is in the range `[2, 10^5]`.
- `-10^9 <= Node.val <= 10^9`
- All `Node.val` are unique.
- `p != q`
- `p` and `q` will exist in the BST.


#include <vector>
#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stack>
#include <queue>
#include <utility>
#include <map>
#include <unordered_set>
#include <unordered_map>

using namespace std;

constexpr int null = -1000;

int signature(const std::string &str)
{
    int result = 0;
    for (char c : str)
        result |= 1 << (c - 'a');
    return result;
}

std::vector<std::vector<std::string> > groupAnagrams(std::vector<std::string> strs)
{
#if 0 // 3.2 seconds on PC | 50 ms on LEETCODE
    std::unordered_map<std::string, std::vector<std::string> > groups;
    std::vector<std::vector<std::string> > result;
    for (std::string str : strs)
    {
        std::string key = str;
        std::sort(key.begin(), key.end());
        groups[key].push_back(str);
    }
    for (auto& [_, group] : groups)
        result.emplace_back(std::move(group));
    return result;
#elif 1 // 2.6 seconds on PC | 23 ms on LEETCODE
    std::vector<std::vector<std::string> > result;
    std::unordered_map<int, std::vector<int> > groups;
    const int n = strs.size();
    for (int i = 0; i < n; ++i)
        groups[signature(strs[i])].push_back(i);
    for (const auto& [_, group] : groups)
    {
        if (group.size() == 1)
            result.push_back({strs[group[0]]});
        else
        {
            std::map<std::string, std::vector<int>> anagrams;
            for (int i : group)
            {
                std::string key = strs[i];
                std::sort(key.begin(), key.end());
                anagrams[key].push_back(i);
            }
            for (const auto& [_, ana] : anagrams)
            {
                std::vector<std::string> current;
                for (int i : ana)
                    current.push_back(strs[i]);
                result.emplace_back(std::move(current));
            }
        }
    }
    return result;
#else // 2.3 seconds on PC | 38 ms on LEETCODE
    std::vector<std::vector<std::string> > result;
    std::unordered_map<int, std::vector<int> > groups;
    const int n = strs.size();
    for (int i = 0; i < n; ++i)
        groups[signature(strs[i])].push_back(i);
    auto cmp = [](const std::string &a, const std::string &b) -> bool
    {
        if (a.size() < b.size()) return true;
        else if (a.size() > b.size()) return false;
        const int n = a.size();
        char hist[26];
        std::memset(hist, 0, sizeof(hist));
        for (char c : a)
            ++hist[c - 'a'];
        for (char c : b)
            --hist[c - 'a'];
        for (int i = 0; i < 26; ++i)
        {
            if (hist[i] < 0) return true;
            if (hist[i] > 0) return false;
        }
        return false;
    };
    for (const auto& [_, group] : groups)
    {
        if (group.size() == 1)
            result.push_back({strs[group[0]]});
        else
        {
            std::map<std::string, std::vector<int>, decltype(cmp)> anagrams(cmp);
            for (int i : group)
                anagrams[strs[i]].push_back(i);
            for (const auto& [_, ana] : anagrams)
            {
                std::vector<std::string> current;
                for (int i : ana)
                    current.push_back(strs[i]);
                result.emplace_back(std::move(current));
            }
        }
    }
    return result;
#endif
}

bool operator==(const std::vector<std::vector<std::string> > &left, const std::vector<std::vector<std::string> > &right)
{
    if (left.size() != right.size()) return false;
    std::map<int, std::vector<std::unordered_set<std::string> > > groups;
    for (const auto &l : left)
        groups[signature(l[0])].push_back({l.begin(), l.end()});
    for (const auto &r : right)
    {
        auto it = groups.find(signature(r[0]));
        if (it == groups.end()) return false;
        bool found = false;
        for (const auto &lut : it->second)
        {
            if (r.size() == lut.size())
            {
                bool not_found = false;
                for (std::string s : r)
                {
                    if (lut.find(s) == lut.end())
                    {
                        not_found = true;
                        break;
                    }
                }
                if (!not_found)
                {
                    found = true;
                    break;
                }
            }
        }
        if (!found) return false;
    }
    return true;
}

std::ostream& operator<<(std::ostream& out, const std::vector<std::vector<std::string> > &anagrams)
{
    const int No = anagrams.size();
    out << '{';
    bool next_out = false;
    for (int i = 0; i < No; ++i)
    {
        if (next_out) [[likely]] out << ", ";
        if ((No > 15) && (i > 10)) { i = No - 1; out << "..., "; }
        next_out = true;
        out << '{';
        bool next_in = false;
        for (const auto &v : anagrams[i])
        {
            if (next_in) [[likely]] out << ", ";
            next_in = true;
            out << v;
        }
        out << '}';
    }
    out << '}';
    return out;
}

void test(std::vector<std::string> strs, std::vector<std::vector<std::string> > solution, unsigned int trials = 1)
{
    std::vector<std::vector<std::string> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = groupAnagrams(strs);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

std::vector<std::string> load_test(const char * filename)
{
    std::ifstream file(filename);
    std::vector<std::string> result;
    std::string current;
    while (file >> current)
        result.emplace_back(std::move(current));
    file.close();
    return result;
}

std::vector<std::vector<std::string> > load_solution(const char * filename)
{
    std::ifstream file(filename);
    std::vector<std::vector<std::string> > result;
    std::string line;
    while (std::getline(file, line))
    {
        std::istringstream iss(line);
        std::vector<std::string> current;
        std::string word;
        while (iss >> word)
            current.emplace_back(std::move(word));
        if (current.size() > 0)
            result.emplace_back(std::move(current));
    }
    file.close();
    return result;
}

int main(int, char **)
{
// https://leetcode.com/problems/group-anagrams/submissions/
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"eat", "tea", "tan", "ate", "nat", "bat"}, {{"bat"}, {"nat", "tan"}, {"ate", "eat", "tea"}}, trials);
    test({"abc", "bca", "cba"}, {{"abc", "bca", "cba"}}, trials);
    test({""}, {{""}}, trials);
    test({"a"}, {{"a"}}, trials);
    test({"abba", "baab", "ab"}, {{"ab"}, {"abba", "baab"}}, trials);
    test({"ddddddddddg", "dgggggggggg"}, {{"dgggggggggg"}, {"ddddddddddg"}}, trials);
    test(load_test("test_a.txt"), load_solution("solution_a.txt"), trials);
    return 0;
}

#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

bool isValidSudoku(std::vector<std::vector<char>> board)
{
    bool used_row[9][9], used_column[9][9], used_board[9][9];
    std::memset(used_row, false, sizeof(used_row));
    std::memset(used_column, false, sizeof(used_row));
    std::memset(used_board, false, sizeof(used_row));
    for (int r = 0; r < 9; ++r)
    {
        for (int c = 0; c < 9; ++c)
        {
            if (board[r][c] != '.')
            {
                int idx = board[r][c] - '1';
                if (used_row[r][idx]) return false;
                used_row[r][idx] = true;
                if (used_column[c][idx]) return false;
                used_column[c][idx] = true;
                int cell = r / 3 + 3 * (c / 3);
                if (used_board[cell][idx]) return false;
                used_board[cell][idx] = true;
            }
        }
    }
    return true;
}


void test(std::vector<std::vector<char> > board, bool solution, unsigned int trials = 1)
{
    bool result;
    for (unsigned int i = 0; i < trials; ++i)
        result = isValidSudoku(board);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{'5','3','.','.','7','.','.','.','.'}
         ,{'6','.','.','1','9','5','.','.','.'}
         ,{'.','9','8','.','.','.','.','6','.'}
         ,{'8','.','.','.','6','.','.','.','3'}
         ,{'4','.','.','8','.','3','.','.','1'}
         ,{'7','.','.','.','2','.','.','.','6'}
         ,{'.','6','.','.','.','.','2','8','.'}
         ,{'.','.','.','4','1','9','.','.','5'}
         ,{'.','.','.','.','8','.','.','7','9'}}, true, trials);
    test({{'8','3','.','.','7','.','.','.','.'}
         ,{'6','.','.','1','9','5','.','.','.'}
         ,{'.','9','8','.','.','.','.','6','.'}
         ,{'8','.','.','.','6','.','.','.','3'}
         ,{'4','.','.','8','.','3','.','.','1'}
         ,{'7','.','.','.','2','.','.','.','6'}
         ,{'.','6','.','.','.','.','2','8','.'}
         ,{'.','.','.','4','1','9','.','.','5'}
         ,{'.','.','.','.','8','.','.','7','9'}}, false, trials);
    return 0;
}

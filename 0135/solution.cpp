#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

int candy(std::vector<int> ratings)
{
#if 1
    const int n = ratings.size();
    int aux[n];
    
    // When the ratting of the previous child is lower than the current,
    // current child gets an extra candy.
    aux[0] = 1;
    for (int i = 1; i < n; ++i)
        aux[i] = (ratings[i] > ratings[i - 1])?(aux[i - 1] + 1):1;
    // When the ratting of the next child is lower than the current child AND
    // he receives more candies than the current child, increase the number of
    // candies of the current child.
    int total = aux[n - 1];
    for (int i = n - 1; i > 0; --i)
    {
        if ((ratings[i - 1] > ratings[i]) && (aux[i - 1] <= aux[i]))
            aux[i - 1] = aux[i] + 1;
        total += aux[i - 1];
    }
    return total;
#else
    const int n = ratings.size();
    std::pair<int, int> order[n];
    int candies[n];
    for (int i = 0; i < n; ++i)
    {
        order[i] = {ratings[i], i};
        candies[i] = 0;
    }
    std::sort(order, order + n);
    int total = 0;
    for (int i = 0; i < n; ++i)
    {
        int idx = order[i].second;
        candies[idx] = 1;
        if ((idx > 0) && (ratings[idx - 1] < ratings[idx]))
            candies[idx] = std::max(candies[idx], candies[idx - 1] + 1);
        if ((idx < n - 1) && (ratings[idx + 1] < ratings[idx]))
            candies[idx] = std::max(candies[idx], candies[idx + 1] + 1);
        total += candies[idx];
    }
    return total;
#endif
}

void test(std::vector<int> ratings, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = candy(ratings);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 2}, 5, trials);
    test({1, 2, 2}, 4, trials);
    test({1, 2, 3, 4, 5}, 15, trials);
    test({5, 4, 3, 2, 1}, 15, trials);
    test({1, 2, 3, 2, 1}, 9, trials);
    test({1, 2, 2, 2, 1}, 7, trials);
    test({1, 1, 8, 2, 2}, 6, trials);
    return 0;
}

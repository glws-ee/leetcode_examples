=Distinct Substances=

Given two strings `s` and `t`, return *the number of distinct subsequences of* `s` *which equals* `t`.

A string's **subsequence** is a new string formed from the original string by deleting some (can be none) of the characters without disturbing the remaining characters' relative positions. (i.e., `"ACE"` is a subsequence of `"ABCDE"` while `"AEC"` is not).

It is guaranteed the answer fits on a 32-bit signed integer.

**Example 1:**<br>
*Input:* `s = "rabbbit", t = "rabbit"`<br>
*Output:* `3`<br>
*Explanation:*<br>
As shown below, there are 3 ways you can generate `"rabbit"` from `S`.<br>
**rabb** b **it**<br>
**ra** b **bbit**<br>
**rab** b **bit**<br>

**Example 2:**<br>
*Input:* `s = "babgbag", t = "bag"`<br>
*Output:* `5`<br>
*Explanation:*<br>
As shown below, there are 5 ways you can generate `"bag"` from `S`.<br>
**ba** b **g** bag<br>
**ba** bgba **g**<br>
**b** abgb **ag**<br>
ba **b** gb **ag**<br>
babg **bag**<br>
 
**Constraints:**<br>
- `1 <= s.length, t.length <= 1000`
- `s` and `t` consist of English letters.



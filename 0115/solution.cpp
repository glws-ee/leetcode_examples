#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <functional>
#include <unordered_map>

using namespace std;

constexpr int null = -1000;

int numDistinct(std::string s, std::string t)
{
    const int ns = s.size();
    const int nt = t.size();
    std::unordered_map<long, int> memo;
    std::function<int(int, int)> search = [&](int pos_s, int pos_t) -> int
    {
        if ((pos_s >= ns) || (pos_t >= nt)) return 0;
        const long call_id = static_cast<long>(pos_s) << 32 | static_cast<long>(pos_t);
        if (auto it = memo.find(call_id); it != memo.end()) return it->second;
        int result = 0;
        if (pos_t == nt - 1)
        {
            for (int i = pos_s; i < ns; ++i)
                if (s[i] == t[pos_t])
                    ++result;
        }
        else
        {
            const int end = ns - (nt - 1 - pos_t);
            for (int i = pos_s; i < end; ++i)
                if (s[i] == t[pos_t])
                    result += search(i + 1, pos_t + 1);
        }
        return memo[call_id] = result;
    };
    return search(0, 0);
}

void test(std::string s, std::string t, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = numDistinct(s, t);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("rabbbit", "rabbit", 3, trials);
    test("babgbag", "bag", 5, trials);
    test("adbdadeecadeadeccaeaabdabdbcdabddddabcaaadbabaaedeeddeaeebcdeabcaaaeeaeeabcddcebddebeebedaecccbdcbcedbdaeaedcdebeecdaaedaacadbdccabddaddacdddc", "bcddceeeebecbc", 700531452, trials);
    return 0;
}

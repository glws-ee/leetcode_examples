#include <vector>
#include <iostream>

using namespace std;

static constexpr int MAXINT = 1000001;

int minOperations(vector<int>& nums, int x)
{
    const int n = nums.size();
    int accum = 0;
    int result = n + 1;
    // Increase left side ......................................................
    int left = 0;
    for (; (left < n) && (accum < x); ++left) accum += nums[left];
    if (accum == x) result = left;
    if (left == n) return (accum == x)?left:-1; // No possible solution, exit!
    
    // Decrease left and increase right ........................................
    int oper = left;
    int right = n - 1;
    for (; left > 0;)
    {
        // Remove left.
        --left;
        accum -= nums[left];
        --oper;
        // Add right.
        for (; (right >= 0) && (accum < x); --right, ++oper)
            accum += nums[right];
        // Check if solution is better.
        if ((accum == x) && (oper < result))
            result = oper;
    }
    
    // Increase right ..........................................................
    for (; (right >= 0) && (accum < x) && (oper < result); --right, ++oper) accum += nums[right];
    if ((accum == x) && (oper < result))
        result = oper;
    return (result <= n)?result:-1;
}

void test(std::vector<int> values, int expected, int solution)
{
    int result = minOperations(values, expected);
    if (result == solution) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
    test({1, 1, 4, 2, 3}, 5, 2);
    test({5, 6, 7, 8, 9}, 4, -1);
    test({3, 2, 20, 1, 1, 3}, 10, 5);
    test({8828,9581,49,9818,9974,9869,9991,10000,10000,10000,9999,9993,9904,8819,1231,6309}, 134365, 16);
    return 0;
}


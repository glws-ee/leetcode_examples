#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

std::string removeDuplicates(std::string s)
{
#if 1
    int n_in = s.size(), n_out = 0;
    char buffer_first[n_in], buffer_second[n_in];
    for (int i = 0; i < n_in; ++i) buffer_first[i] = s[i];
    char * in_ptr = buffer_first, * out_ptr = buffer_second;
    
    while (true)
    {
        n_out = 0;
        int i;
        for (i = 0; i < n_in - 1; ++i)
        {
            if (in_ptr[i] == in_ptr[i + 1]) ++i;
            else out_ptr[n_out++] = in_ptr[i];
        }
        if (i < n_in) out_ptr[n_out++] = in_ptr[i++];
        
        if (n_in == n_out)
            break;
        std::swap(in_ptr, out_ptr);
        n_in = n_out;
    }
    return std::string(in_ptr, n_in);
#else
    while (true)
    {
        const int n = s.size();
        std::string buffer;
        int i;
        for (i = 0; i < n - 1; ++i)
        {
            if (s[i] == s[i + 1]) ++i;
            else buffer += s[i];
        }
        if (i < n) buffer += s[i];
        if (buffer.size() == s.size())
            break;
        s = buffer;
    }
    return s;
#endif
}

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = removeDuplicates(s);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abbaca", "ca", trials);
    test("azxxzy", "ay", trials);
    return 0;
}

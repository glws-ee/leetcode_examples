#include <vector>
#include <cstring>
#include <iostream>
#include <string>
#include <cmath>
#include <charconv>

using namespace std;

long reverseNumber(long value)
{
    long copy = value;
    long accumulate = 0;
    while (copy)
    {
        long b = copy / 10;
        long r = copy - b * 10;
        accumulate = accumulate * 10 + r;
        copy = b;
    }
    return accumulate;
}

bool isPalindrome(long value) { return value == reverseNumber(value); }

long superpalindromesValue(unsigned int digits, long value_left, long value_right)
{
    const bool cut_number = digits % 2 == 1;
    const unsigned int half = digits / 2 + cut_number;//static_cast<unsigned int>(std::ceil(static_cast<double>(digits) / 2.0));
    unsigned int max_digit = 1;
    for (unsigned int i = 0; i < half; ++i)
        max_digit *= 10;
    unsigned int number_of_superpalindromes = 0;
    if (cut_number)
    {
        for (unsigned int d = max_digit / 10; d < max_digit; ++d)
        {
            long value = max_digit * (d / 10) + reverseNumber(d);
            value = value * value;
            if ((value >= value_left) && (value <= value_right) && isPalindrome(value))
                ++number_of_superpalindromes;
        }
    }
    else
    {
        for (unsigned int d = max_digit / 10; d < max_digit; ++d)
        {
            long value = max_digit * d + reverseNumber(d);
            value = value * value;
            if ((value >= value_left) && (value <= value_right) && isPalindrome(value))
                ++number_of_superpalindromes;
        }
    }
    return number_of_superpalindromes;
}

int superpalindromesInRange(string left, string right)
{
    const long value_left = std::stol(left);
    const long value_right = std::stol(right);
    const unsigned int left_digits = static_cast<unsigned int>(std::ceil(static_cast<double>(left.size()) / 2.0));
    const unsigned int right_digits = static_cast<unsigned int>(std::ceil(static_cast<double>(right.size()) / 2.0));
    unsigned int number_of_superpalindromes = 0;
#if 0
    // 1 1
    // 2 4
    // 3 9
    // 11 121
    // 22 484
    // 101 10201
    // 111 12321
    // 121 14641
    // 202 40804
    // 212 44944
    // 1001 1002001
    // 1111 1234321
    // 2002 4008004
    constexpr int pre_computed[] = { 1, 4, 9, 121, 484, 10201, 12321, 14641, 40804, 44944, 1002001, 1234321, 4008004 };
    unsigned int begin = 0, end;
    for (           ; begin < 13 && pre_computed[begin] < value_left ; ++begin);
    for (end = begin; end   < 13 && pre_computed[end]   < value_right;   ++end);
    number_of_superpalindromes = end - begin;
    for (unsigned int digits = std::max(4u, left_digits); digits <= right_digits; ++digits)
#else
    for (unsigned int digits = left_digits; digits <= right_digits; ++digits)
#endif
        number_of_superpalindromes += superpalindromesValue(digits, value_left, value_right);
    return number_of_superpalindromes;
}

void test(const string &left, const string &right, int solution, unsigned int trials)
{
    int result;
    for (unsigned int t = 0; t < trials; ++t)
        result = superpalindromesInRange(left, right);
    if (result == solution) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("4", "1000", 4, trials);
    test("1", "2", 1, trials);
    test("10", "10000", 2, trials);
    return 0;
}


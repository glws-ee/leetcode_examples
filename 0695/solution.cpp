#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <fmt/core.h>
#include <fmt/color.h>
#include <random>

using namespace std;

int maxAreaOfIsland(std::vector<std::vector<int> > grid)
{
    const int nrows = grid.size();
    if (nrows == 0) return 0;
    const int ncols = grid[0].size();
    if (ncols == 0) return 0;
    int colored_grid[nrows][ncols];
    int coloring_lut[nrows * ncols];
    int number_of_regions = 0;
    
    // Special case: first row.
    if (grid[0][0])
    {
        coloring_lut[number_of_regions] = number_of_regions;
        colored_grid[0][0] = number_of_regions;
        ++number_of_regions;
    }
    else colored_grid[0][0] = -1;
    for (int x = 1; x < ncols; ++x)
    {
        if (grid[0][x])
        {
            if (grid[0][x - 1])
                colored_grid[0][x] = colored_grid[0][x - 1];
            else
            {
                coloring_lut[number_of_regions] = number_of_regions;
                colored_grid[0][x] = number_of_regions;
                ++number_of_regions;
            }
        }
        else colored_grid[0][x] = -1;
    }
    
    for (int y = 1; y < nrows; ++y)
    {
        // Special case: first element of the row.
        if (grid[y][0])
        {
            if (grid[y - 1][0])
                colored_grid[y][0] = colored_grid[y - 1][0];
            else
            {
                coloring_lut[number_of_regions] = number_of_regions;
                colored_grid[y][0] = number_of_regions;
                ++number_of_regions;
            }
        }
        else colored_grid[y][0] = -1;
        // General case.
        for (int x = 1; x < ncols; ++x)
        {
            if (grid[y][x])
            {
                if ((grid[y][x - 1]) || (grid[y - 1][x]))
                {
                    if ((grid[y][x - 1]) && (grid[y - 1][x]))
                    {
                        if (colored_grid[y][x - 1] == colored_grid[y - 1][x])
                            colored_grid[y][x] = colored_grid[y][x - 1];
                        else
                        {
                            int left = colored_grid[y][x - 1];
                            while (coloring_lut[left] != left)
                                left = coloring_lut[left];
                            int up = colored_grid[y - 1][x];
                            while (coloring_lut[up] != up)
                                up = coloring_lut[up];
                            if (left < up)
                            {
                                colored_grid[y][x] = left;
                                coloring_lut[up] = left;
                            }
                            else
                            {
                                colored_grid[y][x] = up;
                                coloring_lut[left] = up;
                            }
                        }
                    }
                    else if (grid[y][x - 1])
                        colored_grid[y][x] = colored_grid[y][x - 1];
                    else colored_grid[y][x] = colored_grid[y - 1][x];
                }
                else
                {
                    coloring_lut[number_of_regions] = number_of_regions;
                    colored_grid[y][x] = number_of_regions;
                    ++number_of_regions;
                }
            }
            else colored_grid[y][x] = -1;
        }
    }
    for (int i = 0; i < number_of_regions; ++i)
    {
        if (coloring_lut[i] != i)
        {
            int aux = i;
            while (coloring_lut[aux] != aux) aux = coloring_lut[aux];
            coloring_lut[i] = aux;
        }
    }
    if (number_of_regions == 0) return 0;
    int histogram[number_of_regions];
    for (int i = 0; i < number_of_regions; ++i)
        histogram[i] = 0;
    for (int y = 0; y < nrows; ++y)
    {
        for (int x = 0; x < ncols; ++x)
        {
            if (colored_grid[y][x] >= 0)
            {
                colored_grid[y][x] = coloring_lut[colored_grid[y][x]];
                ++histogram[colored_grid[y][x]];
            }
        }
    }
    int max_region = 0;
    for (int i = 0; i < number_of_regions; ++i)
        max_region = std::max(max_region, histogram[i]);
#if 0
    // ADD -lfmt TO THE BUILD COMMAND TO AVOID LINKER ERROR.
    int nchrs = static_cast<int>(std::ceil(std::log10(number_of_regions)));
    fmt::color color_show[number_of_regions];
    fmt::color color_available[] = { fmt::color::light_blue, fmt::color::light_coral,
                                     fmt::color::light_cyan, fmt::color::light_green,
                                     fmt::color::light_salmon, fmt::color::light_yellow,
                                     fmt::color::light_sky_blue, fmt::color::orange,
                                     fmt::color::light_sea_green, fmt::color::red,
                                     fmt::color::indigo, fmt::color::golden_rod,
                                     fmt::color::gold, fmt::color::medium_blue,
                                     fmt::color::navy, fmt::color::powder_blue,
                                     fmt::color::plum, fmt::color::spring_green,
                                     fmt::color::yellow_green, fmt::color::sky_blue };
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> distrib(0, 19);
    for (int i = 0; i < number_of_regions; ++i)
        color_show[i] = color_available[distrib(gen)];
    for (int y = 0; y < nrows; ++y)
    {
        for (int x = 0; x < ncols; ++x)
        {
            if (colored_grid[y][x] >= 0)
                fmt::print(bg(color_show[colored_grid[y][x]]) | fg(fmt::color::black), "{:{}}", colored_grid[y][x], nchrs);
            else fmt::print(bg(fmt::color::dark_slate_gray), "{:{}}", ' ', nchrs);
        }
        fmt::print("\n");
    }
#endif
    return max_region;
}

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxAreaOfIsland(grid);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0}, {0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0}, {0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0}},  6,  trials);
    //1...1111.1..111...1.1..111.
    //111...1..1.11...1...1.1.1.1
    //1.1.1....1.111...1..11.11..
    //...111...111.11..1.1.11.1..
    //1...11..1..11..1.11..111.11
    //..1111.11.1...11...11.11.11
    test({{1,0,0,0,1,1,1,1,0,1,0,0,1,1,1,0,0,0,1,0,1,0,0,1,1,1,0},{1,1,1,0,0,0,1,0,0,1,0,1,1,0,0,0,1,0,0,0,1,0,1,0,1,0,1},{1,0,1,0,1,0,0,0,0,1,0,1,1,1,0,0,0,1,0,0,1,1,0,1,1,0,0},{0,0,0,1,1,1,0,0,0,1,1,1,0,1,1,0,0,1,0,1,0,1,1,0,1,0,0},{1,0,0,0,1,1,0,0,1,0,0,1,1,0,0,1,0,1,1,0,0,1,1,1,0,1,1},{0,0,1,1,1,1,0,1,1,0,1,0,0,0,1,1,0,0,0,1,1,0,1,1,0,1,1}}, 18, trials);
    test({{0, 0, 0, 0, 0, 0, 0, 0}}, 0, trials);
    //1..1..1...1..11..1.1..11.1...1..1..1..1..1..11..11
    //.11.1.1111.111......1.111..11....11.1.11..1....11.
    //1..1.111..111..11.1.111..1.1.1..111..111.11.1.111.
    //11.1....11.1...11.1..111.1.111.1..11111.1.1..111.1
    test({{1,0,0,1,0,0,1,0,0,0,1,0,0,1,1,0,0,1,0,1,0,0,1,1,0,1,0,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,0,1,1,0,0,1,1},{0,1,1,0,1,0,1,1,1,1,0,1,1,1,0,0,0,0,0,0,1,0,1,1,1,0,0,1,1,0,0,0,0,1,1,0,1,0,1,1,0,0,1,0,0,0,0,1,1,0},{1,0,0,1,0,1,1,1,0,0,1,1,1,0,0,1,1,0,1,0,1,1,1,0,0,1,0,1,0,1,0,0,1,1,1,0,0,1,1,1,0,1,1,0,1,0,1,1,1,0},{1,1,0,1,0,0,0,0,1,1,0,1,0,0,0,1,1,0,1,0,0,1,1,1,0,1,0,1,1,1,0,1,0,0,1,1,1,1,1,0,1,0,1,0,0,1,1,1,0,1}}, 16, trials);
    return 0;
}

You are given an `m x n` binary matrix `grid`. An island is a group of `1`'s (representing land) connected **4-directionally** (horizontal or vertical). You may assume all four edges of the grid are surrounded by water.

The **area** of an island is the number of cells with a value `1` in the island.

Return *the maximum* ***area*** *of an island in* `grid`. If there is no island, return `0`.

**Example 1:**<br>

|0|0|1|0|0|0|0|1|0|0|0|0|0|
|0|0|0|0|0|0|0|1|1|1|0|0|0|
|0|1|1|0|1|0|0|0|0|0|0|0|0|
|0|1|0|0|1|1|0|0|1|0|1|0|0|
|0|1|0|0|1|1|0|0|1|1|1|0|0|
|0|0|0|0|0|0|0|0|0|0|1|0|0|
|0|0|0|0|0|0|0|1|1|1|0|0|0|
|0|0|0|0|0|0|0|1|1|0|0|0|0|

*Input:* `grid = [[0,0,1,0,0,0,0,1,0,0,0,0,0],[0,0,0,0,0,0,0,1,1,1,0,0,0],[0,1,1,0,1,0,0,0,0,0,0,0,0],[0,1,0,0,1,1,0,0,1,0,1,0,0],[0,1,0,0,1,1,0,0,1,1,1,0,0],[0,0,0,0,0,0,0,0,0,0,1,0,0],[0,0,0,0,0,0,0,1,1,1,0,0,0],[0,0,0,0,0,0,0,1,1,0,0,0,0]]`<br>
*Output:* `6`<br>
*Explanation:* The answer is not 11, because the island must be connected 4-directionally.<br>

**Example 2:**<br>
*Input:* `grid = [[0,0,0,0,0,0,0,0]]`<br>
*Output:* `0`<br>

**Constraints:**<br>
    - `m == grid.length`
    - `n == grid[i].length`
    - `1 <= m, n <= 50`
    - `grid[i][j]` is either `0` or `1`.

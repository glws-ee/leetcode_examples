#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <limits>
#include <functional>

using namespace std;

constexpr int null = -1000;

std::ostream& operator<<(std::ostream &out, const std::vector<int> &vec)
{
    out << '{';
    bool next = false;
    for (int v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

int findMin(std::vector<int> nums)
{
    const int n = nums.size();
    if (n == 1)
        return nums[0];
    else if (n == 2)
        return std::min(nums[0], nums[1]);
    function<int(int, int)> search = [&](int begin, int end)
    {
        for (int iter = 0; iter < n; ++iter)
        {
            if (nums[begin] < nums[end])
                return nums[begin];
            else if (end - begin <= 1)
                return std::min(nums[begin], nums[end]);
            int aux = (begin + end) / 2;
            // ============================================
            //std::cout << nums << '\n';
            //std::cout << ' ';
            //for (int d = 0; d < begin; ++d)
            //    std::cout << "   ";
            //std::cout << "B";
            //for (int d = begin; d < aux; ++d)
            //    std::cout << "   ";
            //std::cout << "\bM";
            //for (int d = aux; d < end; ++d)
            //    std::cout << "   ";
            //std::cout << "\bE";
            //std::cout << '\n';
            //std::system("read");
            // ============================================
            if (nums[aux] < nums[begin])
                end = aux;
            else if (nums[aux] > nums[end])
                begin = aux;
            else
                return std::min(search(begin, aux - 1), search(aux + 1, end));
        }
        return std::numeric_limits<int>::lowest();
    };
    return search(0, n - 1);
}

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMin(nums);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 5}, 1, trials);
    test({2, 2, 2, 0, 1}, 0, trials);
    test({4, 5, 6, 7, 0, 1, 4}, 0, trials);
    test({1, 4, 4, 5, 6, 7, 0}, 0, trials);
    test({4, 4, 5, 6, 7, 0, 1}, 0, trials);
    test({5, 6, 7, 0, 1, 4, 4}, 0, trials);
    test({0, 1, 4, 4, 5, 6, 7}, 0, trials);
    test({4, 4, 4, 0, 4, 4, 4}, 0, trials);
    test({4, 4, 0, 4, 4, 4, 4}, 0, trials);
    test({4, 4, 4, 4, 0, 4, 4}, 0, trials);
    return 0;
}

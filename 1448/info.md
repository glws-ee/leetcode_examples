=Count Good Nodes in Binary Tree=

Given a binary tree `root`, a node *X* in the tree is named **good** if in the path from root to *X* there are no nodes with a value *greater than* X.

Return the number of **good** nodes in the binary tree.

**Example 1:**<br>

```mermaid 
A[3]->B[1]
A->C[4]
B->D[3]
C->E[1]
C->F[5]
classDef good fill:#ACF;
classDef bad fill:#EEE;
class A,C,D,F good;
class B,E bad;
```
*Input:* `root = [3, 1, 4, 3, null, 1, 5]`<br>
*Output:* `4`<br>
*Explanation:* Nodes in blue are **good**.<br>
Root Node (3) is always a good node.<br>
Node `4 -> (3,4)` is the maximum value in the path starting from the root.<br>
Node `5 -> (3,4,5)` is the maximum value in the path<br>
Node `3 -> (3,1,3)` is the maximum value in the path.<br>

**Example 2:**<br>
```mermaid 
A[3]->B[3]
B->C[4]
B->D[2]
classDef good fill:#ACF;
classDef bad fill:#EEE;
class A,B,C good;
class D bad;
```
*Input:* `root = [3, 3, null, 4, 2]`<br>
*Output:* `3`<br>
*Explanation:* Node `2 -> (3, 3, 2)` is not good, because `3` is higher than it.<br>

**Example 3:**<br>
*Input:* `root = [1]`<br>
*Output:* `1`<br>
*Explanation:* Root is considered as **good**.<br>

**Constrains:**<br>
- The number of nodes in the binary tree is in the range `[1, 10^5]`.
- Each node's value is between `[-10^4, 10^4]`.




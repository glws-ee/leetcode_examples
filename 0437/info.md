=Path Sum III=

Given the `root` of a binary tree and an integer `targetSum`, return *the number of paths where the sum of the values along the path equals* `targetSum`.

The path does not need to start or end at the root or a leaf, but it must go downwards (i.e., traveling only from parent nodes to child nodes).

**Example 1:**<br>
```mermaid 
A((10))-->B((5))
B-->C((3))
B-->D((2))
D-->G((1))
C-->F(( -2))
C-->E((3))
A-->H(( -3))
H-->I((11))
style B fill:#FAA,stroke:#FCA,stroke-width:8px
style C fill:#FAA
style D fill:#FCA
style G fill:#FCA
style H fill:#CCF
style I fill:#CCF
style A fill:#FFF
style F fill:#FFF
style E fill:#FFF
```

*Input:* `root = [10, 5, -3, 3, 2, null, 11, 3, -2, null, 1], targetSum = 8`<br>
*Output:* `3`<br>
*Explanation:* The paths that sum to 8 are shown.<br>

**Example 2:**<br>
*Input:* `root = [5, 4, 8, 11, null, 13, 4, 7, 2, null, null, 5, 1], targetSum = 22`<br>
*Output:* `3`<br>
 
**Constraints:**<br>
- The number of nodes in the tree is in the range `[0, 1000]`.
- `-10^9 <= Node.val <= 10^9`
- `-1000 <= targetSum <= 1000`



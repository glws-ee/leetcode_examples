#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <algorithm>

using namespace std;

int maximumUnits(std::vector<std::vector<int> > boxTypes, int truckSize)
{
#if 0
    std::vector<std::pair<int, int> > info;
    for (const auto &v : boxTypes)
        info.push_back({v[1], v[0]});
    std::sort(info.rbegin(), info.rend());
    int s = 0;
    int result = 0;
    for (const auto &v : info)
    {
        s += v.second;
        result += v.first * v.second;
        if (s >= truckSize)
        {
            result -= v.first * (s - truckSize);
            break;
        }
    }
    return result;
#else
    std::sort(boxTypes.begin(), boxTypes.end(), [](const std::vector<int> &left, const std::vector<int> &right) { return left[1] > right[1]; });
    int s = 0;
    int result = 0;
    for (const auto &v : boxTypes)
    {
        s += v[0];
        result += v[1] * v[0];
        if (s >= truckSize)
        {
            result -= v[1] * (s - truckSize);
            break;
        }
    }
    return result;
#endif
}

void test(std::vector<std::vector<int> > boxTypes, int truckSize, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maximumUnits(boxTypes, truckSize);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 30'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 3}, {2, 2}, {3, 1}}, 4, 8, trials);
    test({{5, 10}, {2, 5}, {4, 7}, {3, 9}}, 10, 91, trials);
    return 0;
}

Given an integer array `nums` that may contain duplicates, return *all possible subsets (the power set)*.

The solution set **must not** contain duplicate subsets. Return the solution in any order.

 
**Example 1:**<br>
*Input:* `nums = [1, 2, 2]`<br>
*Output:* `[[], [1], [1, 2], [1, 2, 2], [2], [2, 2]]`<br>

**Example 2:**<br>
Input: `nums = [0]`<br>
Output: `[[], [0]]`<br>
 
**Constraints:**<br>
- `1 <= nums.length <= 10`
- `-10 <= nums[i] <= 10`



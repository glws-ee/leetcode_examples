#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <unordered_set>

using namespace std;

void recursiveSolution(std::vector<int> &nums, std::vector<int> &aux, int idx, int n, std::vector<std::vector<int> > &result)
{
    result.push_back(aux);
    if (idx < n)
    {
        for (int i = idx; i < n; ++i)
        {
            if ((i > idx) && (nums[i] == nums[i - 1])) continue;
            aux.push_back(nums[i]);
            recursiveSolution(nums, aux, i + 1, n, result);
            aux.pop_back();
        }
    }
}

std::vector<std::vector<int> > subsetsWithDup(std::vector<int> nums)
{
#if 0 // 4ms (LC) | 620ms (Local)
    int n = nums.size();
    std::string nums_str(n, 0);
    for (int i = 0; i < n; ++i)
        nums_str[i] = 'a' + nums[i] + 10;
    sort(nums_str.begin(), nums_str.end());
    std::unordered_set<std::string> seen;
    char buffer[n + 1];
    for (int mask = 0, size = 1 << n; mask < size; ++mask)
    {
        char * ptr = buffer;
        for (int i = 0; i < n; ++i)
            if ((mask >> i) & 1)
                *(ptr++) = nums_str[i];
        *ptr = '\0';
        if (std::string code(buffer); seen.find(code) == seen.end())
            seen.insert(code);
    }
    std::vector<std::vector<int> > result(seen.size());
    for (int idx = 0; const auto &s : seen)
    {
        const int l = s.size();
        result[idx].resize(l);
        for (int k = 0; k < l; ++k)
            result[idx][k] = static_cast<int>(s[k] - 'a') - 10;
        ++idx;
    }
    return result;
#elif 1 // 4ms (LC) | 194ms (Local)
    std::vector<std::vector<int> > result;
    std::vector<int> aux;
    aux.reserve(nums.size());
    std::sort(nums.begin(), nums.end());
    recursiveSolution(nums, aux, 0, nums.size(), result);
    return result;
#elif 0 // 4ms (LC) | 484ms (Local)
    std::sort(nums.begin(), nums.end());
    std::vector<std::vector<int> > result{{}};
    for (auto num : nums)
    {
        int size = result.size();
        for (int i = 0; i < size; ++i)
        {
            std::vector<int> aux = result[i];
            aux.push_back(num);
            if (std::find(result.begin(), result.end(), aux) == result.end())
                result.push_back(aux);
        }
    }
    return result;
#elif 0 // 4ms (LC) | 500 ms
    int n = nums.size();
    std::string nums_str(n, 0);
    for (int i = 0; i < n; ++i)
        nums_str[i] = 'a' + nums[i] + 10;
    sort(nums_str.begin(), nums_str.end());
    std::unordered_set<std::string> seen;
    std::vector<std::string> result_str{""};
    for (char num : nums_str)
    {
        int size = result_str.size();
        for (int i = 0; i < size; ++i)
        {
            if (std::string aux = result_str[i] + num; seen.find(aux) == seen.end())
            {
                seen.insert(aux);
                result_str.push_back(aux);
            }
        }
    }
    const int N = result_str.size();
    std::vector<std::vector<int> > result(N);
    for (int i = 0; i < N; ++i)
    {
        const int l = result_str[i].size();
        result[i].resize(l);
        for (int j = 0; j < l; ++j)
            result[i][j] = static_cast<int>(result_str[i][j] - 'a') - 10;
    }
    return result;
#endif
}

bool operator==(const std::vector<std::vector<int> > &left, const std::vector<std::vector<int> > &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    std::unordered_set<std::string> left_set, right_set;
    auto vec2str = [](const std::vector<int> &vec)
    {
        const int l = vec.size();
        std::string text(l, '0');
        for (int j = 0; j < l; ++j)
            text[j] = 'a' + vec[j] + 10;
        return text;
    };
    for (int i = 0; i < n; ++i)
    {
        left_set.emplace(vec2str(left[i]));
        right_set.emplace(vec2str(right[i]));
    }
    if ((n != left_set.size()) || (n != right_set.size())) return false;
    for (const std::string &key : left_set)
        if (right_set.find(key) == right_set.end()) return false;
    return true;
}

std::ostream& operator<<(std::ostream &out, const std::vector<std::vector<int> > &vec)
{
    out << '{';
    bool next_out = false;
    for (const auto &invec : vec)
    {
        if (next_out) [[likely]] out << ", ";
        next_out = true;
        out << '{';
        bool next_in = false;
        for (int v : invec)
        {
            if (next_in) [[likely]] out << ", ";
            next_in = true;
            out << v;
        }
        out << '}';
    }
    out << '}';
    return out;
}

void test(std::vector<int> nums, std::vector<std::vector<int> > solution, unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = subsetsWithDup(nums);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 100'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 2}, {{}, {1}, {1, 2}, {1, 2, 2}, {2}, {2, 2}}, trials);
    test({0}, {{}, {0}}, trials);
    test({1, 2, 3}, {{}, {1}, {1, 2}, {1, 2, 3}, {1, 3}, {2}, {2, 3}, {3}}, trials);
    test({4, 4, 4, 1, 4}, {{}, {1}, {1, 4}, {1, 4, 4}, {1, 4, 4, 4}, {1, 4, 4, 4, 4}, {4}, {4, 4}, {4, 4, 4}, {4, 4, 4, 4}}, trials);
    test({2, 1, 2, 1, 3}, {{}, {1}, {1, 1}, {1, 1, 2}, {1, 1, 2, 2}, {1, 1, 2, 2, 3}, {1, 1, 2, 3}, {1, 1, 3}, {1, 2}, {1, 2, 2}, {1, 2, 2, 3}, {1, 2, 3}, {1, 3}, {2}, {2, 2}, {2, 2, 3}, {2, 3}, {3}}, trials);
    return 0;
}

#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <unordered_map>

using namespace std;

constexpr int null = -1000;

int numDecodings(const std::string &s, int idx, int n, std::unordered_map<int, int> &memory)
{
    if (auto it = memory.find(idx); it != memory.end()) return it->second;
    if (idx == n)
        return 1;
    else if (idx > n)
        return 0;
    // 10 to 19
    if (s[idx] == '1')
        return (memory[idx + 1] = numDecodings(s, idx + 1, n, memory))
             + (memory[idx + 2] = numDecodings(s, idx + 2, n, memory));
    // 20 to 26
    else if (s[idx] == '2')
        return (((idx + 1 < n) && (s[idx + 1] <= '6'))?(memory[idx + 2] = numDecodings(s, idx + 2, n, memory)):0)
             + (memory[idx + 1] = numDecodings(s, idx + 1, n, memory));
    // 3 to 9
    else if ((s[idx] >= '3') && (s[idx] <= '9'))
        return (memory[idx + 1] = numDecodings(s, idx + 1, n, memory));
    // 0 is error.
    else return 0;
}

int numDecodings(std::string s)
{
    std::unordered_map<int, int> memory;
    return numDecodings(s, 0, s.size(), memory);
}

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = numDecodings(s);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("12", 2, trials);
    test("226", 3, trials);
    test("0", 0, trials);
    test("06", 0, trials);
    test("29273192739102981428719231329273192739129814287192313", 2048, trials);
    return 0;
}

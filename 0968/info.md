Given a binary tree, we install cameras on the nodes of the tree. 
Each camera at a node can monitor **its parent, itself, and its immediate children**.
Calculate the minimum number of cameras needed to monitor all nodes of the tree.

**Example 1:**<br>
```mermaid
graph TB
A[ ] --> B[:camera:]
B --> C[ ]
B --> D[ ]
```

*Input:* `[0, 0, null, 0, 0]`<br>
*Output:* `1`<br>
*Explanation:* One camera is enough to monitor all nodes if placed as show.

**Example 2:**<br>
```mermaid
graph TD
A[ ]-->B[:camera:]
B-->C[ ]
C-->D[:camera:]
D-->E[ ]
```

*Input:* `[0, 0, null, 0, null, 0, null, null, 0]`<br>
*Output:* `2`<br>
*Explanation:* At least two cameras are needed to monitor all nodes of the tree. The above image shows one of the valid configurations of camera placement.

**Note:**
    - The number of nodes in the given tree will be in the range `[1, 1000]`.
    - **Every** node has value 0.

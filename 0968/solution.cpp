#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

struct TreeNode
{
    int val = 0;
    TreeNode * left = nullptr;
    TreeNode * right = nullptr;
    TreeNode(void) = default;
    TreeNode(int v) : val(v), left(nullptr), right(nullptr) {}
    TreeNode(int v, const TreeNode * l, const TreeNode * r) :
        val(v),
        left((l != nullptr)?new TreeNode(*l):nullptr),
        right((r != nullptr)?new TreeNode(*r):nullptr) {}
    TreeNode(const TreeNode &other) :
        val(other.val),
        left((other.left != nullptr)?new TreeNode(*other.left):nullptr),
        right((other.right != nullptr)?new TreeNode(*other.right):nullptr) {}
    TreeNode(TreeNode &&other) :
        val(other.val),
        left(std::exchange(other.left, nullptr)),
        right(std::exchange(other.right, nullptr)) {}
    ~TreeNode(void) { delete left; delete right; }
    TreeNode& operator=(const TreeNode &other)
    {
        if (this != &other)
        {
            delete left;
            delete right;
            val = other.val;
            left = (other.left != nullptr)?new TreeNode(*other.left):nullptr;
            right = (other.right != nullptr)?new TreeNode(*other.right):nullptr;
        }
        return *this;
    }
    TreeNode& operator=(TreeNode &&other)
    {
        if (this != &other)
        {
            val = other.val;
            left = std::exchange(other.left, nullptr);
            right = std::exchange(other.right, nullptr);
        }
        return *this;
    }
};

TreeNode * vec2tree(const std::vector<int> &tree)
{
    std::queue<TreeNode *> active_nodes;
    
    TreeNode * result = nullptr;
    bool left = true;
    for (const auto &v : tree)
    {
        if (v != null)
        {
            if (result == nullptr)
            {
                result = new TreeNode(v);
                active_nodes.push(result);
                active_nodes.push(result);
            }
            else
            {
                TreeNode * current = new TreeNode(v);
                if (left) active_nodes.front()->left = current;
                else active_nodes.front()->right = current;
                active_nodes.pop();
                active_nodes.push(current);
                active_nodes.push(current);
                left = !left;
            }
        }
        else
        {
            active_nodes.pop();
            left = !left;
        }
    }
    return result;
}

std::vector<int> tree2vec(TreeNode * root)
{
    if (root == nullptr) return {};
    std::vector<int> result;
    std::queue<TreeNode *> active_nodes;
    active_nodes.push(root);
    result.push_back(root->val);
    while (!active_nodes.empty())
    {
        TreeNode * current = active_nodes.front();
        active_nodes.pop();
        if (current->left != nullptr)
        {
            active_nodes.push(current->left);
            result.push_back(current->left->val);
        }
        else result.push_back(null);
        if (current->right != nullptr)
        {
            active_nodes.push(current->right);
            result.push_back(current->right->val);
        }
        else result.push_back(null);
    }
    while (result.back() == null) result.pop_back();
    return result;
}

std::ostream& operator<<(std::ostream &out, const std::vector<int> &vec)
{
    out << '{';
    bool next = false;
    for (const auto &v : vec)
    {
        if (next) out << ", ";
        out << v;
        next = true;
    }
    out << '}';
    return out;
}

// ################################################################################################
// ################################################################################################

char search(TreeNode * current, int &count)
{
    // Only reached when exited the tree through a leaf.
    if (current == nullptr) return 0;
    // Aggregate the statuses of the leafs.
    int status = search(current->left, count) + search(current->right, count);
    // The current node is covered but it does not provide coverage for the parent.
    if (status == 0) return 3;
    // The current node provides coverage for the parent.
    if (status < 3) return 0;
    // Place a camera.
    ++count;
    return 1;
}

int minCameraCover(TreeNode* root)
{
    // Cases:
    // A) All children are monitored.
    // B) Some children needs monitoring.
    // C) Some children provide coverage for the current node.
    int count = 0;
    int status = search(root, count);
    return count + (status > 2);
}
// ################################################################################################
// ################################################################################################

void test(const std::vector<int> &tree, int solution, unsigned int trials = 1)
{
    std::cout << "INPUT: " << tree << '\n';
    int result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = minCameraCover(root);
        delete root;
    }
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 0, null, 0, 0}, 1, trials);
    test({0,0,null,0,null,0,null,null,0}, 2, trials);
    //        0
    //    C       C
    //  0   0   0   0
    test({0, 0, 0, 0, 0, 0, 0}, 2, trials);
    //        C
    //    0       0
    //  C   C   C   C
    // 0 0 0 0 0 0 0 0
    test({0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 5, trials);
    return 0;
}

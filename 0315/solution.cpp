#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <set>
#include "testA.hpp"
#include "testB.hpp"
#include "testC.hpp"
#include "testD.hpp"

using namespace std;

std::vector<int> countSmaller(std::vector<int> nums)
{
#if 1
    constexpr int max_value = 10'001;
    constexpr int range = 100'001;
    const int n = nums.size();
    int binary_tree[range];
    std::memset(binary_tree, 0, sizeof(binary_tree));
    std::vector<int> result(nums.size(), 0);
    
    for (int i = n - 1; i >= 0; --i)
    {
        int value = nums[i] + max_value; // No negatives.
        result[i] = 0;
        for (int j = value - 1; j > 0; j -= j & (-j))
            result[i] += binary_tree[j];
        for (int j = value; j <= range; j += j & (-j))
            ++binary_tree[j];
    }
    return result;
#else
    const int n = nums.size();
    std::vector<int> result(nums.size(), 0);
    std::set<int, std::greater<int> > elements;
    for (int i = n - 1; i >= 0; --i)
    {
        result[i] = std::distance(elements.upper_bound(nums[i]), elements.end());
        elements.insert(nums[i]);
    }
    return result;
#endif
}

std::ostream& operator<<(std::ostream &out, const std::vector<int> &info)
{
    out << '{';
    bool next = false;
    int idx = 0;
    for (int v : info)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        if (idx == 9) out << "..., ";
        out << v;
        if (idx == 9) break;
        ++idx;
    }
    out << '}';
    return out;
}

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    const std::size_t n = left.size();
    for (std::size_t i = 0; i < n; ++i)
        if (left[i] != right[i]) return false;
    return true;
}

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = countSmaller(nums);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 2, 6, 1}, {2, 1, 1, 0}, trials);
    test({-1}, {0}, trials);
    test({-1, -1}, {0, 0}, trials);
    test({739,5635,3182,3135,6241,-4300,9806,1576,-4212,-9211,9687,-9930,-4595,-2460,327,6413,-9680,-1423,9038,-8381,-782,-868,2634,303,3902,1590,-8316,9615,-7211,-9639,-7223,10000,-8241,4091,-3711,2344,9859,9170,-8523,2567,-8340,6027,7357,5287,8987,4628,8729,4242,-6264,-7325,-8753,1698,6597,7190,9520,9577,4170,-394,-1438,-9903,22,7089,8784,3991,7011,6907,-4578,-2589,7006,6554,4422,-6224,7160,7156,5160,4363,-9279,8681,-9308,8490,1498,-9139,-3227,8215,8537,4804,-5620,-2734,2215,170,8272,1802,7566,-3316,9240,8915,3211,-2897,-7283,5801}, {}, trials);
    test(testA::nums, testA::solution, trials);
    test(testB::nums, testB::solution, trials);
    test(testC::nums, testC::solution, trials);
    test(testD::nums, testD::solution, trials);
    return 0;
}

#include <vector>
#include <cstring>
#include <iostream>
#include <fstream>
#include <random>
#include <stack>
#include <queue>
#include <utility>
#include <set>

std::vector<int> countSmaller(std::vector<int> nums)
{
    const int n = nums.size();
    std::vector<int> result(nums.size(), 0);
    std::set<int, std::greater<int> > elements;
    for (int i = n - 1; i >= 0; --i)
    {
        result[i] = std::distance(elements.upper_bound(nums[i]), elements.end());
        elements.insert(nums[i]);
    }
    return result;
}

int main(int, char **)
{
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> distrib(-9'000, 9'000);
    const int n = 90'000;
    std::vector<int> nums(n);
    std::vector<std::string> test_names = { "testA", "testB", "testC", "testD" };
    for (auto &name : test_names)
    {
        for (int i = 0; i < n; ++i)
            nums[i] = distrib(gen);
        std::vector<int> solution = countSmaller(nums);
        std::ofstream file(name + ".hpp");
        file << "namespace " << name << '\n';
        file << "{\n";
        file << "    const std::vector<int> nums = {";
        bool next = false;
        for (int v : nums)
        {
            if (next) [[likely]] file << ", ";
            next = true;
            file << v;
        }
        file << "};\n";
        file << "    const std::vector<int> solution = {";
        next = false;
        for (int v : solution)
        {
            if (next) [[likely]] file << ", ";
            next = true;
            file << v;
        }
        file << "};\n";
        file << "}\n";
        file.close();
    }
    return 0;
}

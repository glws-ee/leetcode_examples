#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <functional>
#include <unordered_map>

using namespace std;

constexpr int null = -1000;

int longestCommonSubsequence(std::string text1, std::string text2)
{
#if 1
    const short n1 = text1.size();
    const short n2 = text2.size();
    short dc[n2 + 1];
    for (short j = 0; j <= n2; ++j) dc[j] = 0;
    for (short i = 0; i < n1; ++i)
    {
        short previous = 0;
        for (short j = 1; j <= n2; ++j)
        {
            short aux = dc[j];
            if (text1[i] == text2[j - 1])
                dc[j] = previous + 1;
            else
                dc[j] = std::max<short>(dc[j], dc[j - 1]);
            previous = aux;
        }
    }
    return dc[n2];
#elif 0
    const short n1 = text1.size();
    const short n2 = text2.size();
    short dc[n2];
    for (short j = 0; j < n2; ++j)
        dc[j] = 0;
    for (short i = 0; i < n1; ++i)
    {
        short previous = 0;
        for (short j = 0; j < n2; ++j)
        {
            short aux = dc[j];
            if (text1[i] == text2[j])
                dc[j] = previous + 1;
            else
                dc[j] = std::max<short>(dc[j], (j > 0)?dc[j - 1]:0);
            previous = aux;
        }
    }
    return dc[n2 - 1];
#else
    const short n1 = text1.size();
    const short n2 = text2.size();
    std::unordered_map<int, short> memo;
    function<short(short, short, short)> lcs = [&](short idx1, short idx2, short common) -> short
    {
        if ((idx1 >= n1) || (idx2 >= n2)) return common;
        const int key = static_cast<int>(idx1) << 20
                      | static_cast<int>(idx2) << 10
                      | static_cast<int>(common);
        if (auto it = memo.find(key); it != memo.end()) return it->second;
        
        short result = 0;
        if (text1[idx1] == text2[idx2]) result = lcs(idx1 + 1, idx2 + 1, common + 1);
        result = std::max(result, lcs(idx1 + 1, idx2, common));
        result = std::max(result, lcs(idx1, idx2 + 1, common));
        return memo[key] = result;
    };
    return lcs(0, 0, 0);
#endif
}

void test(std::string text1, std::string text2, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestCommonSubsequence(text1, text2);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
    const std::string testA_text1 = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
    const std::string testA_text2 = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 10'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abcde", "ace", 3, trials);
    test("abc", "abc", 3, trials);
    test("abc", "def", 0, trials);
    test("ABCDGH", "AEDFHR", 3, trials);
    test("AGGTAB", "GXTXAYB", 4, trials);
    test("XMJYAUZ", "MZJAWXU", 4, trials);
    test("acadb", "cbda", 2, trials);
    test(testA_text1, testA_text2, 210, trials);
    return 0;
}

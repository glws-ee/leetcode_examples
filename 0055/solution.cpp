#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <fmt/core.h>

using namespace std;

constexpr int null = -1000;

bool canJump(std::vector<int> nums)
{
#if 1
    std::ios::sync_with_stdio(0);
    std::cin.tie(0);
    std::cout.tie(0);
    const int n = nums.size();
    int reach = 0;
    for (int i = 0; i < n; ++i)
    {
        if (reach >= n - 1) return true;
        if ((i == reach) && (nums[i] == 0)) return false;
        reach = std::max(reach, i + nums[i]);
    }
    return false;
#elif 0
    const int n = nums.size();
    int reach = 0;
    for (int i = 0; (i <= reach) && (i < n); ++i)
        reach = std::max(reach, i + nums[i]);
    return reach >= n - 1;
#elif 0
    int n = nums.size();
    bool reach_end[n];
    std::memset(reach_end, false, sizeof(reach_end));
    reach_end[n - 1] = true;
    
    for (int i = n - 2; i >= 0; --i)
    {
        const int end = std::min(n, i + nums[i] + 1);
        bool reachable = false;
        for (int j = i + 1; !reachable && (j < end); ++j)
            reachable = reach_end[j];
        reach_end[i] = reachable;
        if (reachable) n = i + 1;
    }
    return reach_end[0];
#else
    int n = nums.size();
    bool reach_end[n];
    std::memset(reach_end, false, sizeof(reach_end));
    reach_end[n - 1] = true;
    
    for (int i = 0; i < n; ++i)
        fmt::print("{:3}", i);
    fmt::print("\n");
    for (int n : nums)
        fmt::print("{:3}", n);
    fmt::print("\n");
    for (int i = n - 2; i >= 0; --i)
    {
        const int end = std::min(n, i + nums[i] + 1);
        bool reachable = false;
        for (int j = i + 1; !reachable && (j < end); ++j)
            reachable = reach_end[j];
        reach_end[i] = reachable;
        if (reachable) n = i + 1;
        fmt::print("IDX={:<3} end={:<3} reachable={}\n", i, end, reachable);
    }
    return reach_end[0];
#endif
}

void test(std::vector<int> nums, bool solution, unsigned int trials = 1)
{
    bool result;
    for (unsigned int i = 0; i < trials; ++i)
        result = canJump(nums);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 3, 1, 1, 4}, true , trials);
    test({3, 2, 1, 0, 4}, false, trials);
    return 0;
}


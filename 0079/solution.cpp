#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <functional>

using namespace std;

constexpr int null = -1000;

bool exist(std::vector<std::vector<char> > board, std::string word)
{
#if 1
    const int m = board.size();
    const int n = board[0].size();
    const int ws = word.size();
    function<bool(int, int, int)> search = [&](int x, int y, int p) -> bool
    {
        if (p == ws) return true;
        if ((x < 0) || (x >= m) || (y < 0) || (y >= n)) return false;
        
        bool result = false;
        if (word[p] == board[x][y])
        {
            board[x][y] = '%';
            result = search(x - 1, y    , p + 1)
                  || search(x    , y - 1, p + 1)
                  || search(x + 1, y    , p + 1)
                  || search(x    , y + 1, p + 1);
            board[x][y] = word[p];
        }
        return result;
    };
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            if (search(i, j, 0))
                return true;
    return false;
#else
    const int m = board.size();
    const int n = board[0].size();
    const int ws = word.size();
    std::vector<std::vector<bool> > visited(m);
    for (int i = 0; i < m; ++i)
        visited[i].resize(n, false);
    function<bool(int, int, int)> search = [&](int x, int y, int p) -> bool
    {
        if (p == ws) return true;
        if ((x < 0) || (x >= m) || (y < 0) || (y >= n)) return false;
        if (visited[x][y]) return false;
        
        if (word[p] == board[x][y])
        {
            visited[x][y] = true;
            ++p;
            if (search(x - 1, y    , p)) return true;
            if (search(x    , y - 1, p)) return true;
            if (search(x + 1, y    , p)) return true;
            if (search(x    , y + 1, p)) return true;
            visited[x][y] = false;
        }
        return false;
    };
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            if (search(i, j, 0))
                return true;
    return false;
#endif
}

void test(std::vector<std::vector<char> > board, std::string word, bool solution, unsigned int trials = 1)
{
    bool result;
    for (unsigned int i = 0; i < trials; ++i)
        result = exist(board, word);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{'A', 'B', 'C', 'E'}, {'S', 'F', 'C', 'S'}, {'A', 'D', 'E', 'E'}}, "ABCCED", true, trials);
    test({{'A', 'B', 'C', 'E'}, {'S', 'F', 'C', 'S'}, {'A', 'D', 'E', 'E'}}, "SEE", true, trials);
    test({{'A', 'B', 'C', 'E'}, {'S', 'F', 'C', 'S'}, {'A', 'D', 'E', 'E'}}, "ABCB", false, trials);
    return 0;
}

=Word Search=

Given an `m x n` grid of characters `board` and a string `word`, return `true` if word *exists in the grid*.

The word can be constructed from letters of sequentially adjacent cells, where adjacent cells are horizontally or vertically neighboring. The same letter cell may not be used more than once.

**Example 1:**<br>
|A{set:cellbgcolor:#ffaa00}|B{set:cellbgcolor:#ffaa00}|C{set:cellbgcolor:#ffaa00}|E|
|-|-|-|-|
|S|F|C{set:cellbgcolor:#ffaa00}|S|
|A|D{set:cellbgcolor:#ffaa00}|E{set:cellbgcolor:#ffaa00}|E|

*Input:* `board = [["A", "B", "C", "E"], ["S", "F", "C", "S"], ["A", "D", "E", "E"]],  word = "ABCCED"`<br>
*Output:* `true`<br>

**Example 2:**<br>
|A|B|C|E|
|-|-|-|-|
|S|F|C|S{set:cellbgcolor:#ffaa00}|
|A|D|E{set:cellbgcolor:#ffaa00}|E{set:cellbgcolor:#ffaa00}|

*Input:* `board = [["A", "B", "C", "E"], ["S", "F", "C", "S"], ["A", "D", "E", "E"]],  word = "SEE"`<br>
*Output:* `true`<br>

**Example 3:**<br>
|A|B|C|E|
|-|-|-|-|
|S|F|C|S|
|A|D|E|E|

*Input:* `board = [["A", "B", "C", "E"], ["S", "F", "C", "S"], ["A", "D", "E", "E"]],  word = "ABCB"`<br>
*Output:* `false`<br>
 
**Constraints:**<br>
- `m == board.length`
- `n = board[i].length`
- `1 <= m, n <= 6`
- `1 <= word.length <= 15`
- `board` and `word` consists of only lowercase and uppercase English letters.
 
**Follow up:** Could you use search pruning to make your solution faster with a larger `board`?<br>




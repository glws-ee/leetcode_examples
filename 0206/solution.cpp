#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

struct ListNode
{
    int val = 0;
    ListNode * next = nullptr;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
    ~ListNode(void) { delete next; }
};

ListNode * reverseList(ListNode * head)
{
    ListNode * previous = nullptr;
    for (ListNode * current = head; current != nullptr;)
    {
        ListNode * next = current->next;
        current->next = previous;
        previous = current;
        current = next;
    }
    return previous;
}

ListNode * vec2list(const std::vector<int> &vec)
{
    if (vec.size() == 0) return nullptr;
    else
    {
        const int n = vec.size();
        ListNode * l = new ListNode(vec[0]);
        ListNode * current = l;
        for (int i = 1; i < n; ++i)
            current = current->next = new ListNode(vec[i]);
        return l;
    }
}

std::vector<int> list2vec(const ListNode * l)
{
    int n = 0;
    for (const ListNode * current = l; current != nullptr; ++n, current = current->next);
    std::vector<int> vec(n);
    n = 0;
    for (const ListNode * current = l; current != nullptr; ++n, current = current->next)
        vec[n] = current->val;
    return vec;
}

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    for (int i = 0; i < n; ++i)
        if (left[i] != right[i]) return false;
    return true;
}

std::ostream& operator<<(std::ostream &out, const std::vector<int> &vec)
{
    out << '{';
    bool next = false;
    for (int v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

void test(std::vector<int> input, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * linput = vec2list(input);
        ListNode * lr = reverseList(linput);
        result = list2vec(lr);
        delete lr;
    }
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, {5, 4, 3, 2, 1}, trials);
    test({1, 2}, {2, 1}, trials);
    test({}, {}, trials);
    return 0;
}

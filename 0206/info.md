=Reverse Linked List=

Given the `head` of a singly linked list, reverse the list, and return *the reversed list*.

**Example 1:**<br>
*The list*
```mermaid 
A((1))-->B((2))
B-->C((3))
C-->D((4))
D-->E((5))
```
*becomes*<br>
```mermaid 
F((5))-->G((4))
G-->H((3))
H-->I((2))
I-->J((1))
```
*Input:* `head = [1, 2, 3, 4, 5]`<br>
*Output:* `[5, 4, 3, 2, 1]`<br>

**Example 2:**<br>
*The list*
```mermaid 
A((1))-->B((2))
```
*becomes*<br>
```mermaid 
F((2))-->G((1))
```
*Input:* `head = [1, 2]`<br>
*Output:* `[2, 1]`<br>

**Example 3:**<br>
*Input:* `head = []`<br>
*Output:* `[]`<br>
 
**Constraints:**<br>
- The number of nodes in the list is the range `[0, 5000]`.
- `-5000 <= Node.val <= 5000`
 
**Follow up:** A linked list can be reversed either iteratively or recursively. Could you implement both?



#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <limits>

using namespace std;

constexpr int null = -1000;

class MinStack
{
public:
    MinStack(void) :
        m_minimum(std::numeric_limits<int>::max())
    {
        m_data.reserve(30'000);
    }
    void push(int val)
    {
        m_minimum = std::min(m_minimum, val);
        m_data.push_back({val, m_minimum});
    }
    void pop()
    {
        m_data.pop_back();
        m_minimum = (m_data.size() > 0)?m_data.back().second:std::numeric_limits<int>::max();
    }
    int top()
    {
        return m_data.back().first;
    }
    int getMin()
    {
        return m_data.back().second;
    }
protected:
    std::vector<std::pair<int, int> > m_data;
    int m_minimum;
};

enum class OP { MinStack, push, pop, top, getMin };

std::ostream& operator<<(std::ostream &out, const std::vector<int> &vec)
{
    out << '{';
    bool next = false;
    for (int v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    for (int i = 0; i < n; ++i)
        if (left[i] != right[i])
            return false;
    return true;
}

void test(std::vector<OP> operations, std::vector<int> input, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        const int n = operations.size();
        std::vector<int> current_result(n);
        MinStack * stack = nullptr;
        for (int k = 0; k < n; ++k)
        {
            switch (operations[k])
            {
            case OP::MinStack:
                stack = new MinStack();
                current_result[k] = null;
                break;
            case OP::push:
                stack->push(input[k]);
                current_result[k] = null;
                break;
            case OP::pop:
                stack->pop();
                current_result[k] = null;
                break;
            case OP::top:
                current_result[k] = stack->top();
                break;
            case OP::getMin:
                current_result[k] = stack->getMin();
                break;
            }
        }
        delete stack;
        result = current_result;
    }
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::MinStack, OP::push, OP::push, OP::push, OP::getMin, OP::pop, OP::top, OP::getMin},
         {null, -2, 0, -3, null, null, null, null},
         {null, null, null, null, -3, null, 0, -2}, trials);
    test({OP::MinStack, OP::push, OP::push, OP::push, OP::top, OP::pop, OP::getMin, OP::pop, OP::getMin, OP::pop, OP::push, OP::top, OP::getMin, OP::push, OP::top, OP::getMin, OP::pop, OP::getMin},
         {null, 2147483646, 2147483646, 2147483647, null, null, null, null, null, null, 2147483647, null, null, -2147483648, null, null, null, null},
         {null, null, null, null, 2147483647, null, 2147483646, null, 2147483646, null, null, 2147483647, 2147483647, null, -2147483648, -2147483648, null, 2147483647}, trials);
    return 0;
}

Given an `m x n` matrix board containing `'X'` and `'O'`, capture all regions that are 4-directionally surrounded by `'X'`.

A region is captured by flipping all `'O'`s into `'X'`s in that surrounded region.

**Example 1:**<br>
*The board...*<br>
|X{set:cellbgcolor:#92c2de}|X{set:cellbgcolor:#92c2de}|X{set:cellbgcolor:#92c2de}|X{set:cellbgcolor:#92c2de}|
|-|-|-|-|
|X{set:cellbgcolor:#92c2de}|O{set:cellbgcolor:#dec092}|O{set:cellbgcolor:#dec092}|X{set:cellbgcolor:#92c2de}|
|X{set:cellbgcolor:#92c2de}|X{set:cellbgcolor:#92c2de}|O{set:cellbgcolor:#dec092}|X{set:cellbgcolor:#92c2de}|
|X{set:cellbgcolor:#92c2de}|O{set:cellbgcolor:#dec092}|X{set:cellbgcolor:#92c2de}|X{set:cellbgcolor:#92c2de}|
*becomes*<br>
|X{set:cellbgcolor:#92c2de}|X{set:cellbgcolor:#92c2de}|X{set:cellbgcolor:#92c2de}|X{set:cellbgcolor:#92c2de}|
|-|-|-|-|
|X{set:cellbgcolor:#92c2de}|X{set:cellbgcolor:#92c2de}|X{set:cellbgcolor:#92c2de}|X{set:cellbgcolor:#92c2de}|
|X{set:cellbgcolor:#92c2de}|X{set:cellbgcolor:#92c2de}|X{set:cellbgcolor:#92c2de}|X{set:cellbgcolor:#92c2de}|
|X{set:cellbgcolor:#92c2de}|O{set:cellbgcolor:#dec092}|X{set:cellbgcolor:#92c2de}|X{set:cellbgcolor:#92c2de}|

*Input:* `board = [["X", "X", "X", "X"], ["X", "O", "O", "X"], ["X", "X", "O", "X"], ["X", "O", "X", "X"]]`<br>
*Output:* `[["X", "X", "X", "X"], ["X", "X", "X", "X"], ["X", "X", "X", "X"], ["X", "O", "X", "X"]]`<br>
*Explanation:* Surrounded regions should not be on the border, which means that any 'O' on the border of the board are not flipped to 'X'. Any 'O' that is not on the border and it is not connected to an 'O' on the border will be flipped to 'X'. Two cells are connected if they are adjacent cells connected horizontally or vertically.<br>

**Example 2**<br>
*Input:* `board = [["X"]]`<br>
*Output:* `[["X"]]`<br>
 
**Constraints:**<br>
- `m == board.length`
- `n == board[i].length`
- `1 <= m, n <= 200`
- `board[i][j]` is `'X'` or `'O'`.




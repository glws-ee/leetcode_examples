#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

class NumArray
{
    std::vector<int> histogram;
public:
    NumArray(const std::vector<int> &nums) : histogram(nums.size() + 1)
    {
        const int n = nums.size();
        histogram[0] = 0;
        for (int i = 0; i < n; ++i)
            histogram[i + 1] = histogram[i] + nums[i];
    }
    int sumRange(int left, int right)
    {
        return histogram[right + 1] - histogram[left];
    }
};

enum class OP { NumArray, sumRange };

void test(std::vector<OP> operations,
          std::vector<std::vector<int> > input,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    const int n = operations.size();
    if (input.size() != n)
    {
        std::cout << "[FAILURE] Input and operations must have the same length.\n";
        return;
    }
    if (solution.size() != n)
    {
        std::cout << "[FAILURE] Solution and operations must have the same length.\n";
        return;
    }
    for (unsigned int i = 0; i < trials; ++i)
    {
        NumArray * array = nullptr;
        for (int i = 0; i < n; ++i)
        {
            switch (operations[i])
            {
            case OP::NumArray:
                array = new NumArray(input[i]);
                break;
            case OP::sumRange:
            {
                int result = array->sumRange(input[i][0], input[i][1]);
                if (solution[i] == result) std::cout << "[SUCCESS] ";
                else std::cout << "[FAILURE] ";
                std::cout << "expected " << solution[i] << " and obtained " << result << ".\n";
                break;
            }
            default:
                std::cout << "[FAILURE] Unknown operation.\n";
                return;
            }
        }
        delete array;
    }
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::NumArray         , OP::sumRange, OP::sumRange, OP::sumRange},
         {{-2, 0, 3, -5, 2, -1}, {0, 2}      , {2, 5}      , {0, 5}      },
         {null                 , 1           , -1          , -3          }, trials);
    return 0;
}

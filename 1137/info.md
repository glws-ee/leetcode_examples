=N-th Tribonacci Number=

The Tribonacci sequence `T_n` is defined as follows:  `T_0 = 0`, `T_1 = 1`, `T_2 = 1`, and `T_{n + 3} = T_n + T_{n + 1} + T_{n + 2}` for `n >= 0`.

Given `n`, return the value of `T_n`.

**Example 1:**<br>
*Input:* `n = 4`<br>
*Output:* `4`<br>
*Explanation:*<br>
`T_3 = 0 + 1 + 1 = 2`<br>
`T_4 = 1 + 1 + 2 = 4`<br>

**Example 2:**<br>
*Input:* `n = 25`<br>
*Output:* `1389537`<br>
 
**Constraints:**<br>
- `0 <= n <= 37`
- The answer is guaranteed to fit within a 32-bit integer, ie. `answer <= 2^{31} - 1`.




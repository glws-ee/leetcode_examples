#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <functional>
#include <unordered_map>

using namespace std;

constexpr int null = -1000;

int maxLength(std::vector<std::string> arr)
{
    const int N = arr.size();
    int n = 0;
    int arr_bits[N], arr_idx[N];
    for (int i = 0; i < N; ++i)
    {
        arr_idx[n] = i;
        arr_bits[n] = 0;
        bool valid = true;
        for (char c : arr[i])
        {
            int bit = 1 << (c - 'a');
            if ((arr_bits[n] & bit) == 0)
                arr_bits[n] = arr_bits[n] | bit;
            else
            {
                valid = false;
                break;
            }
        }
        if (valid) ++n;
    }
    //std::cout << n << '\n';
    std::unordered_map<long, int> memo;
    function<int(int, int)> dp = [&](int mask, int idx) -> int
    {
        int length = 0;
        const long call_id = static_cast<long>(mask) << 32 | static_cast<long>(idx);
        if (auto it = memo.find(call_id); it != memo.end()) return it->second;
        for (int i = idx; i < n; ++i)
            if ((mask & arr_bits[i]) == 0)
                length = std::max<int>(length, arr[arr_idx[i]].size() + dp(mask | arr_bits[i], idx + 1));
        //std::cout << length << ' ' << n << ' ' << idx << '\n';
        return memo[call_id] = length;
    };
    return dp(0, 0);
}

void test(std::vector<std::string> arr, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxLength(arr);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"un", "iq", "ue"}, 4, trials);
    test({"cha", "r", "act", "ers"}, 6, trials);
    test({"abcdefghijklmnopqrstuvwxyz"}, 26, trials);
    test({"yy", "bkhwmpbiisbldzknpm"}, 0, trials);
    test({"zog","nvwsuikgndmfexxgjtkb","nxko"}, 4, trials);
    return 0;
}

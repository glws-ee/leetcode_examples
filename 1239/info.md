=Maximum Length of a Concatenated String with Unique Characters=

Given an array of strings `arr`. String `s` is a concatenation of a sub-sequence of `arr` which have **unique characters**.

Return *the maximum possible length* of `s`.

**Example 1:**<br>
*Input:* `arr = ["un", "iq", "ue"]`<br>
*Output:* `4`<br>
*Explanation:* All possible concatenations are `""`, `"un"`, `"iq"`, `"ue"`, `"uniq"` and `"ique"`. Maximum length is `4`.<br>

**Example 2:**<br>
*Input:* `arr = ["cha", "r", "act", "ers"]`<br>
*Output:* `6`<br>
*Explanation:* Possible solutions are `"chaers"` and `"acters"`.<br>

**Example 3:**<br>
*Input:* `arr = ["abcdefghijklmnopqrstuvwxyz"]`<br>
*Output:* `26`<br>
 
**Constraints:**<br>
- `1 <= arr.length <= 16`
- `1 <= arr[i].length <= 26`
- `arr[i]` contains only lower case English letters.




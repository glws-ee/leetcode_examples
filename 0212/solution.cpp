#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <functional>
#include <unordered_set>

using namespace std;

constexpr int null = -1000;

bool exist(std::vector<std::vector<char> > &board, const std::string &word)
{
    const int m = board.size();
    const int n = board[0].size();
    const int ws = word.size();
    function<bool(int, int, int)> search = [&](int x, int y, int p) -> bool
    {
        if (p == ws) return true;
        if ((x < 0) || (x >= m) || (y < 0) || (y >= n)) return false;
        
        bool result = false;
        if (word[p] == board[x][y])
        {
            board[x][y] = '%';
            result = search(x - 1, y    , p + 1)
                  || search(x    , y - 1, p + 1)
                  || search(x + 1, y    , p + 1)
                  || search(x    , y + 1, p + 1);
            board[x][y] = word[p];
        }
        return result;
    };
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            if (search(i, j, 0))
                return true;
    return false;
}

class Trie
{
public:
    Trie(void) : m_index(0)
    {
        for (int i = 0; i < m_degree; ++i) m_children[i] = nullptr;
    }
    ~Trie(void)
    {
        for (int i = 0; i < m_degree; ++i) delete m_children[i];
    }
    Trie(const Trie &) = delete;
    Trie(const Trie &&) = delete;
    Trie& operator=(const Trie &) = delete;
    Trie& operator=(const Trie &&) = delete;
    void insert(const std::string &word, short id)
    {
        Trie * ptr = this;
        for (char c : word)
        {
            c -= 'a';
            if (ptr->m_children[c] == nullptr)
                ptr->m_children[c] = new Trie();
            ptr = ptr->m_children[c];
        }
        ptr->m_index = id;
    }
    const Trie * search(char c) const { return m_children[c - 'a']; }
    short index(void) const { return m_index; }
private:
    static constexpr int m_degree = 26;
    Trie * m_children[m_degree];
    short m_index = 0;
};

std::vector<string> findWords(std::vector<std::vector<char> > board, std::vector<string> words)
{
#if 1
    std::vector<std::string> result;
    const int m = board.size();
    const int n = board[0].size();
    Trie trie;
    short nw = 0;
    for (const std::string &w : words)
        trie.insert(w, ++nw);
    char current_word[16];
    std::vector<bool> available(nw, true);
    function<void(int, int, int, const Trie *)> search = [&](int x, int y, int p, const Trie * t)
    {
        if (t == nullptr) return;
        if ((x < 0) || (x >= m) || (y < 0) || (y >= n)) return;
        if (board[x][y] == '%') return;
        if (nw == 0) return;
        
        current_word[p] = board[x][y];
        if (t = t->search(current_word[p]))
        {
            if (short idx = t->index(); idx)
            {
                if (available[idx])
                {
                    current_word[p + 1] = '\0';
                    result.push_back(current_word);
                    available[idx] = false;
                    --nw;
                }
            }
            board[x][y] = '%';
            search(x - 1, y    , p + 1, t);
            search(x    , y - 1, p + 1, t);
            search(x + 1, y    , p + 1, t);
            search(x    , y + 1, p + 1, t);
            board[x][y] = current_word[p];
        }
    };
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            search(i, j, 0, &trie);
    return result;
#else
    std::vector<std::string> result;
    for (const std::string &w : words)
        if (exist(board, w))
            result.push_back(w);
    return result;
#endif
}

bool operator==(const std::vector<std::string> &left, const std::vector<std::string> &right)
{
    std::unordered_set lut(left.begin(), left.end());
    if (lut.size() != right.size()) return false;
    for (const std::string &w : right)
    {
        if (auto it = lut.find(w); it != lut.end())
            lut.erase(it);
        else return false;
    }
    return lut.empty();
}

std::ostream& operator<<(std::ostream &out, const std::vector<std::string> &vec)
{
    out << '{';
    bool next = false;
    for (const std::string &w : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << w;
    }
    out << '}';
    return out;
}

void test(std::vector<std::vector<char> > board,
          std::vector<std::string> words,
          std::vector<std::string> solution,
          unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findWords(board, words);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{'o', 'a', 'a', 'n'},
          {'e', 't', 'a', 'e'},
          {'i', 'h', 'k', 'r'},
          {'i', 'f', 'l', 'v'}},
          {"oath", "pea", "eat", "rain"}, {"eat", "oath"}, trials);
    test({{'a', 'b'}, {'c', 'd'}}, {"abcb"}, {}, trials);
    test({{'o', 'a', 'b', 'n'},
          {'o', 't', 'a', 'e'},
          {'a', 'h', 'k', 'r'},
          {'a', 'f', 'l', 'v'}},
          {"oa", "oaa"}, {"oa", "oaa"}, trials);
    return 0;
}

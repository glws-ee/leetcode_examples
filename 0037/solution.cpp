#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

bool solveSudoku(std::vector<std::vector<char> > &board,
                 bool used_row[9][9],
                 bool used_column[9][9],
                 bool used_board[9][9],
                 int r,
                 int c)
{
    for (; r < 9; ++r)
    {
        for (; c < 9; ++c)
        {
            if (board[r][c] == '.')
            {
                int cell = r / 3 + 3 * (c / 3);
                for (int n = 0; n < 9; ++n)
                {
                    if (!(used_row[r][n] || used_column[c][n] || used_board[cell][n]))
                    {
                        used_row[r][n] = used_column[c][n] = used_board[cell][n] = true;
                        board[r][c] = '1' + n;
                        if (solveSudoku(board, used_row, used_column, used_board, r, c + 1))
                            return true;
                        used_row[r][n] = used_column[c][n] = used_board[cell][n] = false;
                    }
                }
                board[r][c] = '.';
                return false;
            }
        }
        c = 0;
    }
    return true;
}

void solveSudoku(std::vector<std::vector<char> > &board)
{
    bool used_row[9][9], used_column[9][9], used_board[9][9];
    std::memset(used_row, false, sizeof(used_row));
    std::memset(used_column, false, sizeof(used_row));
    std::memset(used_board, false, sizeof(used_row));
    for (int r = 0; r < 9; ++r)
    {
        for (int c = 0; c < 9; ++c)
        {
            if (board[r][c] != '.')
            {
                int idx = board[r][c] - '1';
                used_row[r][idx] = true;
                used_column[c][idx] = true;
                int cell = r / 3 + 3 * (c / 3);
                used_board[cell][idx] = true;
            }
        }
    }
    solveSudoku(board, used_row, used_column, used_board, 0, 0);
}

bool operator==(const std::vector<std::vector<char> > &left,
                const std::vector<std::vector<char> > &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    for (int i = 0; i < n; ++i)
    {
        if (left[i].size() != right[i].size()) return false;
        const int m = left[i].size();
        for (int j = 0; j < m; ++j)
            if (left[i][j] != right[i][j]) return false;
    }
    return true;
}

std::ostream& operator<<(std::ostream &out, const std::vector<std::vector<char> > &board)
{
    out << "\n{";
    bool next_out = false;
    for (const auto &line : board)
    {
        if (next_out) [[likely]] out << "\n,";
        next_out = true;
        bool next_in = false;
        out << '{';
        for (char v : line)
        {
            if (next_in) [[likely]] out << ',';
            next_in = true;
            out << v;
        }
        out << '}';
    }
    out << '}';
    return out;
}

void test(std::vector<std::vector<char> > board,
          std::vector<std::vector<char> > solution, unsigned int trials = 1)
{
    std::vector<std::vector<char> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result = board;
        solveSudoku(result);
    }
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif

    test({{'5','3','.','.','7','.','.','.','.'}
         ,{'6','.','.','1','9','5','.','.','.'}
         ,{'.','9','8','.','.','.','.','6','.'}
         ,{'8','.','.','.','6','.','.','.','3'}
         ,{'4','.','.','8','.','3','.','.','1'}
         ,{'7','.','.','.','2','.','.','.','6'}
         ,{'.','6','.','.','.','.','2','8','.'}
         ,{'.','.','.','4','1','9','.','.','5'}
         ,{'.','.','.','.','8','.','.','7','9'}},
         {{'5','3','4','6','7','8','9','1','2'}
         ,{'6','7','2','1','9','5','3','4','8'}
         ,{'1','9','8','3','4','2','5','6','7'}
         ,{'8','5','9','7','6','1','4','2','3'}
         ,{'4','2','6','8','5','3','7','9','1'}
         ,{'7','1','3','9','2','4','8','5','6'}
         ,{'9','6','1','5','3','7','2','8','4'}
         ,{'2','8','7','4','1','9','6','3','5'}
         ,{'3','4','5','2','8','6','1','7','9'}}, trials);

    return 0;
}

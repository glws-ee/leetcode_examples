#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <bitset>
#include <unordered_set>

using namespace std;

int maxProduct(std::vector<std::string> &words)
{
#if 1
    int maximum_value = 0;
    const int n = words.size();
    int chr_histogram[n];
    for (int i = 0; i < n; ++i)
    {
        chr_histogram[i] = 0;
        for (const auto &c : words[i])
            chr_histogram[i] = chr_histogram[i] | 1 << (c - 'a');
    }
    for (int i = 0; i < n - 1; ++i)
        for (int j = i + 1; j < n; ++j)
            if ((chr_histogram[i] & chr_histogram[j]) == 0)
                maximum_value = std::max(maximum_value, static_cast<int>(words[i].size() * words[j].size()));
    return maximum_value;
#elif 1
    int maximum_value = 0;
    const int n = words.size();
    std::bitset<26> chr_histogram[n];
    for (int i = 0; i < n; ++i)
    {
        chr_histogram[i] = 0;
        for (const auto &c : words[i])
            chr_histogram[i][c - 'a'] = true;
    }
    for (int i = 0; i < n - 1; ++i)
        for (int j = i + 1; j < n; ++j)
            if ((chr_histogram[i] & chr_histogram[j]) == 0)
                maximum_value = std::max(maximum_value, static_cast<int>(words[i].size() * words[j].size()));
    return maximum_value;
#else
    int maximum_value = 0;
    const int n = words.size();
    bool chr_histogram[n][26];
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < 26; ++j)
            chr_histogram[i][j] = false;
        for (const auto &c : words[i])
            chr_histogram[i][c - 'a'] = true;
    }
    for (int i = 0; i < n - 1; ++i)
    {
        for (int j = i + 1; j < n; ++j)
        {
            bool different = true;
            for (int k = 0; different && (k < 26); ++k)
                different = different && !(chr_histogram[i][k] && chr_histogram[j][k]);
            if (different)
                maximum_value = std::max(maximum_value, static_cast<int>(words[i].size() * words[j].size()));
        }
    }
    return maximum_value;
#endif
}

void test(std::vector<std::string> words, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxProduct(words);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"abcw", "baz", "foo", "bar", "xtfn", "abcdef"}, 16, trials);
    test({"a", "ab", "abc", "d", "cd", "bcd", "abcd"}, 4, trials);
    test({"a", "aa", "aaa", "aaaa"}, 0, trials);
    return 0;
}

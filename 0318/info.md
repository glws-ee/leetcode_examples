Given a string array `words`, return *the maximum value of* `length(word[i]) * length(word[j])` *where the two words do not share common letters*. If no such two words exist, return `0`.
 
**Example 1:**<br>
*Input:* `words = ["abcw", "baz", "foo", "bar", "xtfn", "abcdef"]`<br>
*Output:* `16`<br>
*Explanation:* The two words can be `"abcw"`, `"xtfn"`.<br>

**Example 2:**<br>
*Input:* `words = ["a", "ab", "abc", "d", "cd", "bcd", "abcd"]`<br>
*Output:* `4`<br>
*Explanation:* The two words can be `"ab"`, `"cd"`.<br>

**Example 3:**<br>
*Input:* `words = ["a", "aa", "aaa", "aaaa"]`<br>
*Output:* `0`<br>
*Explanation:* No such pair of words.<br>

**Constraints:**<br>
    - `2 <= words.length <= 1000`
    - `1 <= words[i].length <= 1000`
    - `words[i]` consists only of lowercase English letters.

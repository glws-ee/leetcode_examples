#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

int jobScheduling(std::vector<int> startTime, std::vector<int> endTime, std::vector<int> profit)
{
    struct JointData
    {
        int begin = 0;
        int end = 0;
        int profit = 0;
    };
    auto comp = [](const JointData &a, const JointData &b)
    {
        if ((a.begin == b.begin) && (a.end == b.end))
            return a.profit < b.profit;
        else if (a.end == b.end)
            return a.begin < b.begin;
        else return a.end < b.end;
    };
    const int n = startTime.size();
    std::vector<JointData> vec(n);
    int dp[n];
    
    for (int i = 0; i < n; ++i)
    {
        vec[i] = {startTime[i], endTime[i], profit[i]};
        dp[i] = 0;
    }
    std::sort(vec.begin(), vec.end(), comp);
    
    dp[0] = vec[0].profit;
    for(int i=1;i<n;i++)
    {
        int inc = -1, ans = vec[i].profit;
        for (int low = 0, high = i - 1; low <= high;)
        {
            int mid = (low + high) / 2;
            if(vec[mid].end <= vec[i].begin)
            {
                inc = mid;
                low = mid + 1;
            }
            else high = mid - 1;
        }
        if (inc != -1) ans += dp[inc];
        dp[i] = std::max(ans, dp[i - 1]);
    }
    
    return dp[n - 1];
}

void test(std::vector<int> startTime,
          std::vector<int> endTime,
          std::vector<int> profit,
          int solution,
          unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = jobScheduling(startTime, endTime, profit);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 3}, {3, 4, 5, 6}, {50, 10, 40, 70}, 120, trials);
    test({1, 2, 3, 4, 6}, {3, 5, 10, 6, 9}, {20, 20, 100, 70, 60}, 150, trials);
    test({1, 1, 1}, {2, 3, 4}, {5, 6, 4}, 6, trials);
    return 0;
}

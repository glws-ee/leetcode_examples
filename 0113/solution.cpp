#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <unordered_set>

using namespace std;

constexpr int null = -1000;

struct TreeNode
{
    int val = 0;
    TreeNode * left = nullptr;
    TreeNode * right = nullptr;
    TreeNode(void) = default;
    TreeNode(int v) : val(v), left(nullptr), right(nullptr) {}
    TreeNode(int v, const TreeNode * l, const TreeNode * r) :
        val(v),
        left((l != nullptr)?new TreeNode(*l):nullptr),
        right((r != nullptr)?new TreeNode(*r):nullptr) {}
    TreeNode(const TreeNode &other) :
        val(other.val),
        left((other.left != nullptr)?new TreeNode(*other.left):nullptr),
        right((other.right != nullptr)?new TreeNode(*other.right):nullptr) {}
    TreeNode(TreeNode &&other) :
        val(other.val),
        left(std::exchange(other.left, nullptr)),
        right(std::exchange(other.right, nullptr)) {}
    ~TreeNode(void) { delete left; delete right; }
    TreeNode& operator=(const TreeNode &other)
    {
        if (this != &other)
        {
            delete left;
            delete right;
            val = other.val;
            left = (other.left != nullptr)?new TreeNode(*other.left):nullptr;
            right = (other.right != nullptr)?new TreeNode(*other.right):nullptr;
        }
        return *this;
    }
    TreeNode& operator=(TreeNode &&other)
    {
        if (this != &other)
        {
            val = other.val;
            left = std::exchange(other.left, nullptr);
            right = std::exchange(other.right, nullptr);
        }
        return *this;
    }
};

TreeNode * vec2tree(const std::vector<int> &tree)
{
    std::queue<TreeNode *> active_nodes;
    
    TreeNode * result = nullptr;
    bool left = true;
    for (const auto &v : tree)
    {
        if (v != null)
        {
            if (result == nullptr)
            {
                result = new TreeNode(v);
                active_nodes.push(result);
                active_nodes.push(result);
            }
            else
            {
                TreeNode * current = new TreeNode(v);
                if (left) active_nodes.front()->left = current;
                else active_nodes.front()->right = current;
                active_nodes.pop();
                active_nodes.push(current);
                active_nodes.push(current);
                left = !left;
            }
        }
        else
        {
            active_nodes.pop();
            left = !left;
        }
    }
    return result;
}

std::vector<int> tree2vec(TreeNode * root)
{
    if (root == nullptr) return {};
    std::vector<int> result;
    std::queue<TreeNode *> active_nodes;
    active_nodes.push(root);
    result.push_back(root->val);
    while (!active_nodes.empty())
    {
        TreeNode * current = active_nodes.front();
        active_nodes.pop();
        if (current->left != nullptr)
        {
            active_nodes.push(current->left);
            result.push_back(current->left->val);
        }
        else result.push_back(null);
        if (current->right != nullptr)
        {
            active_nodes.push(current->right);
            result.push_back(current->right->val);
        }
        else result.push_back(null);
    }
    while (result.back() == null) result.pop_back();
    return result;
}

std::ostream& operator<<(std::ostream &out, const std::vector<int> &vec)
{
    out << '{';
    bool next = false;
    for (const auto &v : vec)
    {
        if (next) out << ", ";
        out << v;
        next = true;
    }
    out << '}';
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<std::vector<int> > &vec)
{
    out << '{';
    bool next = false;
    for (const auto &v : vec)
    {
        if (next) out << ", ";
        out << v;
        next = true;
    }
    out << '}';
    return out;
}

bool operator==(const std::vector<std::vector<int> > &left, const std::vector<std::vector<int> > &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    std::unordered_set<std::string> left_hash;
    for (int i = 0; i < n; ++i)
    {
        std::string text;
        for (int v : left[i])
            text += std::to_string(v) + ",";
        left_hash.insert(text);
    }
    for (int i = 0; i < n; ++i)
    {
        std::string text;
        for (int v : right[i])
            text += std::to_string(v) + ",";
        if (left_hash.find(text) == left_hash.end()) return false;
    }
    return true;
}

// ################################################################################################
// ################################################################################################

void pathSum(TreeNode * root, int sum, std::vector<int> path, std::vector<std::vector<int> > &result)
{
    if (root != nullptr)
    {
        int diff = sum - root->val;
        if ((root->left == nullptr) && (root->right == nullptr))
        {
            if (diff == 0)
            {
                path.push_back(root->val);
                result.emplace_back(path);
            }
        }
        else
        {
            path.push_back(root->val);
            pathSum(root->left, diff, path, result);
            pathSum(root->right, diff, std::move(path), result);
        }
    }
}

std::vector<std::vector<int> > pathSum(TreeNode* root, int targetSum)
{
    std::vector<std::vector<int> > result;
    pathSum(root, targetSum, {}, result);
    return result;
}

// ################################################################################################
// ################################################################################################

void test(std::vector<int> tree, int targetSum, std::vector<std::vector<int> > solution, unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = pathSum(root, targetSum);
        delete root;
    }
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 4, 8, 11, null, 13, 4, 7, 2, null, null, 5, 1}, 22, {{5, 4, 11, 2}, {5, 8, 4, 5}}, trials);
    test({1, 2, 3}, 5, {}, trials);
    test({1, 2}, 0, {}, trials);
    //         1
    //    -2       -3
    //   1   3  -2    -
    //-1
    test({1, -2, -3, 1, 3, -2, null, -1}, -1, {{1, -2, 1, -1}}, trials);
    return 0;
}

Given the `root` of a binary tree and an integer `targetSum`, return all **root-to-leaf** paths where each path's sum equals `targetSum`.

A **leaf** is a node with no children.

**Example 1:**<br>
```mermaid 
graph TB
A[5] --> B[4]
A --> C[8]
B --> D[11]
D --> E[7]
D --> F[2]
C --> G[13]
C --> H[4]
H --> I[5]
H --> J[1]
```

*Input:* `root = [5, 4, 8, 11, null, 13, 4, 7, 2, null, null, 5, 1], targetSum = 22`<br>
*Output:* `[[5, 4, 11, 2], [5, 8, 4, 5]]`<br>

**Example 2:**<br>
```mermaid 
A[1] --> B[2]
A --> C[3]
```
*Input:* `root = [1, 2, 3], targetSum = 5`<br>
*Output:* `[]`<br>

**Example 3:**<br>
*Input:* `root = [1, 2], targetSum = 0`<br>
*Output:* `[]`<br>

**Constrains**<br>
- The number of nodes in the tree is in the range `[0, 5000]`.
- `-1000 <= Node.val <= 1000`
- `-1000 <= targetSum <= 1000`




=Orderly Queue=

You are given a string `s` and an integer `k`. You can choose one of the first `k` letters of `s` and append it at the end of the string.

Return *the lexicographically smallest string you could have after applying the mentioned step any number of moves*.

**Example 1:**<br>
*Input:* `s = "cba", k = 1`<br>
*Output:* `"acb"`<br>
*Explanation:*<br>
In the first move, we move the 1st character `'c'` to the end, obtaining the string `"bac"`.<br>
In the second move, we move the 1st character `'b'` to the end, obtaining the final result `"acb"`.<br>

**Example 2:**<br>
*Input:* `s = "baaca", k = 3`<br>
*Output:* `"aaabc"`<br>
*Explanation:*<br>
In the first move, we move the 1st character `'b'` to the end, obtaining the string `"aacab"`.<br>
In the second move, we move the 3rd character `'c'` to the end, obtaining the final result `"aaabc"`.<br>
 
**Constraints:**<br>
- `1 <= k <= s.length <= 1000`
- `s` consist of lowercase English letters.




#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

std::string orderlyQueue(std::string s, int k)
{
    if (k >= 2) std::sort(s.begin(), s.end());
    else
    {
        const int n = s.size();
        std::string rotate = s;
        for (int i = 0; i < n; rotate = rotate.substr(1, n - 1) + rotate.substr(0, 1), ++i)
            s = std::min(s, rotate);
    }
    return s;
}

void test(std::string s, int k, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = orderlyQueue(s, k);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("cba", 1, "acb", trials);
    test("baaca", 3, "aaabc", trials);
    return 0;
}

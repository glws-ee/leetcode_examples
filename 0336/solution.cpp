#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <set>
#include <unordered_map>

using namespace std;

bool isPalindrome(std::string word, int begin, int end)
{
    for (int i = begin, j = end; i < j; ++i, --j)
        if (word[i] != word[j]) return false;
    return true;
}

std::vector<std::vector<int> > palindromePairs(std::vector<std::string> words)
{
    const int n = words.size();
    std::vector<std::vector<int> > result;
#if 0
    for (int i = 0; i < n; ++i)
    {
        const int ni = words[i].size();
        for (int j = 0; j < n; ++j)
        {
            if ((i != j) && isPalindrome(words[i] + words[j], 0, ni + words[j].size() - 1))
                result.push_back({i, j});
        }
    }
#else
    std::unordered_map<std::string, int> lut;
    for (int i = 0; i < n; ++i) lut[words[i]] = i;
    for (int i = 0; i < n; ++i)
    {
        if (words[i].size() == 0)
        {
            for (int j = 0; j < n; ++j)
            {
                if ((i != j) && isPalindrome(words[j], 0, words[j].size() - 1))
                {
                    result.push_back({i, j});
                    result.push_back({j, i});
                }
            }
        }
        else
        {
            std::string w(words[i].rbegin(), words[i].rend());
            const int ns = w.size();
            if (auto search = lut.find(w); search != lut.end())
                if (search->second != i)
                    result.push_back({i, search->second});
            for (int j = 1; j < ns; ++j)
            {
                if (isPalindrome(w, 0, j - 1))
                {
                    if (auto search = lut.find(w.substr(j, ns - j)); search != lut.end())
                        result.push_back({i, search->second});
                }
                if (isPalindrome(w, j, ns - 1))
                {
                    if (auto search = lut.find(w.substr(0, j)); search != lut.end())
                        result.push_back({search->second, i});
                }
            }
        }
    }
#endif
    return result;
}

bool operator==(const std::vector<std::vector<int> > &left, const std::vector<std::vector<int> > &right)
{
    if (left.size() != right.size()) return false;
    std::set<std::pair<int, int> > left_unique, right_unique;
    for (const auto &v : left)
        left_unique.insert({v[0], v[1]});
    for (const auto &v : right)
        right_unique.insert({v[0], v[1]});
    if (left_unique.size() != left.size()) return false;
    if (right_unique.size() != right.size()) return false;
    for (auto left_begin = left_unique.begin(), right_begin = right_unique.begin(), end = left_unique.end(); left_begin != end; ++left_begin, ++right_begin)
        if ((left_begin->first != right_begin->first) || (left_begin->second != right_begin->second))
            return false;
    return true;
}

std::ostream& operator<<(std::ostream &out, const std::vector<std::vector<int> > &other)
{
    out << '{';
    bool next_out = false;
    for (const auto &v : other)
    {
        if (next_out) [[likely]] out << ", ";
        next_out = true;
        out << '{';
        bool next_in = false;
        for (const auto &i : v)
        {
            if (next_in) [[likely]] out << ", ";
            next_in = true;
            out << i;
        }
        out << '}';
    }
    out << '}';
    return out;
}

void test(std::vector<std::string> words, std::vector<std::vector<int> > solution, unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = palindromePairs(words);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10000000;
    constexpr unsigned int trials = 10000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"abcd", "dcba", "lls", "s", "sssll"}, {{0, 1}, {1, 0}, {3, 2}, {2, 4}}, trials);
    test({"bat", "tab", "cat"}, {{0, 1}, {1, 0}}, trials);
    test({"a", ""}, {{0, 1}, {1, 0}}, trials);
    
    return 0;
}

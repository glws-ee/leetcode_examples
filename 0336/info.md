Given a list of **unique** words, return all the pairs of the ***distinct*** indices `(i, j)` in the given list, so that the concatenation of the two words `words[i] + words[j]` is a palindrome.

**Example 1:**<br>
*Input:* `words = ["abcd", "dcba", "lls", "s", "sssll"]`<br>
*Output:* `[[0, 1], [1, 0], [3, 2], [2, 4]]`<br>
*Explanation:* The palindromes are `["dcbaabcd", "abcddcba", "slls", "llssssll"]`<br>

**Example 2:**<br>
*Input:* `words = ["bat", "tab", "cat"]`<br>
*Output:* `[[0, 1], [1, 0]]`<br>
*Explanation:* The palindromes are `["battab", "tabbat"]`<br>

**Example 3:**<br>
*Input:* `words = ["a", ""]`<br>
*Output:* `[[0, 1], [1, 0]]`<br>

**Constraints:**<br>
- `1 <= words.length <= 5000`
- `0 <= words[i].length <= 300`
- `words[i]` consists of lower-case English letters.

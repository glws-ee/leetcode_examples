#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <set>
#include <limits>

using namespace std;

constexpr int null = -1000;

int threeSumClosest(std::vector<int> nums, int target)
{
#if 1
    std::sort(nums.begin(), nums.end());
    const int n = nums.size();
    int result = 1'000'000;
    int difference = 1'000'000;
    for (int i = 0; i < n; ++i)
    {
        for (int begin = i + 1, end = n - 1; begin < end;)
        {
            int sum = nums[begin] + nums[end] + nums[i];
            if (int current = std::abs(target - sum); current < difference)
            {
                result = sum;
                difference = current;
                if (difference == 0) break;
            }
            
            if (sum <= target) ++begin;
            else if (sum >= target) --end;
        }
        if (difference == 0) break;
    }
    return result;
#else
    std::sort(nums.begin(), nums.end());
    const int n = nums.size();
    int result = 1'000'000;
    for (int i = 0; i < n; ++i)
    {
        for (int begin = i + 1, end = n - 1; begin < end;)
        {
            int sum = nums[begin] + nums[end] + nums[i];
            if (std::abs(target - sum) < std::abs(target - result))
                result = sum;
            
            if (sum <= target) ++begin;
            else if (sum >= target) --end;
        }
    }
    return result;
#endif
}

void test(std::vector<int> nums, int target, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = threeSumClosest( nums, target);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({-1, 2, 1, -4}, 1, 2, trials);
    test({-1, 2, 1, -4, 3, 1, 6, 3, 7, 9, 12, 4, 1, 4, 5, 6, 1, 6, 8, 3, 6, 69, 29, 21, 593, 53, 41, 52, 63, 79, 29, 593, 392, 293, 491, 29, 4938, 22, 42, 49, 39}, 128, 128, trials);
    test({-3, -2, -5, 3, -5}, -1, -2, trials);
    return 0;
}

#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

int minCostClimbingStairs(std::vector<int> cost)
{
#if 1
    const int n = cost.size();
    //if      (n == 0) return 0;
    //else if (n == 1) return cost[0];
    int prev1 = cost[0];
    int prev2 = cost[1];
    for (int i = 2; i < n; ++i)
    {
        int v = std::min(prev1, prev2) + cost[i];
        prev1 = prev2;
        prev2 = v;
    }
    return std::min(prev1, prev2);
#else
    int prev1 = 0;
    int prev2 = 0;
    for (int c : cost)
    {
        int n = std::min(prev1, prev2) + c;
        prev1 = prev2;
        prev2 = n;
    }
    return std::min(prev1, prev2);
#endif
}

void test(std::vector<int> cost, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minCostClimbingStairs(cost);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    constexpr unsigned int trials = 100000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({10, 15, 20}, 15, trials);
    test({1, 100, 1, 1, 1, 100, 1, 1, 100, 1}, 6, trials);
    return 0;
}

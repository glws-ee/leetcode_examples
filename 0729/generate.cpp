#include <vector>
#include <cstring>
#include <iostream>
#include <fstream>
#include <stack>
#include <queue>
#include <utility>
#include <random>
#include <map>

class MyCalendar {
    int m_start[1000];
    int m_end[1000];
    int m_elements = 0;
public:
    MyCalendar() = default;
    bool book(int start, int end)
    {
        if (start >= end) return true; // Special case: not specified what to do.
        bool available = true;
        for (int i = 0; available && (i < m_elements); ++i)
            if ((end > m_start[i]) && (start < m_end[i]))
                available = (std::min(m_end[i], end) - std::max(m_start[i], start)) <= 0;
        if (available)
        {
            m_start[m_elements] = start;
            m_end[m_elements] = end;
            ++m_elements;
        }
        return available;
    }
};

int main(int, char **)
{
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> distribA(0, 10'000);
    std::uniform_int_distribution<> distribB(2, 50);
    std::vector<std::pair<int, int> > dates(1000);
    std::vector<bool> solution(1000);
    
    for (int i = 0; i < 1000; ++i)
        dates[i] = { distribA(gen), distribB(gen) };
    MyCalendar cal;
    for (int i = 0; i < 1000; ++i)
        solution[i] = cal.book(dates[i].first, dates[i].first + dates[i].second);
    
    std::ofstream file("values.hpp");
    file << "#include <vector>\n";
    file << "namespace testA\n";
    file << "{\n";
    file << "    const std::vector<std::pair<int, int> > dates = {";
    bool next = false;
    for (auto [b, l] : dates)
    {
        if (next) [[likely]] file << ", ";
        next = true;
        file << '{' << b << ", " << b + l << '}';
    }
    file << "};\n";
    file << "    const std::vector<bool> solution = {";
    next = false;
    for (bool s : solution)
    {
        if (next) [[likely]] file << ", ";
        next = true;
        file << static_cast<int>(s);
    }
    file << "};\n";
    file << "}\n";
    file << "\n";
    return 0;
}

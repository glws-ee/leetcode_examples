#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <map>
#include "values.hpp"

using namespace std;

#if 0
class MyCalendar {
    std::map<int, int, std::greater<int> > m_dates;
public:
    MyCalendar() = default;
    bool book(int start, int end)
    {
        if (start >= end) return true; // Special case: not specified what to do.
        bool available = true;
        for (auto it_begin = m_dates.lower_bound(end), it_end = m_dates.end();
                available && (it_begin != it_end);
                ++it_begin)
            if (start < it_begin->second)
                available = (std::min(it_begin->second, end) - std::max(it_begin->first, start)) <= 0;
        if (available)
            m_dates[start] = end;
        return available;
    }
};
#else
class MyCalendar {
    int m_start[1000];
    int m_end[1000];
    int m_elements = 0;
public:
    MyCalendar() = default;
    bool book(int start, int end)
    {
        if (start >= end) return true; // Special case: not specified what to do.
        bool available = true;
        for (int i = 0; available && (i < m_elements); ++i)
            if ((end > m_start[i]) && (start < m_end[i]))
                available = (std::min(m_end[i], end) - std::max(m_start[i], start)) <= 0;
        if (available)
        {
            m_start[m_elements] = start;
            m_end[m_elements] = end;
            ++m_elements;
        }
        return available;
    }
};
#endif

std::ostream& operator<<(std::ostream &out, const std::vector<bool> &vec)
{
    out << '{';
    for (bool v : vec)
        out << ((v)?'T':'F');
    out << '}';
    return out;
}
bool operator==(const std::vector<bool> &first, const std::vector<bool> &second)
{
    if (first.size() != second.size()) return false;
    const int n = first.size();
    for (int i = 0; i < n; ++i)
        if (first[i] != second[i]) return false;
    return true;
}

void test(std::vector<std::pair<int, int> > dates, std::vector<bool> solution, unsigned int trials = 1)
{
    std::vector<bool> result(dates.size());
    for (unsigned int i = 0; i < trials; ++i)
    {
        MyCalendar * cal = new MyCalendar();
        for (int j = 0; j < dates.size(); ++j)
            result[j] = cal->book(dates[j].first, dates[j].second);
        delete cal;
    }
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10000000;
    constexpr unsigned int trials = 30000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{10, 20}, {15, 25}, {20, 30}}, {true, false, true}, trials);
    test(testA::dates, testA::solution, trials);
    return 0;
}

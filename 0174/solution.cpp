#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <functional>
#include <limits>
#include <fmt/core.h>

using namespace std;

constexpr int null = -1000;

int calculateMinimumHP(std::vector<std::vector<int> > dungeon)
{
#if 1
    const int m = dungeon.size();
    const int n = dungeon[0].size();
    int health[n];
    health[n - 1] = dungeon[m - 1][n - 1];
    for (int i = n - 2; i >= 0; --i)
        health[i] = dungeon[m - 1][i] + std::min(0, health[i + 1]);
    for (int i = m - 2; i >= 0; --i)
    {
        health[n - 1] = dungeon[i][n - 1] + std::min(0, health[n - 1]);
        for (int j = n - 2; j >= 0; --j)
            health[j] = dungeon[i][j] + std::min(0, std::max(health[j], health[j + 1]));
    }
    return 1 - std::min(0, health[0]);
#elif 1
    const int m = dungeon.size();
    const int n = dungeon[0].size();
    int health[m][n];
    health[m - 1][n - 1] = dungeon[m - 1][n - 1];
    for (int i = m - 2; i >= 0; --i)
        health[i][n - 1] = dungeon[i][n - 1] + std::min(0, health[i + 1][n - 1]);
    for (int i = n - 2; i >= 0; --i)
        health[m - 1][i] = dungeon[m - 1][i] + std::min(0, health[m - 1][i + 1]);
    for (int i = m - 2; i >= 0; --i)
        for (int j = n - 2; j >= 0; --j)
            health[i][j] = dungeon[i][j] + std::min(0, std::max(health[i + 1][j], health[i][j + 1]));
    return 1 - std::min(0, health[0][0]);
    ////for (int i = 0; i < m; ++i)
    ////{
    ////    for (int j = 0; j < n; ++j)
    ////        fmt::print(" {:3}", dungeon[i][j]);
    ////    fmt::print("\n");
    ////}
    ////fmt::print("-------------------------------\n");
    ////for (int i = 0; i < m; ++i)
    ////{
    ////    for (int j = 0; j < n; ++j)
    ////        fmt::print(" {:3}", health[i][j]);
    ////    fmt::print("\n");
    ////}
#else
    const int m = dungeon.size();
    const int n = dungeon[0].size();
    
    int result_health = std::numeric_limits<int>::lowest();
    std::function<int(int, int, int, int)> cost =[&](int x, int y, int min_health, int health) -> int
    {
        health += dungeon[x][y];
        min_health = std::min(min_health, health);
        if ((x >= m - 1) && (y >= n - 1))
        {
            std::cout << x << ' ' << y << ' ' << min_health << ' ' << health << '\n';
            result_health = std::max(result_health, min_health);
            return min_health;
        }
        int result = min_health;
        if (x < m - 1)
            result = std::min(result, cost(x + 1, y, min_health, health));
        if (y < n - 1)
            result = std::min(result, cost(x, y + 1, min_health, health));
        return result;
    };
    cost(0, 0, 0, 0);
    return 1 - std::min(0, result_health);
#endif
}

void test(std::vector<std::vector<int> > dungeon, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = calculateMinimumHP(dungeon);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{ -2, -3,  3},
          { -5,-10,  1},
          { 10, 30, -5}}, 7, trials);
    test({{ -2, -3,  3, -4},
          { -5,-10, -4,  1},
          { 10, 30,  5, -2}}, 7, trials);
    test({{ -2, -3,  3, -4},
          { -5,-10, -4,  1},
          { 10, 30,  0, -2}}, 8, trials);
    test({{ -2, -5, 10},
          { -3,-10, 30},
          {  3, -4,  0},
          { -4,  1, -2}}, 8, trials);
    test({{0}}, 1, trials);
    return 0;
}

#include <vector>
#include <cstring>
#include <iostream>

using namespace std;

int clumsy(int N)
{
    int accumulated = 0;
    int current = -N;
    --N;
    for (unsigned int idx = 0; N >= 1; --N)
    {
        if (idx == 0) // Multiplication
        {
            current *= N;
            ++idx;
        }
        else if (idx == 1) // Division
        {
            accumulated -= current / N;
            current = 0;
            ++idx;
        }
        else if (idx == 2) // Addition
        {
            accumulated += N;
            ++idx;
        }
        else if (idx == 3)
        {
            current = N;
            idx = 0;
        }
    }
    return accumulated - current;
    //10 * 9 / 8 + 7 - 6 * 5 / 4 + 3 - 2 * 1
    // Operations: * / + -
    //N * clumsy(N - 1)
}

void test(int input, int solution)
{
    int result = clumsy(input);
    if (result == solution) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
    test(4, 7);
    test(10, 12);
    return 0;
}


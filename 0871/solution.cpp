#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <map>
#include <unordered_map>
#include <limits>
#include <functional>

using namespace std;

#if 1
int minRefuelStops(int target, int startFuel, const std::vector<std::vector<int> > &stations)
{
    int result = 0;
    int n = stations.size();
    int fuel = startFuel;
    std::pair<int, int> s[n + 2];
    s[0] = {0, 0};
    for (int i = 0; i < n; ++i)
        s[i + 1] = { stations[i][0], stations[i][1] };
    s[n + 1] = {target, 0};
    std::priority_queue<int, std::vector<int>, std::greater<int> > q;
    for (int i = 1; i < n + 2; ++i)
    {
        fuel -= s[i].first - s[i - 1].first;
        while ((!q.empty()) && (fuel < 0))
        {
            fuel -= q.top();
            q.pop();
            ++result;
        }
        if (fuel < 0) return -1;
        q.push(-s[i].second);
    }
    return result;
}

#elif 0
int move(int position, int fuel, const std::map<int, int> &stations, int target)
{
    if (position + fuel >= target) return 0;
    int result = std::numeric_limits<int>::max();
    for (auto begin = stations.upper_bound(position), end = stations.upper_bound(position + fuel); begin != end; ++begin)
        result = std::min(result, move(begin->first, fuel - (begin->first - position) + begin->second, stations, target));
    if (result != std::numeric_limits<int>::max()) return result + 1;
    return result;
}

int minRefuelStops(int target, int startFuel, const std::vector<std::vector<int> > &stations)
{
    std::map<int, int> stations_map;
    for (const auto &s : stations)
        stations_map[s[0]] = s[1];
    int result = move(0, startFuel, stations_map, target);
    return (result == std::numeric_limits<int>::max())?-1:result;
}
#else
struct location
{
    int position;
    int fuel;
    inline int range(void) const { return position + fuel; }
    inline bool operator>=(int t) { return range() >= t; }
    bool operator==(const location &other) const { return (position == other.position) && (fuel == other.fuel); }
};

namespace std
{
    template<> struct hash<location>
    {
        std::size_t operator()(location const& l) const noexcept
        {
            std::size_t h1 = std::hash<int>{}(l.position);
            std::size_t h2 = std::hash<int>{}(l.fuel);
            return h1 ^ h2;
        }
    };
}

int move(location l, std::unordered_map<location, int> &cache, const std::map<int, int> &stations, int target)
{
    if (l >= target) return 0;
    auto search = cache.find(l);
    if (search != cache.end())
        return search->second;
    int result = std::numeric_limits<int>::max();
    for (auto begin = stations.upper_bound(l.position), end = stations.upper_bound(l.range()); begin != end; ++begin)
    {
        result = std::min(result, move({begin->first, l.fuel - (begin->first - l.position) + begin->second}, cache, stations, target));
    }
    if (result != std::numeric_limits<int>::max())
    {
        cache[l] = result + 1;
        return result + 1;
    }
    cache[l] = result;
    return result;
}

int minRefuelStops(int target, int startFuel, const std::vector<std::vector<int> > &stations)
{
    std::unordered_map<location, int> cache;
    std::map<int, int> stations_map;
    for (const auto &s : stations)
        stations_map[s[0]] = s[1];
    int result = move({0, startFuel}, cache, stations_map, target);
    return (result == std::numeric_limits<int>::max())?-1:result;
}
#endif

void test(int target, int startFuel, std::vector<std::vector<int> > stations, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minRefuelStops(target, startFuel, stations);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10000000;
    constexpr unsigned int trials = 1000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(100, 10, {{10, 60}, {20, 30}, {30, 30}, {60, 40}}, 2, trials);
    test(100, 1, {{10, 100}}, -1, trials);
    test(1, 1, {}, 0, trials);
    test(100, 50, {{25, 30}}, -1, trials);
    std::vector<std::vector<int> > stations;
    for (int i = 1; i < 99; ++i)
        stations.push_back({i, 10});
    test(100, 50, stations, 5, trials);
    test(100, 50, {{25, 25}, {50, 50}}, 1, trials);
    return 0;
}

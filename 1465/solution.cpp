#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <algorithm>
#include "values.hpp"

using namespace std;

int maxArea(int h, int w, std::vector<int> horizontalCuts, std::vector<int> verticalCuts)
{
    long area = 0;
    std::sort(horizontalCuts.begin(), horizontalCuts.end());
    std::sort(verticalCuts.begin(), verticalCuts.end());
#if 1
    int max_dh = 0, max_dv = 0;
    int previous = 0;
    for (auto v : verticalCuts)
    {
        max_dv = std::max(max_dv, v - previous);
        previous = v;
    }
    max_dv = std::max(max_dv, w - previous);
    previous = 0;
    for (auto h : horizontalCuts)
    {
        max_dh = std::max(max_dh, h - previous);
        previous = h;
    }
    max_dh = std::max(max_dh, h - previous);
    area = static_cast<long>(max_dv) * static_cast<long>(max_dh);
#else
    horizontalCuts.push_back(h);
    verticalCuts.push_back(w);
    
    int previous_v = 0;
    for (const auto v : verticalCuts)
    {
        const long dv = static_cast<long>(v - previous_v);
        int previous_h = 0;
        for (const auto h : horizontalCuts)
        {
            area = std::max(area, static_cast<long>(h - previous_h) * dv);
            previous_h = h;
        }
        previous_v = v;
    }
#endif
    return static_cast<int>(area % (1'000'000'000 + 7));
}

void test(int h, int w, std::vector<int> horizontalCuts, std::vector<int> verticalCuts, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxArea(h, w, horizontalCuts, verticalCuts);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 4, {1, 2, 4}, {1, 3}, 4, trials);
    test(5, 4, {3, 1}, {1}, 6, trials);
    test(5, 4, {3}, {3}, 9, trials);
    test(1000000000, 1000000000, values_h, values_v, 755332975, trials);
    return 0;
}

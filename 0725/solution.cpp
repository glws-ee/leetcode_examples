#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

struct ListNode
{
    int val = 0;
    ListNode * next = nullptr;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
    ~ListNode(void) { delete next; }
};

std::vector<ListNode *> splitListToParts(ListNode* head, int k)
{
    std::vector<ListNode *> result(k, nullptr);
    int length = 0;
    for (ListNode * ptr = head; ptr != nullptr; ptr = ptr->next, ++length);
    const int loop_long = length % k;
    const int loop_length = length / k;
    int idx = 0;
    for (int i = 0; i < loop_long; ++i)
    {
        result[i] = head;
        ListNode * previous = nullptr;
        for (int j = 0; j <= loop_length; ++j)
        {
            previous = head;
            head = head->next;
        }
        previous->next = nullptr;
    }
    for (int i = loop_long; i < k; ++i)
    {
        result[i] = head;
        ListNode * previous = nullptr;
        for (int j = 0; j < loop_length; ++j)
        {
            previous = head;
            head = head->next;
        }
        if (previous != nullptr)
            previous->next = nullptr;
    }
    return result;
}

ListNode * vec2list(const std::vector<int> &vec)
{
    if (vec.size() == 0) return nullptr;
    else
    {
        const int n = vec.size();
        ListNode * l = new ListNode(vec[0]);
        ListNode * current = l;
        for (int i = 1; i < n; ++i)
            current = current->next = new ListNode(vec[i]);
        return l;
    }
}

std::vector<int> list2vec(const ListNode * l)
{
    int n = 0;
    for (const ListNode * current = l; current != nullptr; ++n, current = current->next);
    std::vector<int> vec(n);
    n = 0;
    for (const ListNode * current = l; current != nullptr; ++n, current = current->next)
        vec[n] = current->val;
    return vec;
}

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    for (int i = 0; i < n; ++i)
        if (left[i] != right[i]) return false;
    return true;
}

bool operator==(const std::vector<std::vector<int> > &left, const std::vector<std::vector<int> > &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    for (int i = 0; i < n; ++i)
        if (!(left[i] == right[i])) return false;
    return true;
}

std::ostream& operator<<(std::ostream &out, const std::vector<std::vector<int> > &vec)
{
    out << '{';
    bool next_out = false;
    for (const auto &vl : vec)
    {
        if (next_out) [[likely]] out << ", ";
        next_out = true;
        bool next_in = false;
        out << '{';
        for (int v : vl)
        {
            if (next_in) [[likely]] out << ", ";
            next_in = true;
            out << v;
        }
        out << '}';
    }
    out << '}';
    return out;
}

void test(std::vector<int> input, int k, std::vector<std::vector<int> > solution, unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * linput = vec2list(input);
        std::vector<ListNode * > ln_result = splitListToParts(linput, k);
        const int m = ln_result.size();
        result.resize(m);
        for (int j = 0; j < m; ++j)
        {
            result[j] = list2vec(ln_result[j]);
            delete ln_result[j];
        }
    }
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 5, {{1}, {2}, {3}, {}, {}}, trials);
    test({1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, 3, {{1, 2, 3, 4}, {5, 6, 7}, {8, 9, 10}}, trials);
    return 0;
}

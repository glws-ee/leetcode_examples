#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <unordered_map>
#include <set>
#include <map>

using namespace std;

constexpr int null = -1000;

int findLUSlength(std::vector<std::string> strs)
{
#if 1
    auto isSubsequence = [](const std::string &a, const std::string &b) -> bool
    {
        const int na = a.size();
        const int nb = b.size();
        int i = 0;
        for (int j = 0; (i < na) && (j < nb); ++j)
            if (a[i] == b[j])
                ++i;
        return i == na;
    };
    std::map<int, std::unordered_map<std::string, int> > table;
    for (auto &s: strs) ++table[s.size()][s];
    for (auto it = table.rbegin(); it != table.rend(); ++it)
    {
        auto check = [&](const std::string &str_first) -> bool
        {
            for (auto itn = table.rbegin(); itn != it; ++itn)
                for (const auto &str_second: itn->second)
                    if (isSubsequence(str_first, str_second.first))
                        return false;
            return true;
        };
        for (const auto &str_first: it->second)
            if ((str_first.second == 1) && (check(str_first.first)))
                return it->first;
    }
    return -1;
#else
    const int n = strs.size();
    std::unordered_map<std::string, std::set<int> > mp;
    for(int ind = 0; ind < n; ++ind)
    {
        std::string curr = strs[ind];
        int n2 = curr.size();
        for (int i = 0; i < (1 << n2); ++i)
        {
            std::string now = "";
            for(int j = 0;j < n2;j++)
                if (i & (1 << j))
                    now += curr[j];
            mp[now].insert(ind);
        }
    }
    int res = -1;
    for(auto it : mp)
        if (it.second.size() == 1)
            res = std::max(res, static_cast<int>(it.first.size()));              
    return res;
#endif
}

void test(std::vector<std::string> strs, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findLUSlength(strs);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"aba", "cdc", "eae"}, 3, trials);
    test({"aaa", "aaa", "aa"}, -1, trials);
    return 0;
}

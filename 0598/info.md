=Range Addition II=


You are given an `m x n` matrix `M` initialized with all `0`'s and an array of operations `ops`, where `ops[i] = [a_i, b_i]` means `M[x][y]` should be incremented by one for all `0 <= x < a_i` and `0 <= y < b_i`.

Count and return *the number of maximum integers in the matrix after performing all the operations*.

**Example 1:**<br>
*Input:* `m = 3, n = 3, ops = [[2, 2], [3, 3]]`<br>
*Output:* `4`<br>
*Explanation:* The maximum integer in M is `2`, and there are four of it in `M`, so return `4`.<br>

**Example 2:**<br>
*Input:* `m = 3, n = 3, ops = [[2, 2], [3, 3], [3, 3], [3, 3], [2, 2], [3, 3], [3, 3], [3, 3], [2, 2], [3, 3], [3, 3], [3, 3]]`<br>
*Output:* `4`<br>

**Example 3:**<br>
*Input:* `m = 3, n = 3, ops = []`<br>
*Output:* `9`<br>
 

**Constraints:**<br>
- `1 <= m, n <= 4 * 10^4`
- `1 <= ops.length <= 10^4`
- `ops[i].length == 2`
- `1 <= a_i <= m`
- `1 <= b_i <= n`




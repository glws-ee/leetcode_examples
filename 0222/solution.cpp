#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <functional>

using namespace std;

constexpr int null = -1000;

struct TreeNode
{
    int val = 0;
    TreeNode * left = nullptr;
    TreeNode * right = nullptr;
    TreeNode(void) = default;
    TreeNode(int v) : val(v), left(nullptr), right(nullptr) {}
    TreeNode(int v, const TreeNode * l, const TreeNode * r) :
        val(v),
        left((l != nullptr)?new TreeNode(*l):nullptr),
        right((r != nullptr)?new TreeNode(*r):nullptr) {}
    TreeNode(const TreeNode &other) :
        val(other.val),
        left((other.left != nullptr)?new TreeNode(*other.left):nullptr),
        right((other.right != nullptr)?new TreeNode(*other.right):nullptr) {}
    TreeNode(TreeNode &&other) :
        val(other.val),
        left(std::exchange(other.left, nullptr)),
        right(std::exchange(other.right, nullptr)) {}
    ~TreeNode(void) { delete left; delete right; }
    TreeNode& operator=(const TreeNode &other)
    {
        if (this != &other)
        {
            delete left;
            delete right;
            val = other.val;
            left = (other.left != nullptr)?new TreeNode(*other.left):nullptr;
            right = (other.right != nullptr)?new TreeNode(*other.right):nullptr;
        }
        return *this;
    }
    TreeNode& operator=(TreeNode &&other)
    {
        if (this != &other)
        {
            val = other.val;
            left = std::exchange(other.left, nullptr);
            right = std::exchange(other.right, nullptr);
        }
        return *this;
    }
};

TreeNode * vec2tree(const std::vector<int> &tree)
{
    std::queue<TreeNode *> active_nodes;
    
    TreeNode * result = nullptr;
    bool left = true;
    for (const auto &v : tree)
    {
        if (v != null)
        {
            if (result == nullptr)
            {
                result = new TreeNode(v);
                active_nodes.push(result);
                active_nodes.push(result);
            }
            else
            {
                TreeNode * current = new TreeNode(v);
                if (left) active_nodes.front()->left = current;
                else active_nodes.front()->right = current;
                active_nodes.pop();
                active_nodes.push(current);
                active_nodes.push(current);
                left = !left;
            }
        }
        else
        {
            active_nodes.pop();
            left = !left;
        }
    }
    return result;
}

std::vector<int> tree2vec(TreeNode * root)
{
    if (root == nullptr) return {};
    std::vector<int> result;
    std::queue<TreeNode *> active_nodes;
    active_nodes.push(root);
    result.push_back(root->val);
    while (!active_nodes.empty())
    {
        TreeNode * current = active_nodes.front();
        active_nodes.pop();
        if (current->left != nullptr)
        {
            active_nodes.push(current->left);
            result.push_back(current->left->val);
        }
        else result.push_back(null);
        if (current->right != nullptr)
        {
            active_nodes.push(current->right);
            result.push_back(current->right->val);
        }
        else result.push_back(null);
    }
    while (result.back() == null) result.pop_back();
    return result;
}

std::ostream& operator<<(std::ostream &out, const std::vector<int> &vec)
{
    out << '{';
    bool next = false;
    for (const auto &v : vec)
    {
        if (next) out << ", ";
        out << v;
        next = true;
    }
    out << '}';
    return out;
}

// ################################################################################################
// ################################################################################################

int countNodes(TreeNode * root)
{
#if 0
    int maximum = 0, minimum = 0;
    for (TreeNode * ptr = root; ptr != nullptr; ptr = ptr->left) ++maximum;
    for (TreeNode * ptr = root; ptr != nullptr; ptr = ptr->right) ++minimum;
    if (maximum == minimum)
        return (1 << maximum) - 1;
    function<int(TreeNode *, int, int)> countInnerNodes = [&](TreeNode * curr, int left, int right)
    {
        if (left < 0)
        {
            left = 0;
            for (TreeNode * ptr = curr; ptr != nullptr; ptr = ptr->left) ++left;
        }
        else
        {
            right = 0;
            for (TreeNode * ptr = curr; ptr != nullptr; ptr = ptr->right) ++right;
        }
        if (left == right)
            return (1 << left) - 1;
        return 1
             + countInnerNodes(curr->left , left - 1,        -1)
             + countInnerNodes(curr->right,       -1, right - 1);
    };
    return 1
         + countInnerNodes(root->left , maximum - 1,          -1)
         + countInnerNodes(root->right,          -1, minimum - 1);
#elif 0
    int maximum = 0, minimum = 0;
    for (TreeNode * ptr = root; ptr != nullptr; ptr = ptr->left) ++maximum;
    for (TreeNode * ptr = root; ptr != nullptr; ptr = ptr->right) ++minimum;
    if (maximum == minimum)
        return (1 << maximum) - 1;
    return 1
         + countNodes(root->left )
         + countNodes(root->right);
#elif 1
    if (root == nullptr) return 0;
    if (root->left == nullptr) return 1;
    int maximum_height = 0;
    for (TreeNode * ptr = root; ptr != nullptr; ptr = ptr->left) ++maximum_height;
    const int number_of_bits = maximum_height - 1;
    const int number_of_leafs = 1 << number_of_bits;
    int minimum_height = 0;
    for (TreeNode * ptr = root; ptr != nullptr; ptr = ptr->right) ++minimum_height;
    if (maximum_height == minimum_height)
        return (number_of_leafs << 1) - 1;
    int right = number_of_leafs - 1;
    for (int left = 0; right - left > 1;)
    {
        int aux = (left + right) / 2;
        //std::cout << left << ' ' << aux << ' ' << right << '\n';
        TreeNode * current = root;
        for (int displacement = number_of_bits - 1; displacement >= 0; --displacement)
        {
            if ((aux >> displacement) & 1) current = current->right;
            else current = current->left;
        }
        if (current == nullptr) right = aux;
        else left = aux;
    }
    return (number_of_leafs << 1) - 1 - number_of_leafs + right;
#else
    if (root == nullptr) return 0;
    if (root->left == nullptr) return 1;
    int maximum_height = 0;
    for (TreeNode * ptr = root; ptr != nullptr; ptr = ptr->left) ++maximum_height;
    int number_empty = 0;
    
    function<bool(TreeNode *, int)> traverse = [&](TreeNode * current, int height)
    {
        if (current == nullptr)
        {
            if (height == maximum_height) return true;
            ++number_empty;
            return false;
        }
        if (traverse(current->right, height + 1))
            return true;
        return traverse(current->left, height + 1);
    };
    traverse(root, 0);
    
    return (1 << maximum_height) - 1 - number_empty;
#endif
}

// ################################################################################################
// ################################################################################################
void test(std::vector<int> tree, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = countNodes(root);
        delete root;
    }
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
    constexpr int TEST_N = 256;
    constexpr int BIG_TREE_SIZE = 5'000'000;
#if 0
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 100;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 4}, 3, trials);
    test({1, 2, 3, 4}, 4, trials);
    test({1, 2, 3, 4, 5}, 5, trials);
    test({1, 2, 3, 4, 5, 6}, 6, trials);
    test({1, 2, 3, 4, 5, 6, 7}, 7, trials);
    test({}, 0, trials);
    test({1}, 1, trials);
#if 0
    std::vector<int> tree;
    tree.reserve(TEST_N);
    for (int i = 0; i < TEST_N; i += 4)
    {
        test(tree, i, trials);
        for (int j = 0; j < 4; ++j)
            tree.push_back(i + 1 + j);
    }
#endif
    std::vector<int> big_tree(BIG_TREE_SIZE);
    for (int i = 0; i < BIG_TREE_SIZE; ++i) big_tree[i] = i + 1;
    test(big_tree, BIG_TREE_SIZE, trials);
    return 0;
}

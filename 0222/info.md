=Count Complete Tree Nodes=

Given the `root` of a **complete** binary tree, return the number of the nodes in the tree.

According to **Wikipedia**, every level, except possibly the last, is completely filled in a complete binary tree, and all nodes in the last level are as far left as possible. It can have between `1` and `2^h` nodes inclusive at the last level `h`.

Design an algorithm that runs in less than `O(n)` time complexity.

**Example 1:**

```mermaid 
A((1))-->B((2))
A-->C((3))
B-->D((4))
B-->E((5))
C-->F((6))
```
*Input:* `root = [1, 2, 3, 4, 5, 6]`<br>
*Output:* `6`<br>

**Example 2:**<br>
*Input:* `root = []`<br>
*Output:* `0`<br>

**Example 3:**<br>
*Input:* `root = [1]`<br>
*Output:* `1`<br>

**Constraints:**
- The number of nodes in the tree is in the range `[0, 5 * 10^4]`.
- `0 <= Node.val <= 5 * 10^4`
- The tree is guaranteed to be **complete**.



#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

struct TreeNode
{
    int val = 0;
    TreeNode * left = nullptr;
    TreeNode * right = nullptr;
    TreeNode(void) = default;
    TreeNode(int v) : val(v), left(nullptr), right(nullptr) {}
    TreeNode(int v, const TreeNode * l, const TreeNode * r) :
        val(v),
        left((l != nullptr)?new TreeNode(*l):nullptr),
        right((r != nullptr)?new TreeNode(*r):nullptr) {}
    TreeNode(const TreeNode &other) :
        val(other.val),
        left((other.left != nullptr)?new TreeNode(*other.left):nullptr),
        right((other.right != nullptr)?new TreeNode(*other.right):nullptr) {}
    TreeNode(TreeNode &&other) :
        val(other.val),
        left(std::exchange(other.left, nullptr)),
        right(std::exchange(other.right, nullptr)) {}
    ~TreeNode(void) { delete left; delete right; }
    TreeNode& operator=(const TreeNode &other)
    {
        if (this != &other)
        {
            delete left;
            delete right;
            val = other.val;
            left = (other.left != nullptr)?new TreeNode(*other.left):nullptr;
            right = (other.right != nullptr)?new TreeNode(*other.right):nullptr;
        }
        return *this;
    }
    TreeNode& operator=(TreeNode &&other)
    {
        if (this != &other)
        {
            val = other.val;
            left = std::exchange(other.left, nullptr);
            right = std::exchange(other.right, nullptr);
        }
        return *this;
    }
};

TreeNode * vec2tree(const std::vector<int> &tree)
{
    std::queue<TreeNode *> active_nodes;
    
    TreeNode * result = nullptr;
    bool left = true;
    for (const auto &v : tree)
    {
        if (v != null)
        {
            if (result == nullptr)
            {
                result = new TreeNode(v);
                active_nodes.push(result);
                active_nodes.push(result);
            }
            else
            {
                TreeNode * current = new TreeNode(v);
                if (left) active_nodes.front()->left = current;
                else active_nodes.front()->right = current;
                active_nodes.pop();
                active_nodes.push(current);
                active_nodes.push(current);
                left = !left;
            }
        }
        else
        {
            active_nodes.pop();
            left = !left;
        }
    }
    return result;
}

std::vector<int> tree2vec(TreeNode * root)
{
    if (root == nullptr) return {};
    std::vector<int> result;
    std::queue<TreeNode *> active_nodes;
    active_nodes.push(root);
    result.push_back(root->val);
    while (!active_nodes.empty())
    {
        TreeNode * current = active_nodes.front();
        active_nodes.pop();
        if (current->left != nullptr)
        {
            active_nodes.push(current->left);
            result.push_back(current->left->val);
        }
        else result.push_back(null);
        if (current->right != nullptr)
        {
            active_nodes.push(current->right);
            result.push_back(current->right->val);
        }
        else result.push_back(null);
    }
    while (result.back() == null) result.pop_back();
    return result;
}

std::ostream& operator<<(std::ostream &out, const std::vector<int> &vec)
{
    out << '{';
    bool next = false;
    for (const auto &v : vec)
    {
        if (next) out << ", ";
        out << v;
        next = true;
    }
    out << '}';
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<std::vector<int> > &vec)
{
    out << '{';
    bool next = false;
    for (const auto &v : vec)
    {
        if (next) out << ", ";
        out << v;
        next = true;
    }
    out << '}';
    return out;
}

// ################################################################################################
// ################################################################################################

std::vector<std::vector<int> > levelOrder(TreeNode * root)
{
    if (root == nullptr) return {};
    std::queue<std::pair<TreeNode *, int> > queue;
    queue.push({root, 0});
    int current_level = 0;
    std::vector<std::vector<int> > result;
    std::vector<int> partial_result;
    while (!queue.empty())
    {
        auto current = queue.front();
        queue.pop();
        if (current_level == current.second)
            partial_result.push_back(current.first->val);
        else
        {
            result.emplace_back(std::move(partial_result));
            partial_result = {current.first->val};
            current_level = current.second;
        }
        if (current.first->left != nullptr) queue.push({current.first->left, current.second + 1});
        if (current.first->right != nullptr) queue.push({current.first->right, current.second + 1});
    }
    result.emplace_back(std::move(partial_result));
    return result;
}

// ################################################################################################
// ################################################################################################

void test(const std::vector<int> &tree, const std::vector<std::vector<int> > solution, unsigned int trials = 1)
{
    std::cout << "INPUT: " << tree << '\n';
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = levelOrder(root);
        delete root;
    }
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 9, 20, null, null, 15, 7}, {{3}, {9, 20}, {15, 7}}, trials);
    test({1}, {{1}}, trials);
    test({}, {}, trials);
    return 0;
}

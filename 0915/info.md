=Partition Array into Disjoint Intervals=

Given an array `nums`, partition it into two (contiguous) subarrays `left` and `right` so that:

- Every element in `left` is less than or equal to every element in `right`.
- `left` and `right` are non-empty.
- `left` has the smallest possible size.

Return the **length** of `left` after such a partitioning.  It is guaranteed that such a partitioning exists.

**Example 1:**<br>
*Input:* `nums = [5, 0, 3, 8, 6]`<br>
*Output:* `3`<br>
*Explanation:* `left = [5, 0, 3], right = [8, 6]`<br>

**Example 2:**<br>
*Input:* `nums = [1, 1, 1, 0, 6, 12]`<br>
*Output:* `4`<br>
*Explanation:* `left = [1, 1, 1, 0], right = [6, 12]`<br>
 
**Note:**<br>
- `2 <= nums.length <= 30000`
- `0 <= nums[i] <= 10^6`
- It is guaranteed there is at least one way to partition `nums` as described.




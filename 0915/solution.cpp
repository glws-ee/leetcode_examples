#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

int partitionDisjoint(std::vector<int> nums)
{
#if 1
    const int n = nums.size();
    int min_value[n];
    for (int i = n - 1, previous = nums[n - 1]; i >= 0; --i)
        previous = min_value[i] = std::min(nums[i], previous);
    int max = nums[0];
    for (int i = 1; i < n; ++i)
    {
        if (max <= min_value[i]) return i;
        max = std::max(max, nums[i]);
    }
    return 1;
#else
    const int n = nums.size();
    int max_value[n];
    int min_value[n];
    for (int i = n - 1, previous = nums[n - 1]; i >= 0; --i)
        previous = min_value[i] = std::min(nums[i], previous);
    for (int i = 0, previous = nums[0]; i < n; ++i)
        previous = max_value[i] = std::max(nums[i], previous);
    int threshold = min_value[0];
    int idx = 0;
    if (min_value[0] < min_value[1]) return 1;
    std::cout << "[ARR] "; for (int i = 0; i < n; ++i) std::cout << nums[i] << ' '; std::cout << '\n';
    std::cout << "[MIN] "; for (int i = 0; i < n; ++i) std::cout << min_value[i] << ' '; std::cout << '\n';
    std::cout << "[MAX] "; for (int i = 0; i < n; ++i) std::cout << max_value[i] << ' '; std::cout << '\n';
    while (true)
    {
        int search = idx;
        while ((search < n - 1) && (min_value[search] <= threshold)) ++search;
        --search;
        if (search < 0) search = 0; // debug
        std::cout << "INDEX = " << idx << "; ";
        std::cout << "SEARCH = " << search << "; ";
        std::cout << "THRESHOLD = " << threshold << '\n';
        if (max_value[search] > threshold)
            threshold = max_value[search];
        else break;
    }
    return idx + 1;
#endif
}

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = partitionDisjoint(nums);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 0, 3, 8, 6}, 3, trials);
    test({1, 1, 1, 0, 6, 12}, 4, trials);
    test({0, 1, 1, 0, 5, -1, 4, 6, 12}, 7, trials);
    test({0, 0, 0, 0, 0, 0, 0, 0, 0}, 1, trials);
    test({1, 1}, 1, trials);
    test({6, 0, 8, 30, 37, 6, 75, 98, 39, 90, 63, 74, 52, 92, 64}, 2, trials);
    test({1, 0, 2, 0, 2}, 4, trials);
    test({92, 6, 0, 8, 30, 37, 6, 75, 39, 90, 63, 74, 52, 64, 98}, 14, trials);
    return 0;
}

#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <exception>

using namespace std;

constexpr int null = -1000;

class MapSum
{
private:
    static constexpr int SIZE = 26;
    struct Node
    {
        struct Node * children[SIZE] = {nullptr};
        int sum = 0;
        int word = 0;
        ~Node(void)
        {
            for (int i = 0; i < SIZE; ++i) delete children[i];
        }
    };
    Node m_root;
#if 0
    int search(const std::string &word)
    {
        const int n = word.size();
        const Node * current = &m_root;
        for (int i = 0; (current != nullptr) && (i < n); ++i)
            current = current->children[word[i] - 'a'];
        return (current != nullptr)?current->word:0;
    }
#endif
public:
    MapSum(void)
    {
        
    }
    
    void insert(string key, int val)
    {
#if 1
        Node * current = &m_root;
        const int n = key.size();
        for (int i = 0; i < n; ++i)
        {
            const char chr = key[i] - 'a';
            if (!current->children[chr])
                current->children[chr] = new Node();
            current = current->children[chr];
            current->sum += val;
        }
        if (current->word != 0)
        {
            const int previous = current->word;
            current = &m_root;
            for (int i = 0; i < n; ++i)
            {
                const char chr = key[i] - 'a';
                current = current->children[chr];
                current->sum -= previous;
            }
        }
        current->word = val;
#else
        Node * current = &m_root;
        const int n = key.size();
        int update = val;
        if (int previous = search(key); previous)
            update -= previous;
        for (int i = 0; i < n; ++i)
        {
            const char chr = key[i] - 'a';
            if (!current->children[chr])
                current->children[chr] = new Node();
            current = current->children[chr];
            current->sum += update;
        }
        current->word = val;
#endif
    }
    
    int sum(string prefix)
    {
        const int n = prefix.size();
        const Node * current = &m_root;
        for (int i = 0; (current != nullptr) && (i < n); ++i)
            current = current->children[prefix[i] - 'a'];
        return (current != nullptr)?current->sum:0;
    }
};

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    for (int i = 0; i < n; ++i)
        if (left[i] != right[i]) return false;
    return true;
}

std::ostream& operator<<(std::ostream &out, const std::vector<int> &vec)
{
    out << '{';
    bool next = false;
    for (int v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

enum class OP { CREATE, INSERT, SUM };
void test(std::vector<OP> operations, std::vector<std::string> keys, std::vector<int> val, std::vector<int> solution, unsigned int trials = 1)
{
    const int n = operations.size();
    std::vector<int> result(n, null);
    for (unsigned int i = 0; i < trials; ++i)
    {
        MapSum * sum = nullptr;
        for (int o = 0; o < n; ++o)
        {
            switch (operations[o])
            {
            case OP::CREATE:
                sum = new MapSum();
                break;
            case OP::INSERT:
                sum->insert(keys[o], val[o]);
                break;
            case OP::SUM:
                result[o] = sum->sum(keys[o]);
                break;
            default:
                throw std::out_of_range("Operation unknown");
            };
        }
        delete sum;
    }
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::CREATE, OP::INSERT, OP::SUM, OP::INSERT, OP::SUM},
         {""        , "apple"   , "ap"   , "app"     , "ap"   },
         {null      , 3         , null   , 2         , null   },
         {null      , null      , 3      , null      , 5      }, trials);
    test({OP::CREATE, OP::INSERT, OP::SUM, OP::INSERT, OP::SUM},
         {""        , "apple"   , "apple", "app"     , "ap"   },
         {null      , 3         , null   , 2         , null   },
         {null      , null      , 3      , null      , 5      }, trials);
    test({OP::CREATE, OP::INSERT, OP::SUM, OP::INSERT, OP::SUM},
         {""        , "a"       , "ap"   , "b"       , "a"    },
         {null      , 3         , null   , 2         , null   },
         {null      , null      , 0      , null      , 3      }, trials);
    test({OP::CREATE, OP::INSERT, OP::SUM, OP::INSERT, OP::INSERT, OP::SUM},
         {""        , "apple"   , "ap"   , "app"     , "apple"   , "ap"   },
         {null      , 3         , null   , 2         , 2         , null   },
         {null      , null      , 3      , null      , null      , 4      }, trials);
    return 0;
}

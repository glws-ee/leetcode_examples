#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <algorithm>
#include <sstream>
#include <map>
#include <unordered_map>

using namespace std;

std::vector<std::vector<std::string> > findDuplicate(std::vector<std::string> &paths)
{
#if 0
    std::unordered_map<std::string, std::vector<std::string> > duplicates;
    for (const auto &p : paths)
    {
        auto pos = p.find(' ');
        auto path = p.substr(0, pos); // find/substr is much faster that istringstream to tokenize the input string.
        for (; pos < p.size(); pos = p.find(' ', pos))
        {
            ++pos;
            auto prev = pos;
            pos = p.find('(', pos);
            auto filename = p.substr(prev, pos - prev);
            prev = pos + 1;
            pos = p.find(')', pos);
            auto content = p.substr(prev, pos - prev);
            duplicates[content].push_back(path + "/" + filename);
        }
    }
    std::vector<std::vector<std::string> > result;
    for (auto &d : duplicates)
        if (d.second.size() > 1)
            result.emplace_back(std::move(d.second));
    return result;
#else
    std::unordered_map<std::string, std::vector<std::string> > duplicates;
    for (const auto &p : paths)
    {
        std::istringstream iss(p);
        std::string item, path, filename, content;
        
        std::getline(iss, path, ' ');
        while (std::getline(iss, filename, '('))
        {
            std::getline(iss, content, ')');
            duplicates[content].push_back(path + "/" + filename);
            std::getline(iss, content, ' ');
        }
    }
    std::vector<std::vector<std::string> > result;
    for (auto &d : duplicates)
        if (d.second.size() > 1)
            result.emplace_back(std::move(d.second));
#endif
    return result;
}

std::ostream& operator<<(std::ostream &out, const std::vector<std::vector<std::string> > &result)
{
    bool next_group = false;
    out << '[';
    for (const auto &r : result)
    {
        bool next_path = false;
        if (next_group) out << ", ";
        next_group = true;
        out << '[';
        for (const auto &p : r)
        {
            if (next_path) out << ", ";
            next_path = true;
            out << p;
        }
        out << ']';
    }
    out << ']';
    return out;
}

void test(std::vector<std::string> paths, const std::vector<std::vector<std::string> > &solution, unsigned int trials = 1)
{
    std::vector<std::vector<std::string> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findDuplicate(paths);
    bool same = result.size() == solution.size();
    if (same)
    {
        const int n = result.size();
        std::string str_solution[n], str_result[n];
        for (int i = 0; i < n; ++i)
        {
            std::vector<std::string> copy;
            
            copy = solution[i];
            std::sort(copy.begin(), copy.end());
            for (const auto &s : copy)
                str_solution[i] += s;
            copy = result[i];
            std::sort(copy.begin(), copy.end());
            for (const auto &s : copy)
                str_result[i] += s;
        }
        std::sort(str_solution, str_solution + n);
        std::sort(str_result, str_result + n);
        for (int i = 0; same && (i < n); ++i)
            same = str_solution[i] == str_result[i];
    }
    if (same) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    constexpr unsigned int trials = 100000;
    //constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"root/a 1.txt(abcd) 2.txt(efgh)","root/c 3.txt(abcd)","root/c/d 4.txt(efgh)","root 4.txt(efgh)"}, {{"root/a/2.txt","root/c/d/4.txt","root/4.txt"}, {"root/a/1.txt","root/c/3.txt"}}, trials);
    test({"root/a 1.txt(abcd) 2.txt(efgh)","root/c 3.txt(abcd)","root/c/d 4.txt(efgh)"}, {{"root/a/2.txt","root/c/d/4.txt"},{"root/a/1.txt","root/c/3.txt"}}, trials);
    return 0;
}

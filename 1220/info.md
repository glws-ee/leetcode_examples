Given an integer `n`, your task is to count how many strings of length `n` can be formed under the following rules:

- Each character is a lower case vowel (`'a', 'e', 'i', 'o', 'u'`)
- Each vowel `'a'` may only be followed by an `'e'`.
- Each vowel `'e'` may only be followed by an `'a'` or an `'i'`.
- Each vowel `'i'` may not be followed by another `'i'`.
- Each vowel `'o'` may only be followed by an `'i'` or a `'u'`.
- Each vowel `'u'` may only be followed by an `'a'`.
- Since the answer may be too large, return it modulo `10^9 + 7`.

**Example 1:**<br>
*Input:* `n = 1`<br>
*Output:* `5`<br>
*Explanation:* All possible strings are: `"a", "e", "i" , "o" and "u"`.<br>

**Example 2:**<br>
*Input:* `n = 2`<br>
*Output:* `10`<br>
*Explanation:* All possible strings are: `"ae", "ea", "ei", "ia", "ie", "io", "iu", "oi", "ou" and "ua"`.<br>

**Example 3:**<br>
*Input:* `n = 5`<br>
*Output:* `68`<br>

**Constraints:**
- `1 <= n <= 2 * 10^4`




#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

int countVowelPermutation(int n)
{
    constexpr int a = 0, e = 1, i = 2, o = 3, u = 4;
    unsigned long buffer_current[5] = {1, 1, 1, 1, 1}, buffer_next[5];
    unsigned long * count = buffer_current, * next = buffer_next;
    unsigned long max_value = 1'000'000'007;
    
    // a -> e
    // e -> a, i.
    // i -> a, e, o, u
    // o -> i, u
    // u -> a
    for (int k = 1; k < n; ++k)
    {
        next[a] = (count[e] + count[i] + count[u]) % max_value;
        next[e] = (count[a] + count[i]) % max_value;
        next[i] = (count[e] + count[o]) % max_value;
        next[o] = (count[i]) % max_value;
        next[u] = (count[i] + count[o]) % max_value;
        std::swap(count, next);
    }
    
    return (count[a] + count[e] + count[i] + count[o] + count[u]) % max_value;
}

void test(int n, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = countVowelPermutation(n);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(1, 5, trials);
    test(2, 10, trials);
    test(5, 68, trials);
    test(144, 18'208'803, trials);
    return 0;
}

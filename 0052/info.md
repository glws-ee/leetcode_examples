The **n-queens** puzzle is the problem of placing `n` queens on an `n x n` chessboard such that no two queens attack each other.

Given an integer `n`, return *the number of distinct solutions to the* ***n-queens puzzle***.

**Example 1:**<br>

|:---:|:---:|:---:|:---:|
|     |:crown:|   |     |
|     |      |  |:crown:|
|:crown:|   |     |     |
|     |     |:crown:|   |

*Input:* `n = 4`<br>
*Output:* `2`<br>
*Explanation:* There are two distinct solutions to the 4-queens puzzle as shown.<br>

**Example 2:**<br>
*Input:* `n = 1`<br>
*Output:* `1`<br>
 
**Constraints:**<br>
    - `1 <= n <= 9`

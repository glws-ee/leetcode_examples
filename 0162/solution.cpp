#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <unordered_set>

using namespace std;

std::ostream& operator<<(std::ostream &out, const std::vector<int> &s)
{
    out << '{';
    bool next = false;
    for (int v: s)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

int findPeakElement(std::vector<int> nums)
{
    for (int begin = 0, end = nums.size() - 1; begin != end;)
    {
        if (begin == end)
            return begin;
        else if (end - begin == 1)
        {
            return begin + (nums[begin] < nums[end]);
        }
        else
        {
            int mid = (begin + end) / 2;
            if (nums[mid] > nums[mid - 1]) begin = mid;
            else end = mid;
        }
    }
    return 0;
}

std::ostream& operator<<(std::ostream &out, const std::unordered_set<int> &s)
{
    out << '{';
    bool next = false;
    for (int v: s)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

void test(std::vector<int> nums, const std::unordered_set<int> &solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findPeakElement(nums);
    if (solution.find(result) != solution.end()) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 1}, {2}, trials);
    test({1, 2, 1, 3, 5, 6, 4}, {1, 5}, trials);
    test({1, 6, 5, 4, 3, 2, 1}, {1}, trials);
    return 0;
}

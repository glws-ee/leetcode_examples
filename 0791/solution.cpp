#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <unordered_map>

using namespace std;

std::string customSortString(std::string order, std::string str)
{
#if 1
    std::string result(str.size(), ' ');
    char lut[128];
    short histogram[26];
    char idx = 0;
    std::memset(histogram, 0, sizeof(histogram));
    std::memset(lut, -1, sizeof(lut));
    for (char c : order)
        lut[c] = idx++;
    for (short i = 0; i < 128; ++i)
        if (lut[i] == -1) lut[i] = idx;
    for (char c : str)
        ++histogram[lut[c]];
    for (char i = 1; i <= idx; ++i)
        histogram[i] += histogram[i - 1];
    for (auto begin = str.rbegin(), end = str.rend(); begin != end; ++begin)
    {
        char c = *begin;
        result[--histogram[lut[c]]] = c;
    }
    return result;
#else
    std::string result(str.size(), ' ');
    std::unordered_map<char, char> lut;
    short histogram[26];
    char idx = 0;
    std::memset(histogram, 0, sizeof(histogram));
    for (char c : order)
        lut[c] = idx++;
    for (char c : str)
    {
        if (auto it = lut.find(c); it != lut.end())
            ++histogram[it->second];
        else ++histogram[idx];
    }
    for (char i = 1; i <= idx; ++i)
        histogram[i] += histogram[i - 1];
    for (auto begin = str.rbegin(), end = str.rend(); begin != end; ++begin)
    {
        char c = *begin;
        if (auto it = lut.find(c); it != lut.end())
            result[--histogram[it->second]] = c;
        else result[--histogram[idx]] = c;
    }
    return result;
#endif
}

void test(std::string order, std::string str, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = customSortString(order, str);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected '" << solution << "' and obtained '" << result << "'.\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("cba", "abcd", "cbad", trials);
    test("cba", "adbcd", "cbadd", trials);
    test("cba", "axbcd", "cbaxd", trials);
    return 0;
}

#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

int minFlipsMonoIncr(std::string s)
{
#if 1
    int flips = 0, counter = 0;
    for (auto c : s)
    {
        bool zero = c == '0';
        flips = std::min(flips += zero, counter += !zero);
    }
    return flips;
#else
    int flips = 0, counter = 0;
    for (auto c : s)
    {
        if (c == '0') ++flips;
        else ++counter;
        flips = std::min(flips, counter);
    }
    return flips;
#endif
}

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minFlipsMonoIncr(s);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("00110", 1, trials);
    test("010110", 2, trials);
    test("00011000", 2, trials);
    return 0;
}

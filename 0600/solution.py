def findIntegers(num):
    s = bin(num + 1)[2:]
    n = len(s)
    dp = [1, 2] + [0]*(n-2)
    for i in range(2, n):
        dp[i] = dp[i-1] + dp[i-2]
    #print(num, s, n, dp)
    flag, ans = 0, 0
    for i in range(n):
        if s[i] == "0": continue
        if flag == 1: break
        if i > 0 and s[i-1] == "1": flag = 1
        ans += dp[-i-1]
        #print(dp[-i-1])
    return ans

if __name__ == '__main__':
    print(f"f(5) = {findIntegers(5)} <-> 5");
    print(f"f(1) = {findIntegers(1)} <-> 2");
    print(f"f(2) = {findIntegers(2)} <-> 3");
    print(f"f(29374) = {findIntegers(29374)} <-> 1597");

#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

int findIntegers(int n)
{
    // The number of valid number of the sequence follow the Fibonacci series f(x) where x is
    // the number of binary digits in n. That is because each time we increase the number of
    // binary digits by one, we increase the number of valid numbers by f(x - 1), corresponding
    // to the numbers where we add a 0 in front of them, and f(x - 2), corresponding to the
    // where we add a 1 in front of them. The second half (where 1 is added) eliminates more
    // numbers because all binary numbers that began with 1 are eliminated.
    // Binary digits:      0  1  2  3  4   5   6   7   8   9   10   11   12   13   14    15
    constexpr int fib[] = {1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597,
    //                       16    17    18     19     20     21     22     23      24
                           2584, 4181, 6765, 10946, 17711, 28657, 46368, 75025, 121393,
    //                       25,       26,     27,     28,      29,      30,      31,      32
                           196418, 317811, 514229, 832040, 1346269, 2178309, 3524578, 5702887};
    bool binary[32];
    int length = -1;
    std::memset(binary, false, sizeof(binary));
    ++n;
    for (int i = 0; i < 32; ++i)
    {
        if (binary[i] = (n & 1))
            length = i;
        n >>= 1;
    }
    ++length;
    
    int result = 0;
    for (int i = length - 1; i >= 0; --i)
    {
        if (binary[i])
        {
            result += fib[i];
            if (binary[i + 1]) break;
        }
    }
    return result;
}

void test(int n, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findIntegers(n);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, 5, trials);
    test(1, 2, trials);
    test(2, 3, trials);
    test(29374, 1597, trials);
    return 0;
}

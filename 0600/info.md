=Non-negative Integers without Consecutive Ones=

Given a positive integer `n`, return the number of the integers in the range `[0, n]` whose binary representations do not contain consecutive ones.

**Example 1:**<br>
*Input:* `n = 5`<br>
*Output:* `5`<br>
*Explanation:*<br>
Here are the non-negative integers `<= 5` with their corresponding binary representations:
`0 : 0`<br>
`1 : 1`<br>
`2 : 10`<br>
`3 : 11`<br>
`4 : 100`<br>
`5 : 101`<br>
Among them, only integer `3` disobeys the rule (two consecutive ones) and the other `5` satisfy the rule.<br>

**Example 2:**<br>
*Input:* `n = 1`<br>
*Output:* `2`<br>

**Example 3:**<br>
*Input:* `n = 2`<br>
*Output:* `3`<br>

**Constraints:**<br>
- `1 <= n <= 10^9`




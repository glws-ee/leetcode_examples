#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <unordered_map>
#include <limits>

using namespace std;

constexpr int null = -1000;


int numberOfArithmeticSlices(std::vector<int> nums)
{
#if 1
    const int n = nums.size();
    int result = 0;
    std::unordered_map<int, std::vector<int> > histogram;
    int dp[n][n];
    std::memset(dp, 0, sizeof(dp));
    for (int i = 0; i < n; ++i) histogram[nums[i]].push_back(i);
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < i; j++) 
        {
            long next = 2l * nums[j] - nums[i];
            if (next < std::numeric_limits<int>::lowest()
             || next > std::numeric_limits<int>::max())
                continue;
            if (auto it = histogram.find(next); it != end(histogram)) 
            {
                for (int k : it->second) 
                {
                    if (k >= j) break;
                    dp[i][j] += dp[j][k] + 1;
                }
            }
            result += dp[i][j];
        }
    }
    return result;
#else
    const int n = nums.size();
    std::unordered_map<long, int> dp[n];
    int result = 0;
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < i; ++j)
        {
            long diff = static_cast<long>(nums[i]) - static_cast<long>(nums[j]);
            ++dp[i][diff];
            if (dp[j].find(diff) != dp[j].end())
            {
                dp[i][diff] += dp[j][diff];
                result += dp[j][diff];
            }
        }
    }
    return result;
#endif
}

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = numberOfArithmeticSlices(nums);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({2, 4, 6, 8, 10}, 7, trials);
    test({7, 7, 7, 7, 7}, 16, trials);
    return 0;
}

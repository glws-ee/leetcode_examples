#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

class Node
{
public:
    int val = 0;
    std::vector<Node *> children;

    Node() = default;
    Node(int _val) : val(_val) {}
    Node(int _val, std::vector<Node *> _children) : val(_val), children(_children) {}
    ~Node(void)
    {
        for (auto &n : children) delete n;
    }
};

Node * vec2tree(const std::vector<int> &tree)
{
    const int n = tree.size();
    if (n == 0) return nullptr;
    Node * root = new Node(tree[0]);
    std::queue<Node *> queue;
    queue.push(root);
    for (int i = 2; i < n; ++i)
    {
        Node * current = queue.front();
        queue.pop();
        int j;
        for (j = i; (j < n) && (tree[j] != null); ++j);
        current->children.resize(j - i, nullptr);
        for (int k = i; k < j; ++k)
            queue.push(current->children[k - i] = new Node(tree[k]));
        i = j;
    }
    return root;
}

// ############################################################################
// ############################################################################
std::vector<std::vector<int>> levelOrder(Node* root)
{
    if (root == nullptr) return {};
    else
    {
        std::vector<std::vector<int> > result;
        std::queue<Node *> queue;
        std::vector<int> current_level;
        queue.push(root);
        queue.push(nullptr);
        while (!queue.empty())
        {
            Node * current = queue.front();
            queue.pop();
            if (current == nullptr)
            {
                if (!queue.empty()) queue.push(nullptr);
                result.push_back(current_level);
                current_level.resize(0);
            }
            else
            {
                current_level.push_back(current->val);
                for (auto &n : current->children)
                    queue.push(n);
            }
        }
        return result;
    }
    //if (root != nullptr)
    //{
    //    std::cout << '{';
    //    bool next = false;
    //    for (auto &c : root->children)
    //    {
    //        if (next) [[likely]] std::cout << ", ";
    //        next = true;
    //        std::cout << ((c != nullptr)?c->val:null);
    //    }
    //    std::cout << "};\n";
    //    for (auto &c : root->children)
    //        levelOrder(c);
    //}
    //return {};
}
// ############################################################################
// ############################################################################

std::ostream& operator<<(std::ostream &out, const std::vector<int> &vec)
{
    out << '{';
    bool next = false;
    for (int v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<std::vector<int> > &vec)
{
    out << '{';
    bool next = false;
    for (const auto &v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    for (int i = 0; i < n; ++i)
        if (left[i] != right[i]) return false;
    return true;
}

bool operator==(const std::vector<std::vector<int> > &left, const std::vector<std::vector<int> > &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    for (int i = 0; i < n; ++i)
        if (left[i] != right[i]) return false;
    return true;
}

void test(std::vector<int> tree, std::vector<std::vector<int> > solution, unsigned int trials = 1)
{
    std::cout << "INPUT: " << tree << '\n';
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        Node * root = vec2tree(tree);
        result = levelOrder(root);
        delete root;
    }
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, null, 3, 2, 4, null, 5, 6}, {{1}, {3, 2, 4}, {5, 6}}, trials);
    test({1, null, 2, 3, 4, 5, null, null, 6, 7, null, 8, null, 9, 10, null, null, 11, null, 12, null, 13, null, null, 14}, {{1}, {2, 3, 4, 5}, {6, 7, 8, 9, 10}, {11, 12, 13}, {14}}, trials);
    return 0;
}

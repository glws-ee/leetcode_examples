=Longest Duplicate Substring=

Given a string `s`, consider all *duplicated substrings*: (contiguous) substrings of s that occur `2` or more times. The occurrences may overlap.

Return **any** duplicated substring that has the longest possible length. If `s` does not have a duplicated substring, the answer is `""`.

**Example 1:**<br>
*Input:* `s = "banana"`<br>
*Output:* `"ana"`<br>

**Example 2:**<br>
*Input:* `s = "abcd"`<br>
*Output:* `""`<br>

**Constraints:**<br>
- `2 <= s.length <= 3 * 10^4`
- `s` consists of lowercase English letters.




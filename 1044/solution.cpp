#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <unordered_map>

using namespace std;

constexpr int null = -1000;

std::string longestDupSubstring(std::string s)
{
#if 1
    const int n = s.size();
    constexpr int M = 433;
    std::vector<int> hashMap[M];
    
    auto check = [&](int size) -> std::pair<bool, int>
    {
        if (size == 0)
            return std::make_pair(true, 0);
        for (int i = 0; i < M; ++i)
            hashMap[i].clear();
        
        int hash = 0;
        int f = 1;
        for (int i = 0; i < n; ++i)
        {
            if (i < size - 1)
                f = (f * 31) % M;
            if (i >= size)
                hash = (M + (hash - (s[i - size] - 'a') * f) % M) % M;
            hash = ((hash * 31) % M + (s[i] - 'a')) % M;
            
            if (i < size - 1)
                continue;
            if (hashMap[hash].size() == 0)
                hashMap[hash].push_back(i);
            else
            {
                auto &entry = hashMap[hash];
                for (int index: entry)
                {
                    int tempsize = size;
                    for (int tempi = i; (tempsize > 0) && (s[index] == s[tempi]); --index, --tempi, --tempsize);
                    if (tempsize == 0) return std::make_pair(true, i);
                }
                entry.push_back(i);
            }
        }
        return std::make_pair(false, -1);
    };
    
    int lo = 0, hi = n - 1, index;
    bool res;
    while (lo < hi)
    {
        int mid = lo + (hi - lo + 1) / 2;
        std::tie(res, index) = check(mid);
        if (res == false)
            hi = mid - 1;
        else lo = mid;
    }
    std::tie(res, index) = check(lo);
    
    return s.substr(index - lo + 1, lo);
#else
    const int n = s.size();
    constexpr long base = 1e5 + 1, mod = 1e11 + 1;
    std::string result;
    
    auto search = [&](int &l, int &r) -> void
    {
        int m = (l + r + 1) / 2;
        long long hash = 0, d = 1;
        std::unordered_map<int, std::vector<int> > h;
        for (int i = 0; i < n; ++i)
        {
            hash = (hash * base + (s[i] - '0')) % mod;
            if (i >= m)
                hash = (hash + mod - d * (s[i - m] - '0') % mod) % mod;
            else d = d * base % mod;
            
            if (i >= m - 1)
            {
                for (auto pos : h[hash])
                {
                    if (std::equal(s.begin() + pos, s.begin() + pos + m, s.begin() + i - m + 1))
                    {
                        result = s.substr(i - m + 1, m);
                        l = m;
                        return;
                    }
                }
                h[hash].push_back(i - m + 1);
            }
        }
        r = m - 1;
    };
    
    for (int left_idx = 0, right_idx = n; left_idx < right_idx; )
        search(left_idx, right_idx);
    return result;
#endif
}

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestDupSubstring(s);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected '" << solution << "' and obtained '" << result << "'.\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("banana", "ana", trials);
    test("abcd", "", trials);
    
    return 0;
}

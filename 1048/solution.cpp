#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <algorithm>

using namespace std;

bool isPredecessor(const std::string &str_long, const std::string &str_short)
{
    bool skip = false;
    for (unsigned int i = 0, j = 0; i < str_long.size(); ++i)
    {
        if (str_long[i] != str_short[j])
        {
            if (skip) return false;
            skip = true;
        }
        else ++j;
    }
    return true;
}
// words.length <= 1000
// words[i].length <= 16
int longestStrChain(std::vector<std::string> &words)
{
    std::pair<short, short> length[words.size()];
    short lhist[17];
    for (short i = 0; i < 17; ++i) lhist[i] = 0;
    for (short i = 0; i < words.size(); ++i)
    {
        const short s = words[i].size();
        length[i] = {s, i};
        ++lhist[s];
    }
    short max_hist = 0;
    for (short i = 0; i < 17; ++i)
        if (lhist[i] > max_hist)
            max_hist = lhist[i];
    short chain_length[2][max_hist];
    for (short i = 0; i < max_hist; ++i)
        chain_length[0][i] = chain_length[1][i] = 0;
    std::sort(length, length + words.size());
    short max_size = length[words.size() - 1].first;
    short min_size = length[0].first;
    
    int longest_chain = 0;
    bool current_chain = false;
    for (short s = max_size, p = words.size() - 1; s > min_size; --s)
    {
        short n = p - lhist[s];
        short e = n - lhist[s - 1];
        for (short j = n; j > e; --j)
        {
            chain_length[current_chain][n - j] = 0;
            for (short i = p; i > n; --i)
                if (isPredecessor(words[length[i].second], words[length[j].second]))
                    chain_length[current_chain][n - j] = std::max(static_cast<short>(chain_length[!current_chain][p - i] + 1),
                                                                  chain_length[current_chain][n - j]);
        }
        for (short j = n; j > e; --j)
            longest_chain = std::max(longest_chain, static_cast<int>(chain_length[current_chain][n - j]));
        p -= lhist[s];
        current_chain = !current_chain;
    }
    
    return longest_chain + 1;
}

void test(std::vector<std::string> words, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = longestStrChain(words);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"a", "b", "ba", "bca", "bda", "bdca"}, 4, trials);
    test({"xbc", "pcxbcf", "xb", "cxbc", "pcxbc"}, 5, trials);
    test({"sanfrancisca", "bcela", "bacelona", "sanfranciscosa", "barcelona", "sanfrancisco", "bacelna", "sanfranciscos", "l", "cela", "cla", "cl", "bcelna", "fadslkajfld"}, 9, trials);
    return 0;
}

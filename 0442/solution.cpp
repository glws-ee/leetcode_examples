#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <unordered_map>

using namespace std;

constexpr int null = -1000;

std::vector<int> findDuplicates(std::vector<int> nums)
{
#if 1
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(NULL);
    const int n = nums.size();
    std::vector<int> result;
    result.reserve(n);
    int histogram[n + 1];
    std::memset(histogram, 0, sizeof(histogram));
    for (int v : nums)
    {
        ++histogram[v];
        if (histogram[v] == 2)
            result.push_back(v);
    }
    return result;
#else
    const int n = nums.size();
    std::vector<int> result;
    result.reserve(n);
    int histogram[n + 1];
    std::memset(histogram, 0, sizeof(histogram));
    for (int n : nums)
        ++histogram[n];
    for (int i = 0; i <= n; ++i)
        if (histogram[i] == 2)
            result.push_back(i);
    return result;
#endif
}

std::ostream& operator<<(std::ostream &out, std::vector<int> &vec)
{
    out << '{';
    bool next = false;
    for (int n : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << n;
    }
    out << '}';
    return out;
}

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    std::unordered_map<int, int> unique;
    for (int l : left)
        ++unique[l];
    for (int r : right)
    {
        if (auto it = unique.find(r); it != unique.end())
        {
            --it->second;
            if (it->second == 0)
                unique.erase(it);
        }
        else return false;
    }
    return unique.size() == 0;
}

void test(std::vector<int> nums, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findDuplicates(nums);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 3, 2, 7, 8, 2, 3, 1}, {2, 3}, trials);
    test({1, 1, 2}, {1}, trials);
    test({1}, {}, trials);
    return 0;
}

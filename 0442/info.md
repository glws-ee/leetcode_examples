=Find All Duplicates in an Array=

Given an integer array `nums` of length `n` where all the integers of `nums` are in the range `[1, n]` and each integer appears **once** or **twice**, return *an array of all the integers that appears* ***twice***.

You must write an algorithm that runs in `O(n)` time and uses only constant extra space.

**Example 1:**<br>
*Input:* `nums = [4, 3, 2, 7, 8, 2, 3, 1]`<br>
*Output:* `[2, 3]`<br>

**Example 2:**<br>
*Input:* `nums = [1, 1, 2]`<br>
*Output:* `[1]`<br>

**Example 3:**<br>
*Input:* `nums = [1]`<br>
*Output:* `[]`<br>
 
**Constraints:**<br>
- `n == nums.length`
- `1 <= n <= 10^5`
- `1 <= nums[i] <= n`
- Each element in `nums` appears **once** or **twice**.


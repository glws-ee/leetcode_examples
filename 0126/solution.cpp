#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <unordered_set>
#include <unordered_map>

using namespace std;

constexpr int null = -1000;

std::vector<std::vector<std::string>>
findLadders(
        std::string beginWord,
        std::string endWord,
        std::vector<std::string> wordList
        )
{
#if 0
    std::unordered_map<std::string, int> dictionary;
    const int n = wordList.size();
    if (n > 10)
    {
        std::cerr << "TOO MANY ELEMENTS WILL END UP SING ALL MEMORY. THIS SOLUTION IS WRONG BECAUSE YOU HAVE TO RETURN THE SHORTEST PATH SO YOU DON'T HAVE TO KEEP AL POSSIBLE WORDS FOR EACH INDIVIDUAL PATH\n\n\n";
        return {};
    }
    for (int idx = 0; const auto &w : wordList)
        dictionary[w] = idx++;
    if (dictionary.find(endWord) == dictionary.end()) return {};
    std::queue<std::pair<std::vector<std::string>, std::vector<bool> > > q;
    std::vector<std::vector<std::string> > result;
    
    q.push({{beginWord}, std::vector<bool>(n, true)});
    for (bool found = false; !(found || q.empty());)
    {
        const int current_level_length = q.size();
        for (int i = 0; i < current_level_length; ++i)
        {
            auto [path, available] = std::move(q.front());
            q.pop();
            
            std::vector<std::string> words;
            std::string current = path.back();
            for (int i = 0; i < current.size(); ++i)
            {
                char previous_character = current[i];
                for (char c = 'a'; c <= 'z'; ++c)
                {
                    current[i] = c;
                    if (auto it = dictionary.find(current); (it != dictionary.end()) && (available[it->second]))
                        words.push_back(current);
                }
                current[i] = previous_character;
            }
            
            for (const auto &w: words)
            {
                auto new_path = path;
                auto new_available = available; 
                new_path.push_back(w);
                new_available[dictionary[w]] = false;
                if (w == endWord)
                {
                    found = true;
                    result.push_back(new_path);
                }
                q.push({std::move(new_path), std::move(new_available)});
            }
        }
    }
    return result;
#elif 0
    if (wordList.size() > 10)
    {
        std::cerr << "TOO MANY ELEMENTS WILL END UP SING ALL MEMORY. THIS SOLUTION IS WRONG BECAUSE YOU HAVE TO RETURN THE SHORTEST PATH SO YOU DON'T HAVE TO KEEP AL POSSIBLE WORDS FOR EACH INDIVIDUAL PATH\n\n\n";
        return {};
    }
    std::queue<std::pair<std::vector<std::string>, std::unordered_set<std::string> > > q;
    std::vector<std::vector<std::string> > result;
    
    q.push({{beginWord}, std::unordered_set<std::string>(wordList.begin(), wordList.end())});
    for (bool found = false; !(found || q.empty());)
    {
        const int current_level_length = q.size();
        for (int i = 0; i < current_level_length; ++i)
        {
            auto [path, dictionary] = std::move(q.front());
            q.pop();
            
            std::vector<std::string> words;
            std::string current = path.back();
            for (int i = 0; i < current.size(); ++i)
            {
                char previous_character = current[i];
                for (char c = 'a'; c <= 'z'; ++c)
                {
                    current[i] = c;
                    if (dictionary.find(current) != dictionary.end())
                        words.push_back(current);
                }
                current[i] = previous_character;
            }
            
            for (const auto &w: words)
            {
                auto new_path = path;
                auto new_dictionary = dictionary; 
                new_path.push_back(w);
                new_dictionary.erase(w);
                if (w == endWord)
                {
                    found = true;
                    result.push_back(new_path);
                }
                q.push({std::move(new_path), std::move(new_dictionary)});
            }
        }
    }
    return result;
#else
    std::queue<std::vector<std::string> > q;
    std::vector<std::vector<std::string> > result;
    std::unordered_set<std::string> dictionary(wordList.begin(), wordList.end());
    if (dictionary.find(endWord) == dictionary.end()) return {};
    
    q.push({beginWord});
    for (bool found = false; !q.empty();)
    {
        std::unordered_set<std::string> visited;
        
        const int current_level_length = q.size();
        for (int i = 0; i < current_level_length; ++i)
        {
            std::vector<std::string> curr = std::move(q.front());
            q.pop();
            
            std::vector<std::string> words;
            std::string current = curr.back();
            for (int i = 0; i < current.size(); ++i)
            {
                char previous_character = current[i];
                for (char c = 'a'; c <= 'z'; ++c)
                {
                    current[i] = c;
                    if (dictionary.find(current) != dictionary.end())
                        words.push_back(current);
                }
                current[i] = previous_character;
            }
            
            for (auto s: words)
            {
                std::vector<std::string> new_path(curr.begin(), curr.end());
                new_path.push_back(s);
                if (s == endWord)
                {
                    found = true;
                    result.push_back(new_path);
                }
                visited.insert(s);
                q.push(new_path);
            }
        }
        if (found) break;
        for (auto s: visited) dictionary.erase(s);
    }
    return result;
#endif
}

bool operator==(const std::vector<std::string> &left, const std::vector<std::string> &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    for (int i = 0; i < n; ++i)
        if (left[i] != right[i]) return false;
    return true;
}
bool operator==(const std::vector<std::vector<std::string> > &left, const std::vector<std::vector<std::string> > &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    bool not_used[n];
    std::memset(not_used, true, sizeof(not_used));
    for (int i = 0; i < n; ++i)
    {
        bool found = false;
        for (int j = 0; (!found) && (j < n); ++j)
        {
            if ((not_used[i]) && (left[i] == right[j]))
            {
                found = true;
                not_used[i] = false;
            }
        }
        if (!found) return false;
    }
    return true;
}
std::ostream& operator<<(std::ostream &out, const std::vector<std::vector<std::string> > &info)
{
    out << '{';
    bool outer_next = false;
    for (const auto &vec : info)
    {
        if (outer_next) [[likely]] out << ", ";
        outer_next = true;
        out << '{';
        bool inner_next = false;
        for (const auto &s : vec)
        {
            if (inner_next) [[likely]] out << ", ";
            inner_next = true;
            out << s;
        }
        out << '}';
    }
    out << '}';
    return out;
}

void test(std::string beginWord, std::string endWord, std::vector<std::string> wordList, std::vector<std::vector<std::string> > solution, unsigned int trials = 1)
{
    std::vector<std::vector<std::string> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findLadders(beginWord, endWord, wordList);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 100'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("hit", "cog", {"hot", "dot", "dog", "lot", "log", "cog"}, {{"hit", "hot", "dot", "dog", "cog"}, {"hit", "hot", "lot", "log", "cog"}}, trials);
    test("hit", "cog", {"hot", "dot", "dog", "lot", "log"}, {}, trials);
    test("cet", "ism", {"kid","tag","pup","ail","tun","woo","erg","luz","brr","gay","sip","kay","per","val","mes","ohs","now","boa","cet","pal","bar","die","war","hay","eco","pub","lob","rue","fry","lit","rex","jan","cot","bid","ali","pay","col","gum","ger","row","won","dan","rum","fad","tut","sag","yip","sui","ark","has","zip","fez","own","ump","dis","ads","max","jaw","out","btu","ana","gap","cry","led","abe","box","ore","pig","fie","toy","fat","cal","lie","noh","sew","ono","tam","flu","mgm","ply","awe","pry","tit","tie","yet","too","tax","jim","san","pan","map","ski","ova","wed","non","wac","nut","why","bye","lye","oct","old","fin","feb","chi","sap","owl","log","tod","dot","bow","fob","for","joe","ivy","fan","age","fax","hip","jib","mel","hus","sob","ifs","tab","ara","dab","jag","jar","arm","lot","tom","sax","tex","yum","pei","wen","wry","ire","irk","far","mew","wit","doe","gas","rte","ian","pot","ask","wag","hag","amy","nag","ron","soy","gin","don","tug","fay","vic","boo","nam","ave","buy","sop","but","orb","fen","paw","his","sub","bob","yea","oft","inn","rod","yam","pew","web","hod","hun","gyp","wei","wis","rob","gad","pie","mon","dog","bib","rub","ere","dig","era","cat","fox","bee","mod","day","apr","vie","nev","jam","pam","new","aye","ani","and","ibm","yap","can","pyx","tar","kin","fog","hum","pip","cup","dye","lyx","jog","nun","par","wan","fey","bus","oak","bad","ats","set","qom","vat","eat","pus","rev","axe","ion","six","ila","lao","mom","mas","pro","few","opt","poe","art","ash","oar","cap","lop","may","shy","rid","bat","sum","rim","fee","bmw","sky","maj","hue","thy","ava","rap","den","fla","auk","cox","ibo","hey","saw","vim","sec","ltd","you","its","tat","dew","eva","tog","ram","let","see","zit","maw","nix","ate","gig","rep","owe","ind","hog","eve","sam","zoo","any","dow","cod","bed","vet","ham","sis","hex","via","fir","nod","mao","aug","mum","hoe","bah","hal","keg","hew","zed","tow","gog","ass","dem","who","bet","gos","son","ear","spy","kit","boy","due","sen","oaf","mix","hep","fur","ada","bin","nil","mia","ewe","hit","fix","sad","rib","eye","hop","haw","wax","mid","tad","ken","wad","rye","pap","bog","gut","ito","woe","our","ado","sin","mad","ray","hon","roy","dip","hen","iva","lug","asp","hui","yak","bay","poi","yep","bun","try","lad","elm","nat","wyo","gym","dug","toe","dee","wig","sly","rip","geo","cog","pas","zen","odd","nan","lay","pod","fit","hem","joy","bum","rio","yon","dec","leg","put","sue","dim","pet","yaw","nub","bit","bur","sid","sun","oil","red","doc","moe","caw","eel","dix","cub","end","gem","off","yew","hug","pop","tub","sgt","lid","pun","ton","sol","din","yup","jab","pea","bug","gag","mil","jig","hub","low","did","tin","get","gte","sox","lei","mig","fig","lon","use","ban","flo","nov","jut","bag","mir","sty","lap","two","ins","con","ant","net","tux","ode","stu","mug","cad","nap","gun","fop","tot","sow","sal","sic","ted","wot","del","imp","cob","way","ann","tan","mci","job","wet","ism","err","him","all","pad","hah","hie","aim","ike","jed","ego","mac","baa","min","com","ill","was","cab","ago","ina","big","ilk","gal","tap","duh","ola","ran","lab","top","gob","hot","ora","tia","kip","han","met","hut","she","sac","fed","goo","tee","ell","not","act","gil","rut","ala","ape","rig","cid","god","duo","lin","aid","gel","awl","lag","elf","liz","ref","aha","fib","oho","tho","her","nor","ace","adz","fun","ned","coo","win","tao","coy","van","man","pit","guy","foe","hid","mai","sup","jay","hob","mow","jot","are","pol","arc","lax","aft","alb","len","air","pug","pox","vow","got","meg","zoe","amp","ale","bud","gee","pin","dun","pat","ten","mob"}, {{"cet", "get", "gee", "gte", "ate", "ats", "its", "ito", "ibo", "ibm", "ism"}, {"cet", "cat", "can", "ian", "inn", "ins", "its", "ito", "ibo", "ibm", "ism"}, {"cet", "cot", "con", "ion", "inn", "ins", "its", "ito", "ibo", "ibm", "ism"}}, trials);


    return 0;
}

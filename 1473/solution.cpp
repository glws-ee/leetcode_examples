#include <vector>
#include <cstring>
#include <iostream>

using namespace std;

static constexpr int MAXVALUE = 10000001;

int minCost(int dp[100][101][21], int pstn, int prev,
            vector<int>& houses, vector<vector<int>>& cost, int m, int n, int target)
{
    if (pstn == m)
        return (target == 0)?0:MAXVALUE;
    if (target < 0) return MAXVALUE;
    if (target > m - pstn + 1) return MAXVALUE;
    if (dp[pstn][target][prev] != -1)
        return dp[pstn][target][prev];
    int rslt = MAXVALUE;
    if (houses[pstn] == 0)
    {
        for (int j = 0; j < n; ++j)
        {
            rslt = std::min(rslt, cost[pstn][j] +
                            minCost(dp,
                                    pstn + 1,
                                    j + 1,
                                    houses, cost, m, n,
                                    target - (j + 1 != prev)));
        }
    }
    else rslt = minCost(dp,
                        pstn + 1,
                        houses[pstn],
                        houses, cost, m, n,
                        target - (houses[pstn] != prev));
    dp[pstn][target][prev] = rslt;
    return rslt;
}

int minCost(vector<int>& houses, vector<vector<int>>& cost, int m, int n, int target)
{
    // This dp is to avoid recomputing the 'tails' when at a certaint house index, we
    // already have the cost for a certain target and previous color (the algorithm
    // will follow the same path).
    int dp[100][101][21];
    std::memset(dp, -1, sizeof(dp));
    //for (unsigned int i = 0; i < m; ++i)
    //    for (unsigned int j = 0; j <= target; ++j)
    //        for (unsigned int k = 0; k <= n; ++k)
    //            dp[i][j][k] = -1;
    int rslt = minCost(dp, 0, 0, houses, cost, m, n, target);
    return (rslt < MAXVALUE)?rslt:-1;
}


void test(std::vector<int> houses, vector<vector<int> > cost, int m, int n, int target, int solution)
{
    int result = minCost(houses, cost, m, n, target);
    if (result == solution) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
    test({0, 0, 0, 0, 0}, {{1, 10}, {10, 1}, {10, 1}, {1, 10}, {5, 1}}, 5, 2, 3, 9);
    test({0, 2, 1, 2, 0}, {{1, 10}, {10, 1}, {10, 1}, {1, 10}, {5, 1}}, 5, 2, 3, 11);
    test({0, 0, 0, 0, 0}, {{1, 10}, {10, 1}, {1, 10}, {10, 1}, {1, 10}}, 5, 2, 5, 5);
    test({3, 1, 2, 3}, {{1, 1, 1}, {1, 1, 1}, {1, 1, 1}, {1, 1, 1}}, 4, 3, 3, -1);
    return 0;
}


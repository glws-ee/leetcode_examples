#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include "values_testA.hpp"

using namespace std;

int numMatchingSubseq(std::string s, std::vector<std::string> words)
{
    const std::size_t n = words.size();
    int result = 0;
    if (n < 26)
    {
        std::size_t position[n];
        for (std::size_t i = 0; i < n; ++i)
            position[i] = 0;
        
        for (char c : s)
        {
            for (std::size_t i = 0; i < n; ++i)
                if ((position[i] < words[i].size()) && (c == words[i][position[i]]))
                    ++position[i];
        }
        for (std::size_t i = 0; i < n; ++i)
            if (position[i] == words[i].size())
                ++result;
    }
    else
    {
        std::pair<std::size_t, std::size_t> lut[26][5'000], aux[5'000];
        std::size_t position[26];
        for (std::size_t i = 0; i < 26; ++i)
            position[i] = 0;
        for (std::size_t i = 0; i < words.size(); ++i)
            lut[words[i][0] - 'a'][position[words[i][0] - 'a']++] = {0, i};
        for (char c : s)
        {
            std::size_t idx = c - 'a';
            std::size_t position_aux = 0;
            for (std::size_t i = 0; i < position[idx]; ++i)
            {
                auto [str_pos, word_pos] = lut[idx][i];
                ++str_pos;
                if (str_pos < words[word_pos].size()) [[likely]]
                {
                    std::size_t new_idx = words[word_pos][str_pos] - 'a';
                    if (new_idx == idx)
                        aux[position_aux++] = {str_pos, word_pos};
                    else lut[new_idx][position[new_idx]++] = {str_pos, word_pos};
                }
                else ++result;
            }
            position[idx] = 0;
            for (std::size_t i = 0; i < position_aux; ++i)
                lut[idx][position[idx]++] = aux[i];
        }
    }
    return result;
}

void test(std::string s, std::vector<std::string> words, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = numMatchingSubseq(s, words);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    //constexpr unsigned int trials = 1;
    constexpr unsigned int trials = 10'000;
#endif
    test("abcde", {"a", "bb", "acd", "ace"}, 3, trials);
    test("dsahjpjauf", {"ahjpjau", "ja", "ahbwzgqnuk", "tnmlanowax"}, 2, trials);
    test(testA::s, testA::words, 1000, trials);
    return 0;
}

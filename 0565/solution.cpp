#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include "test_a.hpp"

using namespace std;

constexpr int null = -1000;

int arrayNesting(std::vector<int> nums)
{
#if 0
    // It works, but it's wrong.
    const short n = nums.size();
    short jumps[n];
    std::memset(jumps, 0, sizeof(jumps));
    short result = 0;
    for (short i = 0; i < n; ++i)
    {
        if (!jumps[i])
        {
            const short label = -i - 1;
            short current = 0;
            int pos;
            for (pos = i; !jumps[pos]; ++current, pos = nums[pos])
                jumps[pos] = label;
            if (jumps[pos] < 0) pos = -(jumps[i] + 1);
            if (jumps[pos] > 0)
                current += jumps[pos];
            jumps[i] = current;
            result = std::max(result, current);
        }
    }
    return result;
#else
    const int n = nums.size();
    int jumps[n];
    std::memset(jumps, 0, sizeof(jumps));
    int result = 0;
    for (int i = 0; i < n; ++i)
    {
        if (!jumps[i])
        {
            int current = 0, pos;
            for (pos = i; !jumps[pos]; ++current, pos = nums[pos])
                jumps[pos] = -1;
            if (jumps[pos] > 0)
                current += jumps[pos];
            result = std::max(result, current);
            for (pos = i; jumps[pos] == -1; --current, pos = nums[pos]) jumps[pos] = current;
        }
    }
    return result;
#endif
}

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = arrayNesting(nums);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 10'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 4, 0, 3, 1, 6, 2}, 4, trials);
    test({0, 1, 2}, 1, trials);
    test({1, 2, 0, 0, 3, 4, 5}, 7, trials);
    test(testA::nums, testA::solution, trials);
    return 0;
}

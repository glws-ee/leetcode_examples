#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <limits>

using namespace std;

constexpr int null = -1000;

std::string minWindow(std::string s, std::string t)
{
    int length = std::numeric_limits<int>::max(), begin = 0, previous = 0, count = t.size();
    const int n = s.length();
    int histogram[128];
    std::memset(histogram, 0, sizeof(histogram));
    for (char c: t) ++histogram[c];
    for (int i = 0; i < n; ++i)
    {
        if (histogram[s[i]] > 0)
            --count;
        --histogram[s[i]];
        while (!count && (i >= previous))
        {
            if (i - previous + 1 < length)
            {
                length = i - previous + 1;
                begin = previous;
            }
            ++histogram[s[previous]];
            if (histogram[s[previous++]] > 0)
                count++;
        }
        
    }
    return (length == std::numeric_limits<int>::max())?"":s.substr(begin, length);
}

void test(std::string s, std::string t, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minWindow(s, t);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("ADOBECODEBANC", "ABC", "BANC", trials);
    test("a", "a", "a", trials);
    test("a", "aa", "", trials);
    return 0;
}

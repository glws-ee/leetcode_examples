#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

std::string complexNumberMultiply(std::string num1, std::string num2)
{
    auto convert = [](const std::string &n) -> std::pair<int, int>
    {
        int mid = n.find('+');
        return {std::stoi(n.substr(0, mid)),
                std::stoi(n.substr(mid + 1, n.size() - mid - 2))};
    };
    auto val1 = convert(num1);
    auto val2 = convert(num2);
    return std::to_string(val1.first * val2.first - val1.second * val2.second) + "+"
           + std::to_string(val1.first * val2.second + val2.first * val1.second) + "i";
}

void test(std::string num1, std::string num2, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = complexNumberMultiply(num1, num2);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("1+1i", "1+1i", "0+2i", trials);
    test("1+-1i", "1+-1i", "0+-2i", trials);
    return 0;
}

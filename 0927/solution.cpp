#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <numeric>

using namespace std;

std::ostream& operator<<(std::ostream &out, const std::vector<int> &vec)
{
    out << '{';
    bool next = false;
    for (int v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    for (int i = 0; i < n; ++i)
        if (left[i] != right[i]) return false;
    return true;
}

std::vector<int> threeEqualParts(std::vector<int> arr)
{
#if 1
    int number_of_ones = std::accumulate(arr.begin(), arr.end(), 0);
    const int n = arr.size();
    if (number_of_ones == 0)
        return {0, n - 1};
    else if (number_of_ones % 3 != 0)
        return {-1, -1};
    number_of_ones /= 3;
    std::vector<int>::const_iterator begin[3];
    int count = 1;
    for (int i = 0, sequence_number = 0; sequence_number < 3; ++i)
    {
        if (arr[i])
        {
            if (count == 1)
            {
                begin[sequence_number] = arr.begin() + i;
                ++sequence_number;
                count = number_of_ones;
            }
            else --count;
        }
    }
    for (const auto end = arr.end(); begin[2] != end; ++begin[0], ++begin[1], ++begin[2])
        if ((*begin[0] != *begin[1]) || (*begin[0] != *begin[2]))
            return {-1, -1};
    return {n + static_cast<int>(std::distance(begin[2], begin[0])) - 1,
            n + static_cast<int>(std::distance(begin[2], begin[1]))};
#else
    int number_of_ones = std::accumulate(arr.begin(), arr.end(), 0);
    const int n = arr.size();
    if (number_of_ones == 0)
        return {0, n - 1};
    else if (number_of_ones % 3 != 0)
        return {-1, -1};
    number_of_ones /= 3;
    int begin[3];
    int count = 1;
    for (int i = 0, sequence_number = 0; sequence_number < 3; ++i)
    {
        if (arr[i])
        {
            if (count == 1)
            {
                begin[sequence_number] = i;
                ++sequence_number;
                count = number_of_ones;
            }
            else --count;
        }
    }
    for (int i = 0; begin[2] < n; ++i)
    {
        if ((arr[begin[0]] != arr[begin[1]]) || (arr[begin[0]] != arr[begin[2]]))
            return {-1, -1};
        else
        {
            ++begin[0];
            ++begin[1];
            ++begin[2];
        }
    }
    if (begin[2] != n) return {-1, -1};
    return {begin[0] - 1, begin[1]};
#endif
}

void test(std::vector<int> arr, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = threeEqualParts(arr);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 0, 1, 0, 1}, {0, 3}, trials);
    test({1, 1, 0, 1, 1}, {-1, -1}, trials);
    test({1, 1, 0, 0, 1}, {0, 2}, trials);
    test({1, 0, 1, 1, 0}, {-1, -1}, trials);
    test({0, 0, 0, 0, 0}, {0, 4}, trials);
    test({0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1}, {4, 12}, trials);
    test({0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1}, {4, 12}, trials);
    test({0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1}, {4, 14}, trials);
    test({1,1,1,0,1,0,0,1,0,1,0,0,0,1,0,0,1,1,1,0,1,0,0,1,0,1,0,0,0,1,0,0,0,0,1,1,1,0,1,0,0,1,0,1,0,0,0,1,0,0}, {15, 32}, trials);
    return 0;
}

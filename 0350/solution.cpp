#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <unordered_map>

using namespace std;

constexpr int null = -1000;

std::vector<int> intersect(std::vector<int> nums1, std::vector<int> nums2)
{
#if 1
    short lut[1024];
    std::memset(lut, 0, sizeof(lut));
    std::vector<int> result;
    result.reserve(std::min(nums1.size(), nums2.size()));
    for (int v: nums1) ++lut[v];
    for (int v: nums2)
        if (--lut[v] >= 0)
            result.push_back(v);
    return result;
#else
    std::unordered_map<int, int> lut;
    std::vector<int> result;
    for (int v : nums1) ++lut[v];
    for (int v : nums2)
    {
        if (auto it = lut.find(v); it != lut.end())
        {
            if (--it->second >= 0) result.push_back(v);
        }
    }
    return result;
#endif
}

std::ostream& operator<<(std::ostream &out, std::vector<int> &vec)
{
    out << '{';
    bool next = false;
    for (int v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    std::unordered_map<int, int> map_lut;
    for (int v : left)
        map_lut[v] += 1;
    for (int v : right)
    {
        if (auto it = map_lut.find(v); it != map_lut.end())
        {
            if (it->second <= 0) return false;
            else --it->second;
        }
        else return false;
    }
    return true;
}

void test(std::vector<int> num1, std::vector<int> num2, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = intersect(num1, num2);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 2, 1}, {2, 2}, {2, 2}, trials);
    test({4, 9, 5}, {9, 4, 9, 8, 4}, {4, 9}, trials);
    test({4, 4, 9, 5}, {9, 4, 9, 8, 4}, {4, 9, 4}, trials);
    test({4, 4, 4, 9, 5}, {9, 4, 9, 8, 4}, {4, 9, 4}, trials);
    return 0;
}

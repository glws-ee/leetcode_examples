=Maximum Length of Repeated Subarray=
Given two integer arrays `nums1` and `nums2`, return the *maximum length of a subarray that appears in* ***both*** *arrays*.

**Example 1:**<br>
*Input:* `nums1 = [1, 2, 3, 2, 1], nums2 = [3, 2, 1, 4, 7]`<br>
*Output:* `3`<br>
*Explanation:* The repeated subarray with maximum length is `[3, 2, 1]`.<br>

**Example 2:**<br>
*Input:* `nums1 = [0, 0, 0, 0, 0], nums2 = [0, 0, 0, 0, 0]`<br>
*Output:* `5`<br>

**Constraints:**<br>
- `1 <= nums1.length, nums2.length <= 1000`
- `0 <= nums1[i], nums2[i] <= 100`

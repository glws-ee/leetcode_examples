#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <unordered_set>

using namespace std;

int findLength(std::vector<int> nums1, std::vector<int> nums2)
{
#if 1
    const int n1 = nums1.size() + 1;
    const int n2 = nums2.size() + 1;
    int dp[2][n2];
    bool previous = false;
    int result = 0;
    
    for(int i = 0; i < n2; i++)
        dp[previous][i] = 0;
    dp[!previous][0] = 0;
    for(int i = 1; i < n1; i++)
    {
        previous = !previous;
        for(int j = 1; j < n2; j++)
            result = std::max(result, dp[previous][j] = (nums1[i - 1] == nums2[j - 1])?(dp[!previous][j - 1] + 1):0);
    }
  
    return result;
#else
    std::unordered_set<std::string> subarrays;
    const int n1 = nums1.size();
    for (int i = 0; i < n1; ++i)
        for (int j = i; j < n1; ++j)
            subarrays.insert(std::string(&nums1[i], &nums1[j + 1]));
    const int n2 = nums2.size();
    int result = 0;
    for (int i = 0; i < n2 - result; ++i)
    {
        for (int j = i + result; j < n2; ++j)
        {
            std::string search(std::string(&nums2[i], &nums2[j + 1]));
            if (subarrays.find(search) != subarrays.end())
                result = std::max(result, j - i + 1);
        }
    }
    return result;
#endif
}

void test(std::vector<int> nums1, std::vector<int> nums2, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findLength(nums1, nums2);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 2, 1}, {3, 2, 1, 4, 7}, 3, trials);
    test({0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}, 5, trials);
    return 0;
}

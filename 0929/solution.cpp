#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <unordered_set>

using namespace std;

constexpr int null = -1000;

int numUniqueEmails(std::vector<std::string> emails)
{
    std::unordered_set<std::string> unique;
    for (const std::string &current : emails)
    {
        const int n = current.size();
        std::string address;
        int i;
        bool copy = true;
        for (i = 0; (current[i] != '@') && (i < n); ++i)
        {
            if (current[i] == '+') copy = false;
            if (copy && (current[i] != '.'))
                    address += current[i];
        }
        address += current.substr(i, n - i);
        unique.insert(address);
    }
    return unique.size();
}

void test(std::vector<std::string> emails, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = numUniqueEmails(emails);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"test.email+alex@leetcode.com", "test.e.mail+bob.cathy@leetcode.com", "testemail+david@lee.tcode.com"}, 2, trials);
    test({"a@leetcode.com","b@leetcode.com","c@leetcode.com"}, 3, trials);
    return 0;
}

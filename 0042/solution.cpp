#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

int trap(std::vector<int> height)
{
#if 1
    const int n = height.size();
    if (n < 3) return 0;
    int left = -1;
    int left_height = 0;
    int right = n;
    int right_height = 0;
    int water = 0;
    while (left < right)
    {
        if (left_height < right_height)
        {
            ++left;
            if (height[left] >= left_height)
                left_height = height[left];
            else
                water += left_height - height[left];
        }
        else
        {
            --right;
            if (height[right] >= right_height)
                right_height = height[right];
            else
                water += right_height - height[right];
        }
    }
    return water;
#else
    const int n = height.size();
    if (n < 3) return 0;
    int left = -1;
    int left_height = 0;
    int right = n;
    int right_height = 0;
    int water = 0;
    while (left < right)
    {
        for (int v : height)
            std::cout << v << ' ';
        std::cout << '\n';
        if (left_height < right_height)
        {
            ++left;
            for (int k = 0; k < left; ++k)
                std::cout << "  ";
            std::cout << '*';
            if (height[left] >= left_height)
                left_height = height[left];
            else
                water += left_height - height[left];
            std::cout << " => " << left_height << ' ' << height[left] << ' ' << water << '\n';
        }
        else
        {
            --right;
            for (int k = 0; k < right; ++k)
                std::cout << "  ";
            std::cout << '*';
            if (height[right] >= right_height)
                right_height = height[right];
            else
                water += right_height - height[right];
            std::cout << " => " << right_height << ' ' << height[right] << ' ' << water << '\n';
        }
        std::cout << '\n';
    }
    return water;
#endif
}

void test(std::vector<int> height, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = trap(height);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1}, 6, trials);
    test({4, 2, 0, 3, 2, 5}, 9, trials);
    return 0;
}

=Trapping Rain Water=

Given `n` non-negative integers representing an elevation map where the width of each bar is `1`, compute how much water it can trap after raining.
 
**Example 1:**<br>
*Input:* `height = [0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]`<br>
*Output:* `6`<br>
*Explanation:*<br>
| *0* | *1* | *0* | *2* | *1* | *0* | *1* | *3* | *2* | *1* | *2* | *1* |
| - | - | - | - | - | - | - | - | - | - | - | - |
| **.** | **.** | **.** | **.** | **.** | **.** | **.** | **.** | **.** | **.** | **.** | **.** |
| **.** | **.** | **.** | **.** | **.** | **.** | **.** | **#** | **.** | **.** | **.** | **.** |
| **.** | **.** | **.** | **#** | **w** | **w** | **w** | **#** | **#** | **w** | **#** | **.** |
| **.** | **#** | **w** | **#** | **#** | **w** | **#** | **#** | **#** | **#** | **#** | **#** |
The above elevation map (`#` symbols) is represented by array `[0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]`. In this case, `6` units of rain water (`w` symbols) are being trapped.<br>

**Example 2:**<br>
*Input:* `height = [4, 2, 0, 3, 2, 5]`<br>
*Output:* `9`
 
**Constraints:**<br>
- `n == height.length`
- `0 <= n <= 3 * 10^4`
- `0 <= height[i] <= 10^5`




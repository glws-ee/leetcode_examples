#include <vector>
#include <cstring>
#include <iostream>
#include <tuple>
#include <cassert>

using namespace std;

class NumMatrix {
public:
    NumMatrix(vector<vector<int>>& matrix)
    {
        ncols = (matrix.size() > 0)?(matrix[0].size() + 1):0;
        integral.resize((matrix.size() + 1) * ncols, 0);
        int idx = ncols + 1;
        for (int row = 0; row < matrix.size(); ++row)
        {
            //assert(ncols == matrix[row].size() + 1);
            for (int col = 0; col < ncols - 1; ++col)
            {
                integral[idx] = matrix[row][col]
                              + integral[idx - 1]
                              + integral[idx - ncols]
                              - integral[idx - ncols - 1];
                ++idx;
            }
            ++idx;
        }
#if 0
        int debug = 0;
        for (const auto &v : integral)
        {
            if (debug % ncols == 0) std::cout << '\n';
            std::cout << ' ' << v;
            ++debug;
        }
        std::cout << '\n';
#endif
    }
    
    int sumRegion(int row1, int col1, int row2, int col2)
    {
        //if (row1 > row2) std::swap(row1, row2);
        //if (col1 > col2) std::swap(col1, col2);
        ++row2;
        ++col2;
        return integral[col2 + row2 * ncols]
             + integral[col1 + row1 * ncols]
             - integral[col2 + row1 * ncols]
             - integral[col1 + row2 * ncols];
    }
protected:
    std::vector<int> integral;
    int ncols = 0;
};

void test(vector<vector<int> > matrix, const std::tuple<int, int, int, int> &p, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        NumMatrix nm(matrix);
        result = nm.sumRegion(std::get<0>(p), std::get<1>(p), std::get<2>(p), std::get<3>(p));
    }
    if (result == solution) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{3, 0, 1, 4, 2}, {5, 6, 3, 2, 1}, {1, 2, 0, 1, 5}, {4, 1, 0, 1, 7}, {1, 0, 3, 0, 5}}, {2, 1, 4, 3}, 8, trials);
    test({{3, 0, 1, 4, 2}, {5, 6, 3, 2, 1}, {1, 2, 0, 1, 5}, {4, 1, 0, 1, 7}, {1, 0, 3, 0, 5}}, {1, 1, 2, 2}, 11, trials);
    test({{3, 0, 1, 4, 2}, {5, 6, 3, 2, 1}, {1, 2, 0, 1, 5}, {4, 1, 0, 1, 7}, {1, 0, 3, 0, 5}}, {1, 2, 2, 4}, 12, trials);
    return 0;
}

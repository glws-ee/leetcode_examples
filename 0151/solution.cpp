#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

std::string reverseWords(std::string s)
{
    int number_of_words = 0, minimum_size = 0;
    for (bool space = true; char c : s)
    {
        if (c != ' ')
        {
            if (space) { ++number_of_words; space = false; }
            ++minimum_size;
        }
        else space = true;
    }
    const int n = s.size();
    std::string result(minimum_size + number_of_words - 1, '\0');
    int idx = 0, right_begin = n - 1, right_end;
    while (right_begin > 0)
    {
        while ((right_begin >= 0) && (s[right_begin] == ' ')) --right_begin;
        if (right_begin < 0)
            break;
        right_end = right_begin;
        while ((right_begin > 0) && (s[right_begin] != ' ')) --right_begin;
        if (s[right_begin] == ' ') ++right_begin;
        for (int i = right_begin; i <= right_end; ++i)
            result[idx++] = s[i];
        result[idx++] = ' ';
        --right_begin;
    }
    return result;
}

void test(std::string s, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = reverseWords(s);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected '" << solution << "' and obtained '" << result << "'.\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("the sky is blue", "blue is sky the", trials);
    test("  hello world  ", "world hello", trials);
    test("a good   example", "example good a", trials);
    test("  Bob    Loves  Alice   ", "Alice Loves Bob", trials);
    test("Alice does not even like bob", "bob like even not does Alice", trials);
    return 0;
}

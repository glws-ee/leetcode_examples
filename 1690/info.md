Alice and Bob take turns playing a game, with **Alice starting first**.

There are `n` stones arranged in a row. On each player's turn, they can **remove** either the leftmost stone or the rightmost stone from the row and receive points equal to the **sum** of the remaining stones' values in the row. The winner is the one with the higher score when there are no stones left to remove.

Bob found that he will always lose this game (poor Bob, he always loses), so he decided to **minimize the score's difference**. Alice's goal is to **maximize the difference** in the score.

Given an array of integers `stones` where `stones[i]` represents the value of the `i-th` stone from the left, return *the* ***difference*** *in Alice and Bob's score if they both play* ***optimally***.

**Example 1:**<br>
*Input:* `stones = [5, 3, 1, 4, 2]`<br>
*Output:* `6`<br>
*Explanation:* <br>
- Alice removes `2` and gets `5 + 3 + 1 + 4 = 13` points. `Alice = 13, Bob = 0, stones = [5, 3, 1, 4]`.<br>
- Bob removes `5` and gets `3 + 1 + 4 = 8` points. `Alice = 13, Bob = 8, stones = [3, 1, 4]`.<br>
- Alice removes `3` and gets `1 + 4 = 5` points. `Alice = 18, Bob = 8, stones = [1, 4]`.<br>
- Bob removes `1` and gets `4` points. `Alice = 18, Bob = 12, stones = [4]`.<br>
- Alice removes `4` and gets `0` points. `Alice = 18, Bob = 12, stones = []`.<br>
The score difference is `18 - 12 = 6`.<br>

**Example 2:**<br>
*Input:* `stones = [7, 90, 5, 1, 100, 10, 10, 2]`<br>
*Output:* `122`<br>
 
**Constraints:**<br>
 - `n == stones.length`<br>
 - `2 <= n <= 1000`<br>
 - `1 <= stones[i] <= 1000`<br>

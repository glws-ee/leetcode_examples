#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <functional>

using namespace std;

constexpr int null = -1000;

bool canPartitionKSubsets(std::vector<int> nums, int k)
{
    const int n = nums.size();
    int sum = 0;
    for (int n : nums)
        sum += n;
    if ((n < k) || (sum % k)) return false;
    std::vector<bool> visited(nums.size(), false);
    function<bool(int, int, int, int)> backtrack = [&](int target, int curr_sum, int i, int k) -> bool
    {
        if (k == 0)  return true;
        if (curr_sum == target) return backtrack(target, 0, 0, k - 1);
        for (int j = i; j < n; ++j)
        {
            if (visited[j] || (curr_sum + nums[j] > target)) continue;
            visited[j] = true;
            if (backtrack(target, curr_sum + nums[j], j+1, k)) return true;
            visited[j] = false;
        }
        return false;
    };
    return backtrack(sum / k, 0, 0, k);
}

void test(std::vector<int> nums, int k, bool solution, unsigned int trials = 1)
{
    bool result;
    for (unsigned int i = 0; i < trials; ++i)
        result = canPartitionKSubsets(nums, k);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({4, 3, 2, 3, 5, 2, 1}, 4, true, trials);
    test({1, 2, 3, 4}, 3, false, trials);
    return 0;
}

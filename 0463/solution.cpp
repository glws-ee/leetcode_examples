#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

int islandPerimeter(std::vector<std::vector<int> > grid)
{
    const int row = grid.size();
    const int col = grid[0].size();
    int perimeter = 0;
    bool land[col + 1];
    for (int c = 0; c <= col; ++c) land[c] = false;
    for (int r = 0; r < row; ++r)
    {
        for (int c = 0; c < col; ++c)
        {
            bool current = grid[r][c] == 1;
            if (current != land[c]) ++perimeter;
            if (land[c + 1] != current) ++perimeter;
            land[c + 1] = current;
        }
        if (land[col]) ++perimeter;
    }
    for (int c = 1; c <= col; ++c)
        if (land[c]) ++perimeter;
    return perimeter;
}

void test(std::vector<std::vector<int> > grid, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = islandPerimeter(grid);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{0, 1, 0, 0},
          {1, 1, 1, 0},
          {0, 1, 0, 0},
          {1, 1, 0, 0}}, 16, trials);
    test({{0, 0, 1, 0},
          {0, 1, 1, 1},
          {0, 0, 1, 0},
          {0, 1, 1, 0}}, 16, trials);
    test({{1}}, 4, trials);
    test({{1, 0}}, 4, trials);
    return 0;
}


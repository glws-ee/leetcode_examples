#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <algorithm>

using namespace std;

bool solve(int match_index, long * sides, const std::vector<int> &matchsticks)
{
    if (match_index >= matchsticks.size())
        return (sides[0] == 0)
            && (sides[1] == 0)
            && (sides[2] == 0)
            && (sides[3] == 0);
    for (int side_index = 0; side_index < 4; ++side_index)
    {
        if (sides[side_index] - matchsticks[match_index] >= 0)
        {
            sides[side_index] -= matchsticks[match_index];
            if (solve(match_index + 1, sides, matchsticks)) return true;
            sides[side_index] += matchsticks[match_index];
        }
    }
    return false;
}

bool makesquare(std::vector<int> matchsticks)
{
    long sum = 0, max = 0;
    if (matchsticks.size() < 4) return false;
    for (int m : matchsticks)
    {
        sum += m;
        if (m > max) max = m;
    }
    long target = sum / 4;
    if ((max > target) || (sum % 4 > 0)) return false;
    long sides[4] = {target, target, target, target};
    std::sort(matchsticks.rbegin(), matchsticks.rend());
    return solve(0, sides, matchsticks);
}

void test(std::vector<int> matchsticks, bool solution, unsigned int trials = 1)
{
    bool result;
    for (unsigned int i = 0; i < trials; ++i)
        result = makesquare(matchsticks);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10000000;
    constexpr unsigned int trials = 100;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 1, 2, 2, 2}, true, trials);
    test({3, 3, 3, 3, 4}, false, trials);
    test({8,16,24,32,40,48,56,64,72,80,88,96,104,112,60}, false, trials);
    return 0;
}

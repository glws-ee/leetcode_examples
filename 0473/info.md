You are given an integer array `matchsticks` where `matchsticks[i]` is the length of the `i_th` matchstick. You want to use **all the matchsticks** to make one square. You **should not break** any stick, but you can link them up, and each matchstick must be used **exactly one time**.

Return `true` if you can make this square and `false` otherwise.

**Example 1:**<br>
*Input:* `matchsticks = [1, 1, 2, 2, 2]`<br>
*Output:* `true`<br>
*Explanation:* You can form a square with length 2, one side of the square came two sticks with length 1.<br>

**Example 2:**<br>
*Input:* `matchsticks = [3, 3, 3, 3, 4]`<br>
*Output:* `false`<br>
*Explanation:* You cannot find a way to form a square with all the matchsticks.<br>

**Constraints:**<br>
- `1 <= matchsticks.length <= 15`
- `0 <= matchsticks[i] <= 10^9`


#include <vector>
#include <cstring>
#include <iostream>

using namespace std;

#if 1
    int LCS(const string &word1, const string &word2)
    {
        ////const unsigned int n = word1.size();
        ////const unsigned int m = word2.size();
        ////if (m > n) return LCS(word2, word1); // Use less memory.
        ////int dp[2][m + 1];
        ////for (unsigned int j = 0; j <= m; ++j)
        ////    dp[0][j] = 0;
        ////dp[1][0] = 0;
        ////int current = 1, previous = 0;
        ////for (unsigned int i = 1; i <= n; ++i)
        ////{
        ////    for (unsigned int j = 1; j <= m; ++j)
        ////    {
        ////        if (word1[i - 1] == word2[j - 1])
        ////             dp[current][j] = 1 + dp[previous][j - 1];
        ////        else dp[current][j] = max(dp[previous][j], dp[current][j - 1]);
        ////    }
        ////    previous = current;
        ////    current = !current;
        ////}
        ////return dp[previous][m];
        const unsigned int n = word1.size();
        const unsigned int m = word2.size();
        if (m > n) return LCS(word2, word1); // Use less memory.
        int dp[m + 1];
        std::memset(dp, 0, sizeof(dp));
        for (unsigned int i = 1; i <= n; ++i)
        {
            int previous = 0;
            for (unsigned int j = 1; j <= m; ++j)
            {
                int backup = dp[j];
                if (word1[i - 1] == word2[j - 1])
                     dp[j] = previous + 1;
                else dp[j] = max(dp[j], dp[j - 1]);
                previous = backup;
            }
        }
        return dp[m];
    }
    int minDistance(string word1, string word2)
    {
        return word1.size() + word2.size() - 2 * LCS(word1, word2);
    }
#elif 0
    int minDistance(string word1, string word2)
    {
        const unsigned int m = word1.size();
        const unsigned int n = word2.size();
        char word1l[m], word2l[n];
        // Check vocabulary size (only English lowercase).
        bool available_characters1[128] = { false };
        bool available_characters2[128] = { false };
        for (unsigned int i = 0; i < m; ++i)
            available_characters1[word1[i]] = true;
        for (unsigned int i = 0; i < n; ++i)
            available_characters2[word2[i]] = true;
        for (unsigned int i = 'a'; i <= 'z'; ++i)
            if (!(available_characters1[i] && available_characters2[i]))
                available_characters1[i] = false;
        // Reduce the words to the available items only.
        unsigned int rm = 0, rn = 0;
        for (unsigned int i = 0; i < m; ++i)
            if (available_characters1[word1[i]])
                word1l[rm++] = word1[i];
        for (unsigned int i = 0; i < n; ++i)
            if (available_characters1[word2[i]])
                word2l[rn++] = word2[i];
        // Compute the longest common sub-sequence.
        int dp[rm + 1][rn + 1];
        for (unsigned int i = 0; i <= rm; ++i)
        {
            for (unsigned int j = 0; j <= rn; ++j)
            {
                if ((i == 0) || (j == 0))
                    dp[i][j] = 0;
                else if (word1l[i - 1] == word2l[j - 1])
                    dp[i][j] = 1 + dp[i - 1][j - 1];
                else dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
            }
        }
        return m + n - 2 * dp[rm][rn];
    }
#else
    // Hunt-Szymanski algorithm
    int minDistance(string word1, string word2)
    {
        const unsigned int m = word1.size();
        const unsigned int n = word2.size();
        // Make sure that the longest string is the first string.
        if (n > m) return minDistance(word2, word1);
        // Check vocabulary size (only English lowercase).
        bool available_characters1[26] = { false };
        bool available_characters2[26] = { false };
        vector<int> character_index(26);
        int vocabulary_size = 0;
        for (unsigned int i = 0; i < m; ++i)
            available_characters1[word1[i] - 'a'] = true;
        for (unsigned int i = 0; i < n; ++i)
            available_characters2[word2[i] - 'a'] = true;
        for (unsigned int i = 0; i < 26; ++i)
        {
            if (available_characters1[i] && available_characters2[i])
            {
                character_index[i] = vocabulary_size;
                ++vocabulary_size;
            }
            else character_index[i] = -1;
        }
        
        // Build the match list.
        vector<vector<short> > matchlist(vocabulary_size);
        vector<int> L(n + 1, 0);
        for (unsigned int i = 0; i < vocabulary_size; ++i)
            matchlist[i].resize(n + 2);
        for (unsigned int i = 0; i < m; ++i)
        {
            const int idx = character_index[word1[i] - 'a'];
            if ((idx >= 0) && (matchlist[idx][0] == 0)) // First instance of the character on the longest string.
            {
                matchlist[idx][0] = 1;
                unsigned int k = 1;
                for (unsigned int j = n; j > 0; --j)
                    if (word1[i] == word2[j - 1])
                        matchlist[idx][k++] = j;
                matchlist[idx][k] = -1; // Last element.
            }
        }
        
        // Find the longest common sub-sequence.
        unsigned int LCS = 0;
        for (unsigned int i = 0; i < m; ++i)
        {
            const int idx = character_index[word1[i] - 'a'];
            if (idx >= 0)
            {
                for (unsigned int j = 1; matchlist[idx][j] != -1; ++j)
                {
                    const int value = matchlist[idx][j];
                    if (value > L[LCS]) L[++LCS] = value;
                    else
                    {
                        int high = LCS;
                        int low = 0;
                        int k = 0;
                        int mid;
                        while (true)
                        {
                            mid = low + ((high - low) / 2);
                            if (L[mid] == value)
                            {
                                k = 1;
                                break;
                            }
                            if (high - low <= 1)
                            {
                                mid = high;
                                break;
                            }
                            if (L[mid] > value) high = mid;
                            else if (L[mid] < value) low = mid;
                        }
                        if (k == 0) L[mid] = value;
                    }
                }
            }
        }
        return n + m - 2 * LCS;
    }
#endif

void test(const string &word1, const string &word2, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minDistance(word1, word2);
    if (result == solution) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
    //constexpr unsigned int trials = 1;
    test("sea", "eat", 2, trials);
    test("leetcode", "etco", 4, trials);
    test("etco", "leetcode", 4, trials);
    test("banana", "atana", 3, trials);
    test("atana", "banana", 3, trials);
    return 0;
}


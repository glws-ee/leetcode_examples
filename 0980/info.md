=Unique Paths III=

You are given an `m x n` integer array `grid` where `grid[i][j]` could be:

- `1` representing the starting square. There is exactly one starting square.
- `2` representing the ending square. There is exactly one ending square.
- `0` representing empty squares we can walk over.
- `-1` representing obstacles that we cannot walk over.

Return *the number of 4-directional walks from the starting square to the ending square, that walk over every non-obstacle square exactly once*.

**Example 1:**<br>
|:runner:| |                |                 |
|--------|-|----------------|-----------------|
|        | |                |                 |
|        | |:checkered_flag:|:european_castle:|

*Input:* `grid = [[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 2, -1]]`<br>
*Output:* `2`<br>
*Explanation:* We have the following two paths: 
1. (0, 0), (0, 1), (0, 2), (0, 3), (1, 3), (1, 2), (1, 1), (1, 0), (2, 0), (2, 1), (2, 2)
2. (0, 0), (1, 0), (2, 0), (2, 1), (1, 1), (0, 1), (0, 2), (0, 3), (1, 3), (1, 2), (2, 2)

**Example 2:**<br>
|:runner:| | |                |
|--------|-|-|----------------|
|        | | |                |
|        | | |:checkered_flag:|

*Input:* `grid = [[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 2]]`<br>
*Output:* `4`<br>
*Explanation:* We have the following four paths: 
1. (0, 0), (0, 1), (0, 2), (0, 3), (1, 3), (1, 2), (1, 1), (1, 0), (2, 0), (2, 1), (2, 2), (2, 3)
2. (0, 0), (0, 1), (1, 1), (1, 0), (2, 0), (2, 1), (2, 2), (1, 2), (0, 2), (0, 3), (1, 3), (2, 3)
3. (0, 0), (1, 0), (2, 0), (2, 1), (2, 2), (1, 2), (1, 1), (0, 1), (0, 2), (0, 3), (1, 3), (2, 3)
4. (0, 0), (1, 0), (2, 0), (2, 1), (1, 1), (0, 1), (0, 2), (0, 3), (1, 3), (1, 2), (2, 2), (2, 3)

**Example 3:**<br>
|                |:runner:|
|----------------|--------|
|                |        |
|:checkered_flag:|        |

*Input:* `grid = [[0, 1], [2, 0]]`<br>
*Output:* `0`<br>
*Explanation:* There is no path that walks over every empty square exactly once. Note that the starting and ending square can be anywhere in the grid.<br>
 
**Constraints:**<br>
- `m == grid.length`
- `n == grid[i].length`
- `1 <= m, n <= 20`
- `1 <= m * n <= 20`
- `-1 <= grid[i][j] <= 2`
- There is exactly one starting cell and one ending cell.




=Rotting Oranges=

You are given a `m x n` grid where each cell can have one of three values:

- `0` representing an empty cell,
- `1` representing a fresh orange, or
- `2` representing a rotten orange.

Every minute, any fresh orange that is **4-directionally adjacent** to a rotten orange becomes rotten.

Return *the minimum number of minutes that must elapse until no cell has a fresh orange*. If *this is impossible, return* `-1`.

**Example 1:**<br>
*Input:* `grid = [[2, 1, 1], [1, 1, 0], [0, 1, 1]]`<br>
*Output:* `4`<br>

**Example 2:**<br>
*Input:* `grid = [[2, 1, 1], [0, 1, 1], [1, 0, 1]]`<br>
*Output:* `-1`<br>
*Explanation:* The orange in the bottom left corner (row 2, column 0) is never rotten, because rotting only happens 4-directionally.<br>

**Example 3:**<br>
*Input:* `grid = [[0, 2]]`<br>
*Output:* `0`<br>
*Explanation:* Since there are already no fresh oranges at minute 0, the answer is just 0.<br>
 
**Constraints:**<br>
- `m == grid.length`
- `n == grid[i].length`
- `1 <= m, n <= 10`
- `grid[i][j]` is `0`, `1`, or `2`.




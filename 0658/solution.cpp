#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    for (int i = 0; i < n; ++i)
        if (left[i] != right[i]) return false;
    return true;
}

std::ostream& operator<<(std::ostream &out, const std::vector<int> &vec)
{
    out << '{';
    bool next = false;
    for (int v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

std::vector<int> findClosestElements(std::vector<int> arr, int k, int x)
{
    const int n = arr.size();
    if (k >= n) return arr; // Special case.
    std::vector<int> result;
    result.reserve(k);
    
    auto cmp = [&x](int a, int b)
    {
        int da = std::abs(a - x);
        int db = std::abs(b - x);
        return (da < db)
            || ((da == db) && (a < b));
    };
    
    auto it = std::lower_bound(arr.begin(), arr.end(), x);
    if (it == arr.begin()) // Special case: result is array head.
    {
        for (int i = 0; i < k; ++i, ++it)
            result.push_back(*it);
    }
    else if (it == arr.end()) // Special case: result is array tail.
    {
        it -= k;
        for (int i = 0; i < k; ++i, ++it)
            result.push_back(*it);
    }
    else
    {
        auto begin = it, end = it;
        const auto last = arr.end() - 1;
        const auto first = arr.begin();
        while (std::distance(begin, end) < k)
        {
            if ((end == last) || ((begin != first) && cmp(*(begin - 1), *(end + 1))))
                --begin;
            else ++end;
        }
        if (!cmp(*begin, *end)) ++begin;
        for (int i = 0; i < k; ++i, ++begin)
            result.push_back(*begin);
    }
    
    return result;
}

void test(std::vector<int> arr, int k, int x, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findClosestElements(arr, k, x);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, 4, 3, {1, 2, 3, 4}, trials);
    test({1, 2, 3, 4, 5}, 4, -1, {1, 2, 3, 4}, trials);
    test({1, 2, 3, 4, 5}, 4, 6, {2, 3, 4, 5}, trials);
    test({1, 2, 4, 5, 7}, 4, 3, {1, 2, 4, 5}, trials);
    test({1, 2, 4, 7, 8}, 4, 5, {2, 4, 7, 8}, trials);
    test({1, 2, 4, 7, 8}, 4, 7, {2, 4, 7, 8}, trials);
    test({0, 0, 1, 2, 3, 3, 4, 7, 7 ,8}, 3, 5, {3, 3, 4}, trials);
    return 0;
}

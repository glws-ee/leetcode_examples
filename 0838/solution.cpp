#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

std::string pushDominoes(std::string dominoes)
{
#if 1
    const int n = dominoes.size();
    for (int begin = 0, end = 0; begin < n; )
    {
        while ((dominoes[begin] != '.') && (begin < n)) ++begin;
        end = begin;
        while ((dominoes[end] == '.') && (end < n)) ++end;
        if (begin == 0)
        {
            if ((end < n) && (dominoes[end] == 'L'))
                while (begin < end)
                    dominoes[begin++] = 'L';
        }
        else if (end == n)
        {
            if (dominoes[begin - 1] == 'R')
                while (begin < end)
                    dominoes[begin++] = 'R';
        }
        else
        {
            if ((dominoes[begin - 1] == 'R') && (dominoes[end] == 'L'))
            {
                while (begin + 1 < end)
                {
                    dominoes[begin++] = 'R';
                    dominoes[--end] = 'L';
                }
            }
            else if (dominoes[begin - 1] == 'R')
                while (begin < end)
                    dominoes[begin++] = 'R';
            else if (dominoes[end] == 'L')
                while (begin < end)
                    dominoes[begin++] = 'L';
        }
        begin = end;
    }
    return dominoes;
#elif 1
    const int n = dominoes.size();
    char buffer[2][n + 2];
    buffer[1][0] = buffer[1][n + 1] = buffer[0][0] = buffer[0][n + 1] = '.';
    for (int i = 0; i < n; ++i)
        buffer[1][i + 1] = buffer[0][i + 1] = dominoes[i];
    bool current = false;
    for (bool change = true; change; current = !current)
    {
        change = false;
        for (int i = 1; i <= n; ++i)
        {
            if (buffer[current][i] == '.')
            {
                bool right = buffer[current][i - 1] == 'R';
                bool left = buffer[current][i + 1] == 'L';
                if (left && right) buffer[!current][i] = 'x';
                else if (left) { buffer[!current][i] = 'L'; change = true; }
                else if (right) { buffer[!current][i] = 'R'; change = true; }
            }
        }
        for (int i = 1; i <= n; ++i)
            buffer[current][i] = buffer[!current][i];
    }
    for (int i = 0; i < n; ++i)
    {
        if (buffer[current][i + 1] == 'R') dominoes[i] = 'R';
        else if (buffer[current][i + 1] == 'L') dominoes[i] = 'L';
        else dominoes[i] = '.';
    }
    return dominoes;
#else
    std::string buffer[2] = {"." + dominoes + ".", "." + dominoes + "."};
    const int n = dominoes.size();
    bool current = false;
    for (bool change = true; change; current = !current)
    {
        change = false;
        for (int i = 1; i <= n; ++i)
        {
            if (buffer[current][i] == '.')
            {
                bool right = buffer[current][i - 1] == 'R';
                bool left = buffer[current][i + 1] == 'L';
                if (left && right) buffer[!current][i] = 'x';
                else if (left) { buffer[!current][i] = 'L'; change = true; }
                else if (right) { buffer[!current][i] = 'R'; change = true; }
            }
        }
        buffer[current] = buffer[!current];
    }
    for (int i = 0; i < n; ++i)
    {
        if (buffer[current][i + 1] == 'R') dominoes[i] = 'R';
        else if (buffer[current][i + 1] == 'L') dominoes[i] = 'L';
        else dominoes[i] = '.';
    }
    return dominoes;
#endif
}

void test(std::string dominoes, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = pushDominoes(dominoes);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected |" << solution << "| and obtained |" << result << "|.\n";
}

int main(int, char **)
{
#if 1
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("RR.L", "RR.L", trials);
    test(".L.R...LR..L..", "LL.RR.LLRRLL..", trials);
    test(".L.R....LR..L..", "LL.RRRLLLRRLL..", trials);
    test(".L.R...LR..R..", "LL.RR.LLRRRRRR", trials);
    return 0;
}

=Max Consecutive Ones=

Given a binary array `nums`, return *the maximum number of consecutive* 1's in the array.

**Example 1:**<br>
*Input:* `nums = [1, 1, 0, 1, 1, 1]`<br>
*Output:* `3`<br>
*Explanation:* The first two digits or the last three digits are consecutive `1`s. The maximum number of consecutive `1`s is `3`.<br>

**Example 2:**<br>
*Input:* `nums = [1, 0, 1, 1, 0, 1]`<br>
*Output:* `2`<br>
 
**Constraints:**<br>
- `1 <= nums.length <= 10^5`
- `nums[i]` is either `0` or `1`.




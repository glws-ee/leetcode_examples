=Two Sum IV - Input is a BST=

Given the `root` of a Binary Search Tree and a target number `k`, return `true` *if there exist two elements in the BST such that their sum is equal to the given target*.

**Example 1:**<br>
```mermaid 
A((5))-->B((3))
A-->C((6))
B-->D((2))
B-->E((4))
C-->F((7))
```
*Input:* `root = {5, 3, 6, 2, 4, null, 7}, k = 9`<br>
*Output:* `true`<br>

**Example 2**<br>
```mermaid 
A((5))-->B((3))
A-->C((6))
B-->D((2))
B-->E((4))
C-->F((7))
```
*Input:* `root = {5, 3, 6, 2, 4, null, 7}, k = 28`<br>
*Output:* `false`<br>

**Example 3:**<br>
*Input:* `root = {2, 1, 3}, k = 4`<br>
*Output:* `true`<br>

**Example 4:**<br>
*Input:* `root = {2, 1, 3}, k = 1`<br>
*Output:* `false`<br>

**Example 5:**<br>
*Input:* `root = {2, 1, 3}, k = 3`<br>
*Output:* `true`<br>

**Constrains:**<br>
- The number of nodes in the tree is in the range `[1, 10^4]`.
- `-10^4 <= Node.val <= 10^4`
- `root` is guaranteed to be a valid binary search tree.
- `-10^5 <= k <= 10^5`




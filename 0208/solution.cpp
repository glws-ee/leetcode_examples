#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <set>

using namespace std;

constexpr int null = -1000;

class Trie
{
    std::set<std::string> m_data;
public:
    Trie()
    {
    }
    void insert(string word)
    {
        m_data.insert(word);
    }
    bool search(string word)
    {
        return m_data.find(word) != m_data.end();
    }
    bool startsWith(string prefix)
    {
        //std::cout << "PREFIX: " << prefix << " ";
        if (auto it = m_data.lower_bound(prefix); it != m_data.end())
        {
            const int n = prefix.size();
            if (it->size() < n) return false;
            if (it->size() == n) return *it == prefix;
            //std::cout << "----> " << *it << '\n';
            return it->substr(0, n) == prefix;
        }
        //else std::cout << "NONE\n";
        return m_data.lower_bound(prefix) != m_data.end();
    }
};

enum class OP { trie, insert, search, startWith };

bool operator==(const std::vector<bool> &left, const std::vector<bool> &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    for (int i = 0; i < n; ++i)
        if (left[i] != right[i])
            return false;
    return true;
}

std::ostream& operator<<(std::ostream &out, const std::vector<bool> &vec)
{
    out << '{';
    bool next = false;
    for (bool b : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        if (b) out << "T";
        else out << "F";
    }
    out << '}';
    return out;
}

void test(std::vector<OP> commands,
          std::vector<std::string> word,
          std::vector<bool> solution,
          unsigned int trials = 1)
{
    const int n = commands.size();
    std::vector<bool> result(n, false);
    for (unsigned int trial = 0; trial < trials; ++trial)
    {
        Trie * t = nullptr;
        for (int j = 0; j < n; ++j)
        {
            switch (commands[j])
            {
            case OP::trie:
                t = new Trie();
                break;
            case OP::insert:
                t->insert(word[j]);
                break;
            case OP::search:
                result[j] = t->search(word[j]);
                break;
            case OP::startWith:
                result[j] = t->startsWith(word[j]);
                break;
            };
        }
        delete t;
    }
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({OP::trie, OP::insert, OP::search, OP::search, OP::startWith, OP::insert, OP::search},
         {""      , "apple"   , "apple"   , "app"     , "app"        , "app"     , "app"},
         {false   , false     , true      , false     , true         , false     , true}, trials);
    test({OP::trie, OP::insert, OP::insert, OP::insert, OP::startWith, OP::startWith, OP::startWith},
         {""      , "apple"   , "app"     , "ball"    , "appl"       , "apb"        , "bal"        },
         {false   , false     , false     , false     , true         , false        , true         }, trials);
    return 0;
}

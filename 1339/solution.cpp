#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <tuple>

using namespace std;

constexpr int null = -1000;

struct TreeNode
{
    int val = 0;
    TreeNode * left = nullptr;
    TreeNode * right = nullptr;
    TreeNode(void) = default;
    TreeNode(int v) : val(v), left(nullptr), right(nullptr) {}
    TreeNode(int v, const TreeNode * l, const TreeNode * r) :
        val(v),
        left((l != nullptr)?new TreeNode(*l):nullptr),
        right((r != nullptr)?new TreeNode(*r):nullptr) {}
    TreeNode(const TreeNode &other) :
        val(other.val),
        left((other.left != nullptr)?new TreeNode(*other.left):nullptr),
        right((other.right != nullptr)?new TreeNode(*other.right):nullptr) {}
    TreeNode(TreeNode &&other) :
        val(other.val),
        left(std::exchange(other.left, nullptr)),
        right(std::exchange(other.right, nullptr)) {}
    ~TreeNode(void) { delete left; delete right; }
    TreeNode& operator=(const TreeNode &other)
    {
        if (this != &other)
        {
            delete left;
            delete right;
            val = other.val;
            left = (other.left != nullptr)?new TreeNode(*other.left):nullptr;
            right = (other.right != nullptr)?new TreeNode(*other.right):nullptr;
        }
        return *this;
    }
    TreeNode& operator=(TreeNode &&other)
    {
        if (this != &other)
        {
            val = other.val;
            left = std::exchange(other.left, nullptr);
            right = std::exchange(other.right, nullptr);
        }
        return *this;
    }
};

TreeNode * vec2tree(const std::vector<int> &tree)
{
    std::queue<TreeNode *> active_nodes;
    
    TreeNode * result = nullptr;
    bool left = true;
    for (const auto &v : tree)
    {
        if (v != null)
        {
            if (result == nullptr)
            {
                result = new TreeNode(v);
                active_nodes.push(result);
                active_nodes.push(result);
            }
            else
            {
                TreeNode * current = new TreeNode(v);
                if (left) active_nodes.front()->left = current;
                else active_nodes.front()->right = current;
                active_nodes.pop();
                active_nodes.push(current);
                active_nodes.push(current);
                left = !left;
            }
        }
        else
        {
            active_nodes.pop();
            left = !left;
        }
    }
    return result;
}

std::vector<int> tree2vec(TreeNode * root)
{
    if (root == nullptr) return {};
    std::vector<int> result;
    std::queue<TreeNode *> active_nodes;
    active_nodes.push(root);
    result.push_back(root->val);
    while (!active_nodes.empty())
    {
        TreeNode * current = active_nodes.front();
        active_nodes.pop();
        if (current->left != nullptr)
        {
            active_nodes.push(current->left);
            result.push_back(current->left->val);
        }
        else result.push_back(null);
        if (current->right != nullptr)
        {
            active_nodes.push(current->right);
            result.push_back(current->right->val);
        }
        else result.push_back(null);
    }
    while (result.back() == null) result.pop_back();
    return result;
}

std::ostream& operator<<(std::ostream &out, const std::vector<int> &vec)
{
    out << '{';
    bool next = false;
    for (const auto &v : vec)
    {
        if (next) out << ", ";
        out << v;
        next = true;
    }
    out << '}';
    return out;
}

// ################################################################################################
// ################################################################################################

struct FlatNode
{
    int left;
    int right;
    int val;
};

std::tuple<long, long> maxProduct(TreeNode * root, long sum)
{
    if (root == nullptr) return {0, 0};
    else
    {
        const long current = static_cast<long>(root->val);
        const long val = current + sum;
        auto [left, product_left] = maxProduct(root->left, val);
        auto [right, product_right] = maxProduct(root->right, val);
        long product = std::max(product_left, product_right);
        if ((left + val) * right > left * (right + val))
            product = std::max(product, (left + val) * right);
        else
            product = std::max(product, left * (right + val));
        std::cout << current << ' ' << sum << ' ' << val << ' ' << left << ' ' << right << ' ' << product << '\n';
        return {left + right + current, product};
    }
}

int localSum(TreeNode * root, FlatNode * flat_tree, int &index)
{
    if (root != nullptr)
    {
        int current = ++index;
        int &sum = flat_tree[current].val = root->val;
        if (root->left != nullptr)
        {
            flat_tree[current].left = index + 1;
            sum += localSum(root->left, flat_tree, index);
        }
        else flat_tree[current].left = -1;
        if (root->right != nullptr)
        {
            flat_tree[current].right = index + 1;
            sum += localSum(root->right, flat_tree, index);
        }
        else flat_tree[current].right = -1;
        return sum;
    }
    else return 0;
}

long maxProduct(TreeNode * root, FlatNode * ftree, int idx, long sum)
{
    if (root == nullptr) return 0;
    else
    {
        long sum_left  = (ftree[idx].left  != -1)?(ftree[ftree[idx].left ].val):0;
        long sum_right = (ftree[idx].right != -1)?(ftree[ftree[idx].right].val):0;
        sum += root->val;
        long product = std::max((sum + sum_left) * sum_right, sum_left * (sum + sum_right));
        product = std::max(product, maxProduct(root->left, ftree, ftree[idx].left, sum + sum_right));
        product = std::max(product, maxProduct(root->right, ftree, ftree[idx].right, sum + sum_left));
        return product;
    }
}

int maxProduct(TreeNode * root)
{
    FlatNode flat_tree[50'001];
    int index = -1;
    localSum(root, flat_tree, index);
    return static_cast<int>(maxProduct(root, flat_tree, 0, 0) % 1'000'000'007L);
}

// ################################################################################################
// ################################################################################################

void test(const std::vector<int> &tree, int solution, unsigned int trials = 1)
{
    std::cout << "INPUT: " << tree << '\n';
    int result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = vec2tree(tree);
        result = maxProduct(root);
        delete root;
    }
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5, 6}, 110, trials);
    test({1, null, 2, 3, 4, null, null, 5, 6}, 90, trials);
    test({2, 3, 9, 10, 7, 8, 6, 5, 4, 11, 1}, 1025, trials);
    test({1, 1}, 1, trials);
    test({5, 6, 6, null, null, 8, 6, 10, null, 5}, 504, trials);
    return 0;
}

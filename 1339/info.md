=Maximum Product of Splitted Binary Tree=

Given the `root` of a binary tree, split the binary tree into two subtrees by removing one edge such that the product of the sums of the subtrees is maximized.

Return *the maximum product of the sums of the two subtrees*. Since the answer may be too large, return it **modulo** 10^9 + 7.

**Note** that you need to maximize the answer before taking the mod and not after taking it.

**Example 1:**<br>
*Input:* `root = [1, 2, 3, 4, 5, 6]`<br>
```mermaid 
A((1))-->B((2))
A-->C((3))
B-->D((4))
B-->E((5))
C-->F((6))
linkStyle 0 stroke-width:2pt,fill:none,stroke:red;
```
*results in*

```mermaid 
A((1))
B((2))
A-->C((3))
B-->D((4))
B-->E((5))
C-->F((6))
```

*Output:* `110`<br>
*Explanation:* Remove the red edge and get 2 binary trees with sum 11 and 10. Their product is 110 (11*10)<br>

**Example 2:**<br>
*Input:* `root = [1, null, 2, 3, 4, null, null, 5, 6]`<br>
```mermaid 
A((1))-->B((2))
B-->C((3))
B-->D((4))
D-->E((5))
D-->F((6))
linkStyle 2 stroke-width:2pt,fill:none,stroke:red;
```
*results in*
```mermaid 
A((1))-->B((2))
B-->C((3))
D((4))
D-->E((5))
D-->F((6))
```
*Output:* `90`<br>
*Explanation:* Remove the red edge and get 2 binary trees with sum 15 and 6.Their product is 90 (15*6)<br>

**Example 3:**<br>
*Input:* `root = [2, 3, 9, 10, 7, 8, 6, 5, 4, 11, 1]`<br>
*Output:* `1025`<br>

**Example 4:**<br>
*Input:* `root = [1, 1]`<br>
*Output:* `1`<br>

**Constraints:**<br>
- The number of nodes in the tree is in the range `[2, 5 * 10^4]`.
- `1 <= Node.val <= 10^4`

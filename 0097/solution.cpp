#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <tuple>

using namespace std;

bool isInterleave(std::string s1, std::string s2, std::string s3)
{
#if 1
    const int n1 = s1.size(), n2 = s2.size(), n3 = s3.size();
    if (n1 + n2 != n3) return false;
    bool dp[n1 + 1][n2 + 1];
    int i, j;
    for (i = 0; i < n1; ++i)
        for (j = 0; j < n2; ++j)
            dp[i][j] = false;
    dp[0][0] = true;
    for (j = 1; j <= n2; ++j)
        dp[0][j] = dp[0][j - 1] && (s2[j - 1] == s3[j - 1]);
    for (i = 1; i <= n1; ++i)
    {
        dp[i][0] = dp[i - 1][0] && (s1[i - 1] == s3[i - 1]);
        for (j = 1; j <= n2; ++j)
            dp[i][j] = dp[i][j - 1] && (s2[j - 1] == s3[i + j - 1])
                    || dp[i - 1][j] && (s1[i - 1] == s3[i + j - 1]);
    }
    return dp[n1][n2];
#else
    if (s3.size() != s1.size() + s2.size()) return false;
    int idx1 = 0, idx2 = 0, idx3 = 0;
    std::stack<std::tuple<int, int, int> > fallback;
    while (idx3 < s3.size())
    {
        bool forward1 = (idx1 < s1.size()) * (s3[idx3] == s1[idx1]);
        bool forward2 = (idx2 < s2.size()) * (s3[idx3] == s2[idx2]);
        if (forward1 || forward2)
        {
            if (forward1 && forward2)
            {
                fallback.push(std::make_tuple(idx1, idx2 + 1, idx3 + 1));
                ++idx1;
            }
            else if (forward1) ++idx1;
            else if (forward2) ++idx2;
            else return false;
            ++idx3;
        }
        else
        {
            if (fallback.empty()) return false;
            std::tie(idx1, idx2, idx3) = fallback.top();
            fallback.pop();
        }
    }
    return idx3 == s3.size();
#endif
}

void test(std::string s1, std::string s2, std::string s3, bool solution, unsigned int trials = 1)
{
    std::cout << "'" << s1 << "' · '" << s2 << "' => '" << s3 << "' ";
    bool result;
    for (unsigned int i = 0; i < trials; ++i)
        result = isInterleave(s1, s2, s3);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aabcc", "dbbca", "aadbbcbcac", true, trials);
    test("aabcc", "dbbca", "aadbbbaccc", false, trials);
    test("bbbbbabbbbabaababaaaabbababbaaabbabbaaabaaaaababbbababbbbbabbbbababbabaabababbbaabababababbbaaababaa", "babaaaabbababbbabbbbaabaabbaabbbbaabaaabaababaaaabaaabbaaabaaaabaabaabbbbbbbbbbbabaaabbababbabbabaab", "babbbabbbaaabbababbbbababaabbabaabaaabbbbabbbaaabbbaaaaabbbbaabbaaabababbaaaaaabababbababaababbababbbababbbbaaaabaabbabbaaaaabbabbaaaabbbaabaaabaababaababbaaabbbbbabbbbaabbabaabbbbabaaabbababbabbabbab", false, trials);
    test("", "", "", true, trials);
    return 0;
}

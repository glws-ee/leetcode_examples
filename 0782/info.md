=Transform to Chessboard=

You are given an `n x n` binary grid `board`. In each move, you can swap any two rows with each other, or any two columns with each other.

Return *the minimum number of moves to transform the board into a* ***chessboard board***. If the task is impossible, return `-1`.

A **chessboard board** is a board where no `0`'s and no `1`'s are 4-directionally adjacent.

**Example 1:**<br>
*Input:* `board = [[0, 1, 1, 0], [0, 1, 1, 0], [1, 0, 0, 1], [1, 0, 0, 1]]`<br>
*Output:* `2`<br>
*Explanation:* One potential sequence of moves is shown. The first move swaps the first and second column. The second move swaps the second and third row.<br>

**Example 2:**<br>
*Input:* `board = [[0, 1], [1, 0]]`<br>
*Output:* `0`<br>
*Explanation:* Also note that the board with 0 in the top left corner, is also a valid chessboard.<br>

**Example 3:**<br>
*Input:* `board = [[1, 0], [1, 0]]`<br>
*Output:* `-1`<br>
*Explanation:* No matter what sequence of moves you make, you cannot end with a valid chessboard.<br>
 
**Constraints:**<br>
- `n == board.length`
- `n == board[i].length`
- `2 <= n <= 30`
- `board[i][j]` is either `0` or `1`.




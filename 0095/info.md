=Unique Binary Search Trees II=

Given an integer `n`, return *all the structurally unique* ***BST's*** *(binary search trees), which has exactly* `n` *nodes of unique values from* `1` *to* `n`. Return the answer in **any order**.

**Example 1:**<br>
```mermaid 
A((1))-->B((3))
B-->C((2))

D((1))-->E((2))
E-->F((3))

H((2))-->I((1))
H-->J((3))

K((3))-->L((2))
L-->M((1))

N((3))-->O((1))
O-->P((2))
```

*Input:* `n = 3`<br>
*Output:* `[[1, null, 2, null, 3], [1, null, 3, 2], [2, 1, 3], [3, 1, null, null, 2], [3, 2, null, 1]]`<br>

**Example 2:**<br>
*Input:* `n = 1`<br>
*Output:* `[[1]]`<br>
 
**Constraints:**<br>
- `1 <= n <= 8`


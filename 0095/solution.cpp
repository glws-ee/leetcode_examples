#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <set>

using namespace std;

constexpr int null = -1000;

struct TreeNode
{
    int val = 0;
    TreeNode * left = nullptr;
    TreeNode * right = nullptr;
    TreeNode(void) = default;
    TreeNode(int v) : val(v), left(nullptr), right(nullptr) {}
    TreeNode(int v, const TreeNode * l, const TreeNode * r) :
        val(v),
        left((l != nullptr)?new TreeNode(*l):nullptr),
        right((r != nullptr)?new TreeNode(*r):nullptr) {}
    TreeNode(const TreeNode &other) :
        val(other.val),
        left((other.left != nullptr)?new TreeNode(*other.left):nullptr),
        right((other.right != nullptr)?new TreeNode(*other.right):nullptr) {}
    TreeNode(TreeNode &&other) :
        val(other.val),
        left(std::exchange(other.left, nullptr)),
        right(std::exchange(other.right, nullptr)) {}
    ~TreeNode(void) { delete left; delete right; }
    TreeNode& operator=(const TreeNode &other)
    {
        if (this != &other)
        {
            delete left;
            delete right;
            val = other.val;
            left = (other.left != nullptr)?new TreeNode(*other.left):nullptr;
            right = (other.right != nullptr)?new TreeNode(*other.right):nullptr;
        }
        return *this;
    }
    TreeNode& operator=(TreeNode &&other)
    {
        if (this != &other)
        {
            val = other.val;
            left = std::exchange(other.left, nullptr);
            right = std::exchange(other.right, nullptr);
        }
        return *this;
    }
};

TreeNode * vec2tree(const std::vector<int> &tree)
{
    std::queue<TreeNode *> active_nodes;
    
    TreeNode * result = nullptr;
    bool left = true;
    for (const auto &v : tree)
    {
        if (v != null)
        {
            if (result == nullptr)
            {
                result = new TreeNode(v);
                active_nodes.push(result);
                active_nodes.push(result);
            }
            else
            {
                TreeNode * current = new TreeNode(v);
                if (left) active_nodes.front()->left = current;
                else active_nodes.front()->right = current;
                active_nodes.pop();
                active_nodes.push(current);
                active_nodes.push(current);
                left = !left;
            }
        }
        else
        {
            active_nodes.pop();
            left = !left;
        }
    }
    return result;
}

std::vector<int> tree2vec(const TreeNode * root)
{
    if (root == nullptr) return {};
    std::vector<int> result;
    std::queue<const TreeNode *> active_nodes;
    active_nodes.push(root);
    result.push_back(root->val);
    while (!active_nodes.empty())
    {
        const TreeNode * current = active_nodes.front();
        active_nodes.pop();
        if (current->left != nullptr)
        {
            active_nodes.push(current->left);
            result.push_back(current->left->val);
        }
        else result.push_back(null);
        if (current->right != nullptr)
        {
            active_nodes.push(current->right);
            result.push_back(current->right->val);
        }
        else result.push_back(null);
    }
    while (result.back() == null) result.pop_back();
    return result;
}

std::ostream& operator<<(std::ostream &out, const std::vector<int> &vec)
{
    out << '{';
    bool next = false;
    for (const auto &v : vec)
    {
        if (next) out << ", ";
        out << v;
        next = true;
    }
    out << '}';
    return out;
}

// ################################################################################################
// ################################################################################################

std::vector<TreeNode *> generateTrees(int begin, int end)
{
#if 0
    if (begin > end) return { nullptr };
    if (begin == end) return { new TreeNode(begin) };
    std::vector<TreeNode *> result;
    for (int i = begin; i <= end; ++i)
    {
        std::vector<TreeNode *> left = generateTrees(begin, i - 1);
        std::vector<TreeNode *> right = generateTrees(i + 1, end);
        for (auto tl : left)
            for (auto tr : right)
                result.push_back(new TreeNode(i, tl, tr));
    }
    return result;
#else
    if (begin > end) return { nullptr };
    if (begin == end) return { new TreeNode(begin) };
    std::vector<TreeNode *> result;
    for (int i = begin; i <= end; ++i)
    {
        std::vector<TreeNode *> left = generateTrees(begin, i - 1);
        std::vector<TreeNode *> right = generateTrees(i + 1, end);
        for (auto tl : left)
            for (auto tr : right)
                result.push_back(new TreeNode(i, tl, tr));
        for (auto tl : left)
            delete tl;
        for (auto tr : right)
            delete tr;
    }
    return result;
#endif
}

std::vector<TreeNode *> generateTrees(int n)
{
    return generateTrees(1, n);
}

// ################################################################################################
// ################################################################################################

std::ostream& operator<<(std::ostream &out, const std::vector<std::vector<int> > &info)
{
    out << '{';
    bool next_out = false;
    for (const auto &vec : info)
    {
        if (next_out) [[likely]] out << ", ";
        next_out = true;
        out << '{';
        bool next_in = false;
        for (int v : vec)
        {
            if (next_in) [[likely]] out << ", ";
            next_in = true;
            out << v;
        }
        out << '}';
    }
    out << '}';
    return out;
}

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    for (int i = 0; i < n; ++i)
        if (left[i] != right[i])
            return false;
    return true;
}

bool operator<(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() < right.size()) return true;
    if (left.size() > right.size()) return false;
    const int n = left.size();
    for (int i = 0; i < n; ++i)
    {
        if (left[i] < right[i])
            return true;
        if (left[i] > right[i])
            return false;
    }
    return false;
}

bool operator==(const std::vector<std::vector<int> > &left, const std::vector<std::vector<int> > &right)
{
    if (left.size() != right.size()) return false;
    std::set<std::vector<int> > left_unique(left.begin(), left.end());
    for (const auto &vec : right)
    {
        if (auto it = left_unique.find(vec); it != left_unique.end())
            left_unique.erase(it);
        else return false;
    }
    return true;
}

void test(int n, std::vector<std::vector<int> > solution, unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result.clear();
        std::vector<TreeNode *> tree_results = generateTrees(n);
        for (auto tn : tree_results)
        {
            result.push_back(tree2vec(tn));
            delete tn;
        }
    }
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(3, {{1, null, 2, null, 3}, {1, null, 3, 2}, {2, 1, 3}, {3, 1, null, null, 2}, {3, 2, null, 1}}, trials);
    test(1, {{1}}, trials);
    return 0;
}

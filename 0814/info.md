=Binary Tree Pruning=

Given the `root` of a binary tree, return *the same tree where every subtree (of the given tree) not containing a 1 has been removed*.

A subtree of a `node` node is `node` plus every node that is a descendant of node.

**Example 1:**<br>
*The input tree*<br>
```mermaid 
graph TB
A[1] --> B[0]
B --> C[0]
B --> D[1]
```
*becomes*<br>
```mermaid 
graph TB
A[1] --> B[0]
B --> D[1]
```

*Input:* `root = [1, null, 0, 0, 1]`<br>
*Output:* `[1, null, 0, null, 1]`<br>
*Explanation:* Only the red nodes satisfy the property *"every subtree not containing a 1"*. The diagram on the right represents the answer.

**Example 2:**<br>
*The input tree*<br>
```mermaid 
graph TB
A[1] --> B[1]
A --> E[0]
E --> F[0]
E --> G[0]
B --> C[0]
B --> D[1]
```
*becomes*<br>
```mermaid 
graph TB
A[1] --> B[1]
B --> D[1]
```

*Input:* `root = [1, 0, 1, 0, 0, 0, 1]`<br>
*Output:* `[1, null, 1, null, 1]`<br>

**Example 3**<br>
*The input tree*<br>
```mermaid 
graph TB
A[1] --> B[0]
A --> E[1]
E --> F[1]
E --> G[1]
F --> H[0]
B --> C[0]
B --> D[1]
```
*becomes*<br>
```mermaid 
graph TB
A[1] --> B[0]
A --> E[1]
E --> F[1]
E --> G[1]
B --> D[1]
```

*Input:* `root = [1, 1, 0, 1, 1, 0, 1, 0]`<br>
*Output:* `[1, 1, 0, 1, 1, null, 1]`<br>

**Constrains:**<br>
- The number of nodes in the tree is in the range `[1, 200]`.
- `Node.val` is either 0 or 1.



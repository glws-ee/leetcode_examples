#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <functional>
#include <limits>

using namespace std;

constexpr int null = -1000;

struct TreeNode
{
    int val = 0;
    TreeNode * left = nullptr;
    TreeNode * right = nullptr;
    TreeNode(void) = default;
    TreeNode(int v) : val(v), left(nullptr), right(nullptr) {}
    TreeNode(int v, TreeNode * l, TreeNode * r) :
        val(v),
        left(l),
        right(r) {}
    TreeNode(const TreeNode &other) :
        val(other.val),
        left((other.left != nullptr)?new TreeNode(*other.left):nullptr),
        right((other.right != nullptr)?new TreeNode(*other.right):nullptr) {}
    TreeNode(TreeNode &&other) :
        val(other.val),
        left(std::exchange(other.left, nullptr)),
        right(std::exchange(other.right, nullptr)) {}
    ~TreeNode(void) { delete left; delete right; }
    TreeNode& operator=(const TreeNode &other)
    {
        if (this != &other)
        {
            delete left;
            delete right;
            val = other.val;
            left = (other.left != nullptr)?new TreeNode(*other.left):nullptr;
            right = (other.right != nullptr)?new TreeNode(*other.right):nullptr;
        }
        return *this;
    }
    TreeNode& operator=(TreeNode &&other)
    {
        if (this != &other)
        {
            val = other.val;
            left = std::exchange(other.left, nullptr);
            right = std::exchange(other.right, nullptr);
        }
        return *this;
    }
};

TreeNode * vec2tree(const std::vector<int> &tree)
{
    std::queue<TreeNode *> active_nodes;
    
    TreeNode * result = nullptr;
    bool left = true;
    for (const auto &v : tree)
    {
        if (v != null)
        {
            if (result == nullptr)
            {
                result = new TreeNode(v);
                active_nodes.push(result);
                active_nodes.push(result);
            }
            else
            {
                TreeNode * current = new TreeNode(v);
                if (left) active_nodes.front()->left = current;
                else active_nodes.front()->right = current;
                active_nodes.pop();
                active_nodes.push(current);
                active_nodes.push(current);
                left = !left;
            }
        }
        else
        {
            active_nodes.pop();
            left = !left;
        }
    }
    return result;
}

std::vector<int> tree2vec(TreeNode * root)
{
    if (root == nullptr) return {};
    std::vector<int> result;
    std::queue<TreeNode *> active_nodes;
    active_nodes.push(root);
    result.push_back(root->val);
    while (!active_nodes.empty())
    {
        TreeNode * current = active_nodes.front();
        active_nodes.pop();
        if (current->left != nullptr)
        {
            active_nodes.push(current->left);
            result.push_back(current->left->val);
        }
        else result.push_back(null);
        if (current->right != nullptr)
        {
            active_nodes.push(current->right);
            result.push_back(current->right->val);
        }
        else result.push_back(null);
    }
    while (result.back() == null) result.pop_back();
    return result;
}

std::ostream& operator<<(std::ostream &out, const std::vector<int> &vec)
{
    out << '{';
    bool next = false;
    for (const auto &v : vec)
    {
        if (next) out << ", ";
        out << v;
        next = true;
    }
    out << '}';
    return out;
}

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    for (int i = 0; i < n; ++i)
        if (left[i] != right[i]) return false;
    return true;
}

// ################################################################################################
// ################################################################################################

TreeNode * bstFromPreorder(std::vector<int> preorder)
{
#if 1
    const int n = preorder.size();
    if (n == 0) return nullptr;
    int idx = 0;
    
    function<TreeNode*(int, int)> build = [&](int min, int max) -> TreeNode*
    {
        if (idx >= n) return nullptr;
        int value = preorder[idx];
        if ((value > max) || (value < min)) return nullptr;
        TreeNode * root = new TreeNode(value);
        ++idx;
        root->left = build(min, value - 1);
        root->right = build(value + 1, max);
        return root;
    };
    return build(std::numeric_limits<int>::lowest(), std::numeric_limits<int>::max());
#else
    const int n = preorder.size();
    if (n == 0) return nullptr;
    
    function<TreeNode*(int, int)> build = [&](int begin, int end) -> TreeNode*
    {
        if (begin >= end) return nullptr;
        int right;
        for (right = begin + 1; right < end; ++right)
            if (preorder[right] > preorder[begin])
                break;
        return new TreeNode(preorder[begin], build(begin + 1, right), build(right, end));
    };
    return build(0, n);
#endif
}

// ################################################################################################
// ################################################################################################

void test(std::vector<int> preorder, std::vector<int> solution, unsigned int trials = 1)
{
    std::cout << "INPUT: " << preorder << '\n';
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        TreeNode * root = bstFromPreorder(preorder);
        result = tree2vec(root);
        delete root;
    }
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({8, 5, 1, 7, 10, 12}, {8, 5, 10, 1, 7, null, 12}, trials);
    test({1, 3}, {1, null, 3}, trials);
    test({8, 4, 1, 6, 5, 7, 10, 12}, {8, 4, 10, 1, 6, null, 12, null, null, 5, 7}, trials);
    return 0;
}

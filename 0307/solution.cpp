#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <map>

using namespace std;

class NumArray
{
    struct info
    {
        int sum = 0;
        unsigned short range_left = 0;
        unsigned short range_right = 0;
        short offset_right = 0;
    };
    info * m_tree;
    int m_elements = -1;
    int build(const std::vector<int> &nums, int left, int right)
    {
        int curr = ++m_elements;
        m_tree[curr].range_left = left;
        m_tree[curr].range_right = right;
        if (left == right)
        {
            m_tree[curr].sum = nums[left];
            m_tree[curr].offset_right = -1;
        }
        else
        {
            int mid = (left + right) / 2;
            m_tree[curr].sum = build(nums, left, mid);
            m_tree[curr].offset_right = static_cast<short>(m_elements - curr + 1);
            m_tree[curr].sum += build(nums, mid + 1, right);
        }
        return m_tree[curr].sum;
    }
    int update(int idx, int index, int val)
    {
        if (m_tree[idx].range_left == m_tree[idx].range_right)
        {
            int difference = val - m_tree[idx].sum;
            m_tree[idx].sum = val;
            return difference;
        }
        else
        {
            const int midpoint = (m_tree[idx].range_left + m_tree[idx].range_right) / 2;
            int difference;
            if (index > midpoint)
                difference = update(idx + m_tree[idx].offset_right, index, val);
            else
                difference = update(idx + 1, index, val);
            m_tree[idx].sum += difference;
            return difference;
        }
    }
    int sumRange(int idx, unsigned short left, unsigned short right)
    {
        if (idx >= m_elements) return 0;
        int l = std::max(m_tree[idx].range_left, left);
        int r = std::min(m_tree[idx].range_right, right);
        if (l <= r)
        {
            if ((l == m_tree[idx].range_left) && (r == m_tree[idx].range_right))
                return m_tree[idx].sum;
            else
            {
                return sumRange(idx + 1, l, r)
                     + sumRange(idx + m_tree[idx].offset_right, l, r);
            }
        }
        else return 0;
    }
public:
    NumArray(const std::vector<int> &nums) :
        m_tree(new info[nums.size() * 2 + 10]),
        m_elements(-1)
    {
        build(nums, 0, nums.size() - 1);
        ++m_elements;
    }
    ~NumArray(void) { delete [] m_tree; }
    
    void update(int index, int val)
    {
        update(0, index, val);
    }
    
    int sumRange(int left, int right)
    {
        return sumRange(0, left, right);
    }
    friend std::ostream& operator<<(std::ostream &out, NumArray &obj)
    {
        out << '[';
        bool next = false;
        for (int i = 0; i < obj.m_elements; ++i)
        {
            if (next) [[likely]] out << ", ";
            next = true;
            out << '{';
            out << obj.m_tree[i].sum << '|';
            out << obj.m_tree[i].range_left << ':';
            out << obj.m_tree[i].range_right << '|';
            out << obj.m_tree[i].offset_right;
            out << '}';
        }
        out << ']';
        return out;
    }
};

std::ostream& operator<<(std::ostream &out, const std::vector<int> &vec)
{
    out << '{';
    bool next = false;
    for (int v : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    for (int i = 0; i < n; ++i)
        if (left[i] != right[i]) return false;
    return true;
}

struct info
{
    bool operation;
    int left;
    int right;
};

void test(const std::vector<int> initialization,
          std::vector<info> interaction,
          std::vector<int> solution,
          unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result.clear();
        NumArray * obj = new NumArray(initialization);
        for (const auto &i : interaction)
        {
            auto [operation, left, right] = i;
            if (operation) result.push_back(obj->sumRange(left, right));
            else obj->update(left, right);
            //std::cout << *obj << '\n';
        }
        delete obj;
    }
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 5}, {{true, 0, 2}, {false, 1, 2}, {true, 0, 2}}, {9, 8}, trials);
    test({2, 4, 5, 7}, {{true, 0, 2}, {false, 1, 2}, {true, 0, 2}}, {11, 9}, trials);
    return 0;
}

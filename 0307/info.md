Given an integer array `nums`, handle multiple queries of the following types:

- **Update** the value of an element in `nums`.
- Calculate the **sum** of the elements of `nums` between indices `left` and `right` inclusive where `left <= right`.

Implement the `NumArray` class:

- `NumArray(int[] nums)` Initializes the object with the integer array `nums`.
- `void update(int index, int val)` **Updates** the value of `nums[index]` to be `val`.
- `int sumRange(int left, int right)` Returns the **sum** of the elements of `nums` between indices `left` and `right` **inclusive** (i.e. `nums[left] + nums[left + 1] + ... + nums[right]`).
 

**Example 1:**<br>
*Input:*<br>
`["NumArray", "sumRange", "update", "sumRange"]`<br>
`[[[1, 3, 5]], [0, 2], [1, 2], [0, 2]]`<br>
*Output:*<br>
`[null, 9, null, 8]`<br>
*Explanation*<br>
`NumArray numArray = new NumArray([1, 3, 5]);`<br>
`numArray.sumRange(0, 2); // return 1 + 3 + 5 = 9`<br>
`numArray.update(1, 2);   // nums = [1, 2, 5]`<br>
`numArray.sumRange(0, 2); // return 1 + 2 + 5 = 8`<br>
 
**Constraints:**<br>
- `1 <= nums.length <= 3 * 10^4`
- `-100 <= nums[i] <= 100`
- `0 <= index < nums.length`
- `-100 <= val <= 100`
- `0 <= left <= right < nums.length`
- At most `3 * 10^4` calls will be made to `update` and `sumRange`.

=Convert Sorted Array to Binary Search Tree=

Given an integer array `nums` where the elements are sorted in **ascending order**, convert *it to a* ***height-balanced*** *binary search tree*.

A **height-balanced** binary tree is a binary tree in which the depth of the two subtrees of every node never differs by more than one.
 
**Example 1:**<br>
*Input:* `nums = [-10, -3, 0, 5, 9]`<br>
*Output:* `[0, -3, 9, -10, null, 5]`<br>
```mermaid 
graph TB
A[0] --> B[-3]
B --> C[-10]
A --> D[9]
D --> E[5]
```
*Explanation:* `[0, -10, 5, null, -3, null, 9]` is also accepted.<br>
```mermaid 
graph TB
A[0] --> B[-10]
B --> C[-3]
A --> D[5]
D --> E[9]
```

**Example 2**<br>
*Input:* `nums = [1, 3]`
*Output:* `[3, 1]` or `[1, 3]` are both a height-balanced BSTs.
```mermaid 
graph TB
A[3] --> B[1]
```
```mermaid 
graph TB
A[1] --> B[3]
```

**Constrains:**<br>
- `1 <= nums.length <= 10^4`
- `-10^4 <= nums[i] <= 10^4`
- `nums` is sorted in a **strictly increasing** order.



#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

std::vector<int> findRedundantConnection(std::vector<std::vector<int> > &edges)
{
    auto search = [](const int * lut, int idx)
    {
        while (lut[idx] != idx) idx = lut[idx];
        return idx;
    };
    const std::size_t n = edges.size();
    int lut[n + 1];
    for (std::size_t i = 0; i <= n; ++i) lut[i] = i;
    for (auto edge : edges)
    {
        int idx_a = search(lut, edge[0]);
        int idx_b = search(lut, edge[1]);
        if (idx_a == idx_b) return edge; // Loop detected.
        lut[idx_a] = idx_b;
    }
    return {};
}

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    const std::size_t n = left.size();
    for (std::size_t i = 0; i < n; ++i)
        if (left[i] != right[i]) return false;
    return true;
}

std::ostream& operator<<(std::ostream &out, const std::vector<int> &info)
{
    out << '{';
    bool next = false;
    for (int v : info)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

void test(std::vector<std::vector<int> > edges, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findRedundantConnection(edges);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {1, 3}, {2, 3}}, {2, 3}, trials);
    test({{1, 2}, {2, 3}, {3, 4}, {1, 4}, {1, 5}}, {1, 4}, trials);
    return 0;
}

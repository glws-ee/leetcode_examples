#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

bool stoneGame(std::vector<int> piles)
{
#if 0
    return true; // Alice always wins.
#else
    const int n = piles.size();
    int dp_alice[n][n];
    int dp_lee[n][n];
    for (int left = n - 1; left >= 0; --left)
    {
        dp_alice[left][left] = piles[left];
        dp_lee[left][left] = 0;
        for (int right = left + 1; right < n; ++right)
        {
            if (piles[left] + dp_lee[left + 1][right] > piles[right] + dp_lee[left][right - 1])
            {
                dp_alice[left][right] = piles[left] + dp_lee[left + 1][right];
                dp_lee[left][right] = dp_alice[left + 1][right];
            }
            else
            {
                dp_alice[left][right] = piles[right] + dp_lee[left][right - 1];
                dp_lee[left][right] = dp_alice[left][right - 1];
            }
        }
    }
    return dp_alice[0][n - 1] > dp_lee[0][n - 1];
#endif
}

void test(std::vector<int> piles, bool solution, unsigned int trials = 1)
{
    bool result;
    for (unsigned int i = 0; i < trials; ++i)
        result = stoneGame(piles);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({5, 3, 4, 5}, true, trials);
    return 0;
}

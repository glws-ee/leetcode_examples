#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <cmath>

using namespace std;

constexpr int null = -1000;

bool judgeSquareSum(int c)
{
    const int thr = std::sqrt(c) + 1;
    for (int i = 0; i < thr; ++i)
    {
        int j = c - i * i;
        int i_sqrt = std::sqrt(j);
        if (i_sqrt * i_sqrt == j) return true;
    }
    return false;
}

void test(int c, bool solution, unsigned int trials = 1)
{
    bool result;
    for (unsigned int i = 0; i < trials; ++i)
        result = judgeSquareSum(c);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(5, true, trials);
    test(3, false, trials);
    test(4, true, trials);
    test(2, true, trials);
    test(1, true, trials);
    return 0;
}

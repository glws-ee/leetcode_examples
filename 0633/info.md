=Sum of Square Numbers=

Given a non-negative integer `c`, decide whether there're two integers `a` and `b` such that `a^2 + b^2 = c`.

**Example 1:**<br>
*Input:* `c = 5`<br>
*Output:* `true`<br>
*Explanation:* `1 * 1 + 2 * 2 = 5`<br>

**Example 2:**<br>
*Input:* `c = 3`<br>
*Output:* `false`<br>

**Example 3:**<br>
*Input:* `c = 4`<br>
*Output:* `true`<br>

**Example 4:**<br>
*Input:* `c = 2`<br>
*Output:* `true`<br>

**Example 5:**<br>
*Input:* `c = 1`<br>
*Output:* `true`<br>
 
**Constraints:**<br>
- `0 <= c <= 2^31 - 1`

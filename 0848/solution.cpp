#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include "test_a.hpp"

using namespace std;

constexpr int null = -1000;

std::string shiftingLetters(std::string s, std::vector<int> shifts)
{
#if 1
    const long n = s.size();
    const long thr = static_cast<long>('z' - 'a' + 1);
    for (long i = n - 1, acum = 0; i >= 0; --i)
        s[i] = static_cast<char>((static_cast<long>(s[i] - 'a') + (acum += shifts[i])) % thr) + 'a';
    return s;
#elif 0
    const long n = s.size();
    const long thr = static_cast<long>('z' - 'a' + 1);
    long values[n];
    for (long i = n - 1, acum = 0; i >= 0; --i)
        values[i] = s[i] - 'a' + (acum += shifts[i]);
    for (long i = 0; i < n; ++i)
        s[i] = static_cast<char>(values[i] % thr) + 'a';
    return s;
#else
    const long n = s.size();
    const long thr = static_cast<long>('z' - 'a' + 1);
    long values[n];
    for (long i = 0; i < n; ++i)
        values[i] = s[i] - 'a';
    for (long i = 0; i < n; ++i)
        for (long j = 0; j <= i; ++j)
            values[j] += shifts[i];
    for (long i = 0; i < n; ++i)
        s[i] = static_cast<char>(values[i] % thr) + 'a';
    return s;
#endif
}

std::ostream& operator<<(std::ostream &out, const std::string &s)
{
    if (s.size() > 40)
    {
        out << '"' << s.substr(0, 20) << "..." << s.substr(s.size() - 20, 20) << '"';
    }
    else std::operator<<(out, s);
    return out;
}

void test(std::string s, std::vector<int> shifts, std::string solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = shiftingLetters(s, shifts);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 100'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("abc", {3, 5, 9}, "rpl", trials);
    test("aaa", {1, 2, 3}, "gfd", trials);
    test(testA::s, testA::shifts, testA::solution, trials);
    return 0;
}

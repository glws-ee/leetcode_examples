#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <functional>

using namespace std;

constexpr int null = -1000;

std::vector<int> sumOfDistancesInTree(int n, std::vector<std::vector<int> > edges)
{
    std::vector<int> result(n), subtree(n, 1), adjacent[n];
    // Deep-first search (first pass)
    std::function<int(int, int)> dfs = [&](int node, int parent)
    {
        int curr = 0;
        for (int neighbor : adjacent[node])
        {
            if (neighbor != parent)
            {
                curr += dfs(neighbor, node);
                curr += subtree[neighbor];
                subtree[node] += subtree[neighbor];
            }
        }
        return curr;
    };
    // Deep-first search (second pass)
    std::function<void(int, int, int)> dfs2 = [&](int node, int parent, int now)
    {
        result[node] = now;
        for (int neighbor : adjacent[node])
            if (neighbor != parent)
                dfs2(neighbor, node, now - subtree[neighbor] + subtree[0] - subtree[neighbor]);
    };
    
    for (const auto &node : edges)
    {
        adjacent[node[0]].push_back(node[1]);
        adjacent[node[1]].push_back(node[0]);
    }
    dfs2(0, -1, dfs(0, -1));
    return result;
}

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    for (int i = 0; i < n; ++i)
        if (left[i] != right[i]) return false;
    return true;
}

std::ostream& operator<<(std::ostream &out, const std::vector<int> &dist)
{
    out << '{';
    bool next = false;
    for (int d : dist)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << d;
    }
    out << '}';
    return out;
}

void test(int n, std::vector<std::vector<int> > edges, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = sumOfDistancesInTree(n, edges);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(6, {{0, 1}, {0, 2}, {2, 3}, {2, 4}, {2, 5}}, {8, 12, 6, 10, 10, 10}, trials);
    test(1, {}, {0}, trials);
    test(2, {{1, 0}}, {1, 1}, trials);
    return 0;
}

#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

struct ListNode
{
    int val = 0;
    ListNode * next = nullptr;
    ListNode() = default;
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode * next) : val(x), next(next) {}
    ~ListNode(void) { delete next; }
    ListNode(const ListNode &other) :
        val(other.val),
        next((other.next != nullptr)?new ListNode(*other.next):nullptr) {}
    ListNode(ListNode &&other) :
        val(other.val),
        next(std::exchange(other.next, nullptr)) {}
    ListNode& operator=(const ListNode &other)
    {
        if (this != &other)
        {
            delete next;
            val = other.val;
            next = (other.next != nullptr)?new ListNode(*other.next):nullptr;
        }
        return *this;
    }
    ListNode& operator=(ListNode &&other)
    {
        if (this != &other)
        {
            delete next;
            val = other.val;
            next = std::exchange(other.next, nullptr);
        }
        return *this;
    }
};

ListNode * vec2list(const std::vector<int> &vec)
{
    if (vec.size() == 0) return nullptr;
    ListNode * head = new ListNode(vec[0]);
    ListNode * prev = head;
    for (std::size_t i = 1; i < vec.size(); ++i)
        prev = (prev->next = new ListNode(vec[i]));
    return head;
}

std::vector<int> list2vec(ListNode * head)
{
    std::vector<int> result;
    for (ListNode * current = head; current != nullptr; current = current->next)
        result.push_back(current->val);
    return result;
}

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    const std::size_t n = left.size();
    for (std::size_t i = 0; i < n; ++i)
        if (left[i] != right[i]) return false;
    return true;
}

std::ostream& operator<<(std::ostream &out, std::vector<int> &info)
{
    out << '{';
    bool next = false;
    for (int v : info)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

ListNode * reverseBetween(ListNode * head, int left, int right)
{
    ListNode * current = head->next;
    ListNode * begin = new ListNode(head->val);
    ListNode * end = begin, * it = nullptr;
    int idx = 1;
    for (; idx < left; ++idx, current = current->next)
    {
        it = end;
        end = (end->next = new ListNode(current->val));
    }
    if (it == nullptr) // At the beginning of the list.
    {
        for (; idx < right; ++idx, current = current->next)
            begin = new ListNode(current->val, begin);
    }
    else // In the middle of the list.
    {
        for (; idx < right; ++idx, current = current->next)
            it->next = new ListNode(current->val, it->next);
    }
    for (; current != nullptr; current = current->next)
        end = (end->next = new ListNode(current->val));
    return begin;
}

void test(std::vector<int> list, int left, int right, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(list);
        ListNode * reversed = reverseBetween(head, left, right);
        result = list2vec(reversed);
        delete head;
        delete reversed;
    }
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, 2, 4, {1, 4, 3, 2, 5}, trials);
    test({1, 2, 3, 4, 5}, 1, 4, {4, 3, 2, 1, 5}, trials);
    test({5}, 1, 1, {5}, trials);
    return 0;
}

#include <vector>
#include <unordered_map>
#include <map>
#include <iostream>

using namespace std;

vector<int> twoSum(vector<int>& nums, int target)
{
#if 1
    std::unordered_map<int, int> possible_values;
    const int n = nums.size();
    for (int i = 0; i < n; ++i)
    {
        auto it = possible_values.find(target - nums[i]);
        if (it != possible_values.end())
            return {it->second, i};
        possible_values[nums[i]] = i;
    }
    return {};
#else
    for (int i = 0; i != nums.size(); ++i)
        for (int j = 0; j != nums.size(); ++j)
            if ((i != j) && (nums[i] + nums[j] == target))
                return {i, j};
    return {};
#endif
}

ostream& operator<<(ostream &out, const vector<int> &vec)
{
    out << '{';
    if (vec.size() > 0)
    {
        out << vec[0];
        for (int i = 1; i < vec.size(); ++i)
            out << ", " << vec[i];
    }
    out << '}';
    return out;
}

void test(std::vector<int> nums, int target, vector<int> solution)
{
    vector<int> result = twoSum(nums, target);
    if ((result.size() == 2)
    && (((result[0] == solution[0]) && (result[1] == solution[1]))
    ||  ((result[0] == solution[1]) && (result[1] == solution[0]))))
         std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
    test({2, 7, 11, 15}, 9, {0, 1});
    test({3, 2, 4}, 6, {1, 2});
    test({3, 3}, 6, {1, 0});
    return 0;
}

=Reduce Array Size to The Half=

Given an array `arr`. You can choose a set of integers and remove all the occurrences of these integers in the array.

Return the *minimum size of the set* so that **at least** half of the integers of the array are removed.

**Example 1:**<br>
*Input:* `arr = [3, 3, 3, 3, 5, 5, 5, 2, 2, 7]`<br>
*Output:* `2`<br>
*Explanation:* Choosing `{3, 7}` will make the new array `[5, 5, 5, 2, 2]` which has size `5` (i.e equal to half of the size of the old array). Possible sets of size `2` are `{3, 5}`, `{3, 2}`, `{5, 2}`. Choosing set `{2, 7}` is not possible as it will make the new array `[3, 3, 3, 3, 5, 5, 5]` which has size greater than half of the size of the old array.<br>

**Example 2:**<br>
*Input:* `arr = [7, 7, 7, 7, 7, 7]`<br>
*Output:* `1`<br>
*Explanation:* The only possible set you can choose is `{7}`. This will make the new array empty.<br>

**Example 3:**<br>
*Input:* `arr = [1, 9]`<br>
*Output:* `1`<br>

**Example 4:**<br>
*Input:* `arr = [1000, 1000, 3, 7]`<br>
*Output:* `1`<br>

**Example 5:**<br>
*Input:* `arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]`<br>
*Output:* `5`<br>

**Constraints:**<br>
- `1 <= arr.length <= 10^5`
- `arr.length` is even.
- `1 <= arr[i] <= 10^5`




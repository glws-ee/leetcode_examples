#include <vector>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <queue>

using namespace std;

bool isPossible(const vector<int> &target, int sum, vector<int> &current)
{
    unsigned int correct = 0;
    for (unsigned int i = 0; i < target.size(); ++i)
    {
        if (sum <= target[i])
        {
            int previous = current[i];
            int new_sum = sum + sum - current[i];
            current[i] = sum;
            if (isPossible(target, new_sum, current)) return true;
            current[i] = previous;
        }
        else if (target[i] == current[i]) ++correct;
    }
    return correct == target.size();
}

#if 0
bool isPossible(vector<int>& target)
{
    const int size = target.size();
    vector<int> source(size, 1);
    return isPossible(target, size, source);
}
#elif 0
bool isPossible(vector<int>& target)
{
    if (target.size() == 1) return target[0] == 1;
    long sum = 0;
    for (const auto &v : target)
        sum += v;
    std::make_heap(target.begin(), target.end());
    while (target[0] != 1)
    {
        std::pop_heap(target.begin(), target.end());
        long selected = target.back();
        long step = sum - selected;
        if (selected - step < 1) return false;
        
        long new_value = 1 + ((selected - step - 1) % step);
        sum += new_value - selected;
        target.back() = new_value;
        std::push_heap(target.begin(), target.end());
    }
    return target[0] == 1;
    // ============================================================================================
        //long number_of_steps = std::max(1L, (selected - target.front()) / step);
        //long difference = step * number_of_steps;
        //sum -= difference;
        //target.back() = static_cast<int>(selected - difference);
    // ============================================================================================
        //int previous;
        //do
        //{
        //    previous = 2 * selected - sum;
        //    //sum -= selected - previous;
        //    //sum = sum - selected + previous
        //    //    = sum - selected + 2 * selected - sum
        //    //    = selected;
        //    sum = selected;
        //    std::cout << selected << ' ' << previous << ' ' << sum << '\n';
        //    selected = previous;
        //} while (previous > target.front());
        //if (previous < 1) return false;
        //target.back() = previous;
        //std::cout << "----------------------------\n";
    // ============================================================================================
}
#else
bool isPossible(vector<int>& target)
{
    if (target.size() == 1) return target[0] == 1;
    long sum = 0;
    for (const auto &v : target)
        sum += v;
    std::priority_queue<long> queue(target.begin(), target.end());
    while (queue.top() != 1)
    {
        long selected = queue.top();
        queue.pop();
        long step = sum - selected;
        if (selected - step < 1) return false;
        
        long new_value = 1 + ((selected - step - 1) % step);
        sum += new_value - selected;
        queue.push(new_value);
    }
    return queue.top() == 1;
    ////long sum = 0;
    ////for (const auto &v : target)
    ////    sum += v;
    ////std::priority_queue<long> queue(target.begin(), target.end());
    ////while (true)
    ////{
    ////    int selected = queue.top();
    ////    queue.pop();
    ////    if (selected == 1) return true;
    ////    long step = sum - selected;
    ////    if (step == 0) return false;
    ////    long number_of_steps = std::max(1L, (selected - queue.top()) / step);
    ////    long difference = step * number_of_steps;
    ////    sum -= difference;
    ////    if (difference >= selected) return false;
    ////    queue.push(selected - difference);
    ////}
    ////return false;
}
#endif

//sp + sp - x = 17
//r = s - 9 = 8
//sp = r + x = r + x
//
//(r + x) + (r + x) - x = s
//2r + 2x - x = s
//x = s - 2r = s - 2 * (s - c) = s - 2 * s + 2 * c = 2 * c - s;
//x = 17 - 2 * 8 = 1 ---- 2 * 9 - 17 = 18 - 17 = 1;

void test(vector<int> target, bool solution, unsigned int trials)
{
    bool result;
    for (unsigned int i = 0; i < trials; ++i)
        result = isPossible(target);
    if (result == solution) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    //constexpr unsigned int trials = 100000;
    constexpr unsigned int trials = 10000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({9,3,5}, true, trials);
    test({1,1,1,2}, false, trials);
    test({8,5}, true, trials);
    test({3,5,33}, true, trials);
    test({1,1000000000}, true, trials);
    test({9,9,9}, false, trials);
    test({1,1,2}, false, trials);
    test({2}, false, trials);
    return 0;
}


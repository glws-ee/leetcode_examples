#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <tuple>
#include <limits>

using namespace std;

constexpr int null = -1000;

int minCut(std::string s)
{
    // Base cases.
    if (s.size() <= 1) return 0;
    else if (s.size() == 2)
    {
        if (s[0] == s[1]) return 0;
        else return 1;
    }
    // General case.
    const int n = s.size();
    bool dp[n][n];
    std::memset(dp, false, sizeof(dp));
    for (int i = 0; i < n; ++i)
        dp[i][i] = true;
    for (int i = 1; i < n; ++i)
        if (s[i - 1] == s[i])
            dp[i - 1][i] = true;
    for (int i = n - 1; i >= 0; --i)
        for (int j = n - 1; j >= i + 2; --j)
            if ((s[i] == s[j]) && dp[i + 1][j - 1])
                dp[i][j] = true;
    int cuts[n];
    for (int i = 0; i < n; ++i)
    {
        if (dp[0][i]) cuts[i] = 0;
        else
        {
            int aux = std::numeric_limits<int>::max();
            for (int j = 0; j < i; ++j)
                if (dp[j + 1][i] && (aux > cuts[j] + 1))
                    aux = cuts[j] + 1;
            cuts[i] = aux;
        }
    }
    return cuts[n - 1];
}

void test(std::string s, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minCut(s);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("aab", 1, trials);
    test("a", 0, trials);
    test("ab", 1, trials);
    test("ababbbabbababa", 3, trials);
    test("aaaa", 0, trials);
    test("geek", 2, trials);
    test("abcde", 4, trials);
    test("abbac", 1, trials);
    test("abbaabba", 0, trials);
    return 0;
}

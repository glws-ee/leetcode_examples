=Palindrome Partitioning II=

Given a string `s`, partition `s` such that every substring of the partition is a palindrome.

Return *the minimum cuts needed* for a palindrome partitioning of `s`.

**Example 1:**<br>
*Input:* `s = "aab"`<br>
*Output:* `1`<br>
*Explanation:* The palindrome partitioning `["aa", "b"]` could be produced using `1` cut.<br>

**Example 2:**<br>
*Input:* `s = "a"`<br>
*Output:* `0`<br>

**Example 3:**<br>
*Input:* `s = "ab"`<br>
*Output:* `1`<br>
 
**Constraints:**<br>
- `1 <= s.length <= 2000`
- `s` consists of lower-case English letters only.




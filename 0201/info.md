=Bitwise AND of Numbers Range=

Given two integers `left` and `right` that represent the range `[left, right]`, return *the bitwise AND of all numbers in this range, inclusive*.

**Example 1:**<br>
*Input:* `left = 5, right = 7`<br>
*Output:* `4`<br>

**Example 2:**<br>
*Input:* `left = 0, right = 0`<br>
*Output: 0*<br>

**Example 3:**<br>
*Input:* `left = 1, right = 2147483647`<br>
*Output:* `0`<br>
 
**Constraints:**<br>
- `0 <= left <= right <= 2^31 - 1`




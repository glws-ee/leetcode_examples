#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <fmt/core.h>

using namespace std;

constexpr int null = -1000;

int rangeBitwiseAnd(int left, int right)
{
#if 1
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(NULL);
    const long difference = right - left;
    unsigned int result = 0;
    for (long i = 0, power = 1; i < 31; ++i, power *= 2, left >>= 1, right >>= 1)
        result = (result >> 1) | ((difference < power) && ((left & 1) & (right & 1))) * 0x40'00'00'00;
    return result;
#else
    constexpr unsigned int one = 1 << 30;
    const int difference = right - left;
    unsigned int result = 0;
    for (int i = 0, power = 1; i < 31; ++i, power *= 2, left >>= 1, right >>= 1)
    {
        //fmt::print("{:2d} Left: {:10d} Right: {:10d}\n", i, left, right);
        if ((difference < power) && ((left & 1) & (right & 1)))
            result = (result >> 1) | one;
        else result >>= 1;
    }
    return result;
#endif
}

void test(int left, int right, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = rangeBitwiseAnd(left, right);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    // Difference = 7 - 5 = 2
    // IF DIFF > 2^BIT
    //      PUT 0
    // ELSE
    //      PUT LEFT[BIT] & RIGHT[BIT]
    // END
    // 5: 101
    // 6: 110
    // 7: 111
    // ------
    //    100 -> 4
    test(5, 7, 4, trials);
    test(0, 0, 0, trials);
    test(1, 2'147'483'647, 0, trials);
    // 3: 011
    // 4: 100
    // 5: 101
    // 6: 110
    // 7: 111
    // ------
    //    000 -> 0
    test(3, 7, 0, trials);
    // 6: 110
    // 7: 111
    // ------
    //    110 -> 6
    test(6, 7, 6, trials);
    //   9: 00001001
    // 255: 11111111
    test(9, 255, 0, trials);
    //   9: 000001001
    // 256: 100000000
    test(9, 256, 0, trials);
    //   9: 011000110
    // 256: 100000000
    test(198, 256, 0, trials);
    //   9: 011000110
    // 255: 011111111
    test(198, 255, 192, trials);
    return 0;
}

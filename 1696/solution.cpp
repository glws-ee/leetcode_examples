#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <limits>
#include <map>
#include <fmt/core.h>
#include <fmt/color.h>
#include "values.hpp"
#include "valuesB.hpp"

using namespace std;
#define DEBUG 0


std::ostream& operator<<(std::ostream &out, const std::vector<int> &info)
{
    out << '{';
    bool next = false;
    for (int v : info)
    {
        if (next) out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

int maxResult(std::vector<int> nums, int k)
{
#if 1
    const int n = nums.size();
    if (n == 1) return nums[0];
    else if (k >= n - 1) // Special case A: you can jump the whole sequence in one go.
    {
        int accum = 0;
        for (int v : nums)
            if (v > 0)
                accum += v;
        if (nums[0] < 0) accum += nums[0];
        if (nums[n - 1] < 0) accum += nums[n - 1];
        return accum;
    }
    else if (k > 10) // Special case B: 'large' number of neighbors.
    {
        std::map<int, int> histogram;
        histogram[nums[0]] = 1;
        int idx = 1;
        for (; idx <= k; ++idx)
        {
            nums[idx] += histogram.rbegin()->first;
            auto search = histogram.find(nums[idx]);
            if (search == histogram.end())
                histogram[nums[idx]] = 1;
            else ++(search->second);
        }
        for (; idx < n; ++idx)
        {
            auto remove = histogram.find(nums[idx - k - 1]);
            if (remove->second > 1) --(remove->second);
            else histogram.erase(remove);
            nums[idx] += histogram.rbegin()->first;
            auto search = histogram.find(nums[idx]);
            if (search == histogram.end())
                histogram[nums[idx]] = 1;
            else ++(search->second);
        }
        return nums[n - 1];
    }
    else // Generic case.
    {
        for (int i = 1; i < n; ++i)
        {
            int max_value = std::numeric_limits<int>::lowest();
            for (int j = 1; j <= k; ++j)
                if (i - j >= 0) max_value = std::max(max_value, nums[i - j]);
            nums[i] += max_value;
        }
        return nums[n - 1];
    }
#else
    const int n = nums.size();
    if (k >= n - 1) // Special case A: you can jump the whole sequence in one go.
    {
        int accum = 0;
        for (int v : nums)
            if (v > 0)
                accum += v;
        if (nums[0] < 0) accum += nums[0];
        if (nums[n - 1] < 0) accum += nums[n - 1];
        return accum;
    }
    else if (k > 10) // Special case B: 'large' number of neighbors.
    {
#if DEBUG
        fmt::print("n: {}\n", n);
        fmt::print("k: {}\n", k);
        std::cout << nums << '\n'; // DEBUG
#endif
        std::map<int, int> histogram;
        histogram[nums[0]] = 1;
        int idx = 1;
        for (; idx <= k; ++idx)
        {
            nums[idx] += histogram.rbegin()->first;
            auto search = histogram.find(nums[idx]);
            if (search == histogram.end())
                histogram[nums[idx]] = 1;
            else ++(search->second);
        }
        for (; idx < n; ++idx)
        {
#if DEBUG
            fmt::print("{{");
            bool next = false;
            for (int m = 0; m < n; ++m)
            {
                if (next) [[likely]] fmt::print(", ");
                next = true;
                if (m == idx - k - 1) fmt::print(fg(fmt::color::black) | bg(fmt::color::red), "{:3d} [-]", nums[m]);
                else if (m == idx) fmt::print(fg(fmt::color::black) | bg(fmt::color::orange), "{:3d} [+]", nums[m]);
                else fmt::print("{:3d}", nums[m]);
            }
            fmt::print("}}\n");
            fmt::print("{:02d}: [R]={:3d}", idx, nums[idx - k - 1]);
            fmt::print(" [A]={:3d}", nums[idx]);
            fmt::print(" [Max]={:3d}", histogram.rbegin()->first);
            fmt::print(" [Min]={:3d}", histogram.begin()->first);
#endif
            auto remove = histogram.find(nums[idx - k - 1]);
            if (remove->second > 1) --(remove->second);
            else histogram.erase(remove);
#if DEBUG
            fmt::print(" ## ");
            fmt::print(" [Max]={:3d}", histogram.rbegin()->first);
            fmt::print(" [Min]={:3d}", histogram.begin()->first);
#endif
            nums[idx] += histogram.rbegin()->first;
            auto search = histogram.find(nums[idx]);
            if (search == histogram.end())
                histogram[nums[idx]] = 1;
            else ++(search->second);
#if DEBUG
            std::cout << '\n';
#endif
        }
        return nums[n - 1];
    }
    else // Generic case.
    {
        for (int i = 1; i < n; ++i)
        {
            int max_value = std::numeric_limits<int>::lowest();
            for (int j = 1; j <= k; ++j)
                if (i - j >= 0) max_value = std::max(max_value, nums[i - j]);
            nums[i] += max_value;
        }
        return nums[n - 1];
    }
#endif
}

void test(std::vector<int> nums, int k, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = maxResult(nums, k);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 10000000;
    constexpr unsigned int trials = 1000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, -1, -2, 4, -7, 3}, 7, 8, trials);
    test({1, -1, -2, 4, -7, 3}, 2, 7, trials);
    test({10, -5, -2, 4, 0, 3}, 3, 17, trials);
    test({1, -5, -20, 4, -1, 3, -6, -3}, 2, 0, trials);
    test(testA::nums, testA::k, testA::solution, trials);
    test(testB::nums, testB::k, testB::solution, trials);
    return 0;
}

#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

struct ListNode
{
    int val = 0;
    ListNode * next = nullptr;
    ListNode() = default;
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode * next) : val(x), next(next) {}
    ~ListNode(void) { delete next; }
    ListNode(const ListNode &other) :
        val(other.val),
        next((other.next != nullptr)?new ListNode(*other.next):nullptr) {}
    ListNode(ListNode &&other) :
        val(other.val),
        next(std::exchange(other.next, nullptr)) {}
    ListNode& operator=(const ListNode &other)
    {
        if (this != &other)
        {
            delete next;
            val = other.val;
            next = (other.next != nullptr)?new ListNode(*other.next):nullptr;
        }
        return *this;
    }
    ListNode& operator=(ListNode &&other)
    {
        if (this != &other)
        {
            delete next;
            val = other.val;
            next = std::exchange(other.next, nullptr);
        }
        return *this;
    }
};

ListNode * vec2list(const std::vector<int> &vec)
{
    if (vec.size() == 0) return nullptr;
    ListNode * head = new ListNode(vec[0]);
    ListNode * prev = head;
    for (std::size_t i = 1; i < vec.size(); ++i)
        prev = (prev->next = new ListNode(vec[i]));
    return head;
}

std::vector<int> list2vec(ListNode * head)
{
    std::vector<int> result;
    for (ListNode * current = head; current != nullptr; current = current->next)
        result.push_back(current->val);
    return result;
}

bool operator==(const std::vector<int> &left, const std::vector<int> &right)
{
    if (left.size() != right.size()) return false;
    const std::size_t n = left.size();
    for (std::size_t i = 0; i < n; ++i)
        if (left[i] != right[i]) return false;
    return true;
}

std::ostream& operator<<(std::ostream &out, std::vector<int> &info)
{
    out << '{';
    bool next = false;
    for (int v : info)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << v;
    }
    out << '}';
    return out;
}

ListNode * reverseKGroup(ListNode * head, int k)
{
    if (k < 2) return head; // Nothing to do here.
    
    ListNode * stack[k];
    int count = 0;
    ListNode * result = nullptr, * last = nullptr;
    while (head != nullptr)
    {
        stack[count++] = head;
        head = head->next;
        if (count == k)
        {
            --count;
            if (last != nullptr) last->next = stack[count];
            if (result == nullptr) result = stack[count];
            for (; count > 0; --count)
                stack[count]->next = stack[count - 1];
            stack[0]->next = nullptr;
            last = stack[0];
        }
    }
    if (count > 0)
        last->next = stack[0];
    
    return result;
}

void test(std::vector<int> input, int k, std::vector<int> solution, unsigned int trials = 1)
{
    std::vector<int> result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        ListNode * head = vec2list(input);
        head = reverseKGroup(head, k);
        result = list2vec(head);
        delete head;
    }
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3, 4, 5}, 2, {2, 1, 4, 3, 5}, trials);
    test({1, 2, 3, 4, 5}, 3, {3, 2, 1, 4, 5}, trials);
    test({1, 2, 3, 4, 5}, 1, {1, 2, 3, 4, 5}, trials);
    test({1}, 1, {1}, trials);
    return 0;
}

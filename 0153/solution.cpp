#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

int findMin(std::vector<int> nums)
{
    int begin = 0, end = nums.size() - 1;
    while (begin < end)
    {
        if (nums[begin] < nums[end]) return nums[begin];
        else if (int mid = (begin + end) / 2; nums[mid] > nums[end]) begin = mid + 1;
        else end = mid;
    }
    return nums[begin];
}

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findMin(nums);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({3, 4, 5, 1, 2}, 1, trials);
    test({4, 5, 6, 7, 0, 1, 2}, 0, trials);
    test({11, 13, 15, 17}, 11, trials);
    return 0;
}

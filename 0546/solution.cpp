#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <limits>
#include <map>
#include <unordered_map>

using namespace std;

constexpr int null = -1000;
int removeBoxesDP(std::vector<int> &dp,
                  const std::vector<int> &boxes,
                  int begin,
                  int end,
                  int repetition,
                  const int n,
                  const int n2)
{
    if (begin > end) return 0;
    int &current = dp[begin + end * n + repetition * n2];
    if (current != 0) return current;
    int res = (repetition + 1) * (repetition + 1) + removeBoxesDP(dp, boxes, begin + 1, end, 0, n, n2);
    for (int m = begin + 1; m <= end; ++m)
        if (boxes[begin] == boxes[m])
            res = std::max(res, removeBoxesDP(dp, boxes, begin + 1, m - 1, 0, n, n2)
                              + removeBoxesDP(dp, boxes, m, end, repetition + 1, n, n2));
    return current = res;
}

int removeBoxesDP(std::vector<int> boxes)
{
    const int n = boxes.size();
    std::vector<int> dp(n * n * n, 0);
    return removeBoxesDP(dp, boxes, 0, n - 1, 0, n, n * n);
}

inline int square(char value) { return static_cast<int>(value) * static_cast<int>(value); }

int removeBoxes(std::vector<std::pair<char, char> > &&groups)
{
    if (groups.size() == 1) return square(groups[0].second);
    else
    {
        const int n = groups.size();
        int max_score = 0;
        for (int i = 0; i < n; ++i)
        {
            std::vector<std::pair<char, char> > current = groups;
            current.erase(current.begin() + i);
            if ((i > 0) && (i < n - 1) && (current[i].first == current[i - 1].first))
            {
                current[i - 1].second += current[i].second;
                current.erase(current.begin() + i);
            }
            max_score = std::max(max_score,
                                 removeBoxes(std::move(current))
                                    + square(groups[i].second));
        }
        return max_score;
    }
}

int removeBoxes(std::string &&groups)
{
    if (groups.size() == 2) return square(groups[1]);
    else
    {
        const int n = groups.size();
        int max_score = 0;
        for (int i = 0; i < n; i += 2)
        {
            std::string current = groups;
            current.erase(i, 2);
            if ((i > 0) && (i < n - 2) && (current[i] == current[i - 2]))
            {
                current[i - 1] += current[i + 1];
                current.erase(i, 2);
            }
            max_score = std::max(max_score,
                                 removeBoxes(std::move(current))
                                     + square(groups[i + 1]));
        }
        return max_score;
    }
}

int removeBoxes(std::vector<std::pair<char, char> > &&groups,
                std::map<std::vector<std::pair<char, char> >, int> &memory)
{
    if (groups.size() == 1) return groups[0].second * groups[0].second;
    else if (auto it = memory.find(groups); it != memory.end()) return it->second;
    else
    {
        const int n = groups.size();
        int max_score = 0;
        for (int i = 0; i < n; ++i)
        {
            std::vector<std::pair<char, char> > current = groups;
            current.erase(current.begin() + i);
            if ((i > 0) && (i < n - 1) && (current[i].first == current[i - 1].first))
            {
                current[i - 1].second += current[i].second;
                current.erase(current.begin() + i);
            }
            max_score = std::max(max_score,
                                 removeBoxes(std::move(current), memory)
                                    + square(groups[i].second));
        }
        memory[groups] = max_score;
        return max_score;
    }
}

int removeBoxes(std::string &&groups, std::unordered_map<std::string, int> &memory)
{
    if (groups.size() == 2) return square(groups[1]);
    else if (auto it = memory.find(groups); it != memory.end()) return it->second;
    else
    {
        const int n = groups.size();
        int max_score = 0;
        for (int i = 0; i < n; i += 2)
        {
            std::string current = groups;
            current.erase(i, 2);
            if ((i > 0) && (i < n - 2) && (current[i] == current[i - 2]))
            {
                current[i - 1] += current[i + 1];
                current.erase(i, 2);
            }
            max_score = std::max(max_score,
                                 removeBoxes(std::move(current), memory)
                                     + square(groups[i + 1]));
        }
        memory[groups] = max_score;
        return max_score;
    }
}

int removeBoxes(std::vector<int> boxes)
{
#if 1
    return removeBoxesDP(boxes);
#elif 0
    const int n = boxes.size();
    std::string groups;
    int previous = std::numeric_limits<int>::lowest();
    char ngroups = 0;
    for (int i = 0; i < n; ++i)
    {
        if (previous != boxes[i])
        {
            ++ngroups;
            previous = boxes[i];
        }
    }
    groups.reserve(ngroups);
    ngroups = 0;
    previous = std::numeric_limits<int>::lowest();
    for (int i = 0; i < n; ++i)
    {
        if (previous != boxes[i])
        {
            ++ngroups;
            previous = boxes[i];
            groups.push_back(static_cast<char>(boxes[i]));
            groups.push_back(1);
        }
        else ++groups.back();
    }
#if 1
    std::unordered_map<std::string, int> memory;
    return removeBoxes(std::move(groups), memory);
#else
    return removeBoxes(std::move(groups));
#endif
#else
    const int n = boxes.size();
    std::vector<std::pair<char, char> > groups;
    int previous = std::numeric_limits<int>::lowest();
    char ngroups = 0;
    for (int i = 0; i < n; ++i)
    {
        if (previous != boxes[i])
        {
            ++ngroups;
            previous = boxes[i];
        }
    }
    groups.reserve(ngroups);
    ngroups = 0;
    previous = std::numeric_limits<int>::lowest();
    for (int i = 0; i < n; ++i)
    {
        if (previous != boxes[i])
        {
            ++ngroups;
            previous = boxes[i];
            groups.push_back({static_cast<char>(boxes[i]), 1});
        }
        else ++groups.back().second;
    }
#if 0
    std::map<std::vector<std::pair<char, char> >, int> memory;
    return removeBoxes(std::move(groups), memory);
#else
    return removeBoxes(std::move(groups));
#endif
#endif
}

void test(std::vector<int> boxes, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = removeBoxes(boxes);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    //constexpr unsigned int trials = 10'000'000;
    constexpr unsigned int trials = 100'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 3, 2, 2, 2, 3, 4, 3, 1}, 23, trials);
    test({1, 1, 1}, 9, trials);
    test({1}, 1, trials);
    test({1,2,2,1,1,1,2,1,1,2,1,2,1,1,2,2,1,1,2,2,1,1,1,2,2,2,2,1,2,1,1,2,2,1,2,1,2,2,2,2,2,1,2,1,2,2,1,1,1,2,2,1,2,1,2,2,1,2,1,1,1,2,2,2,2,2,1,2,2,2,2,2,1,1,1,1,1,2,2,2,2,2,1,1,1,1,2,2,1,1,1,1,1,1,1,2,1,2,2,1}, 2758, trials);
    return 0;
}

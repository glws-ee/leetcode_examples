#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

void setZeroes(std::vector<std::vector<int>> &matrix)
{
#if 0
    const int m = matrix.size();
    const int n = matrix[0].size();
    bool zero_row[200] = {false}, zero_col[200] = {false};
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            if (matrix[i][j] == 0)
                zero_row[i] = zero_col[j] = true;
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            if (zero_row[i] || zero_col[j])
                matrix[i][j] = 0;
#elif 1
    const int m = matrix.size();
    const int n = matrix[0].size();
    std::vector<bool> zero_row(m, false), zero_col(n, false);
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            if (matrix[i][j] == 0)
                zero_row[i] = zero_col[j] = true;
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            if (zero_row[i] || zero_col[j])
                matrix[i][j] = 0;
#else
    const int m = matrix.size();
    const int n = matrix[0].size();
    bool zero_row[m], zero_col[n];
    std::memset(zero_row, false, sizeof(zero_row));
    std::memset(zero_col, false, sizeof(zero_col));
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            if (matrix[i][j] == 0)
                zero_row[i] = zero_col[j] = true;
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            if (zero_row[i] || zero_col[j])
                matrix[i][j] = 0;
#endif
}

std::ostream& operator<<(std::ostream &out, std::vector<std::vector<int> > &matrix)
{
    out << "\n{";
    bool next_row = false;
    for (const auto &row : matrix)
    {
        if (next_row) [[likely]] out << "},\n ";
        next_row = true;
        out << '{';
        bool next_value = false;
        for (int v : row)
        {
            if (next_value) [[likely]] out << ", ";
            next_value = true;
            out << v;
        }
        out << '}';
    }
    out << '}';
    return out;
}

bool operator==(const std::vector<std::vector<int> > &left, const std::vector<std::vector<int> > &right)
{
    if (left.size() != right.size()) return false;
    const int m = left.size();
    for (int i = 0; i < m; ++i)
    {
        if (left[i].size() != right[i].size()) return false;
        const int n = left[i].size();
        for (int j = 0; j < n; ++j)
            if (left[i][j] != right[i][j]) return false;
    }
    return true;
}

void test(std::vector<std::vector<int> > matrix, std::vector<std::vector<int> > solution, unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
    {
        result = matrix;
        setZeroes(result);
    }
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 1
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 1, 1}, {1, 0, 1}, {1, 1, 1}}, {{1, 0, 1}, {0, 0, 0}, {1, 0, 1}}, trials);
    test({{0, 1, 2, 0}, {3, 4, 5, 2}, {1, 3, 1, 5}}, {{0, 0, 0, 0}, {0, 4, 5, 0}, {0, 3, 1, 0}}, trials);
    return 0;
}

#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <algorithm>

using namespace std;

std::vector<std::string> findAndReplacePattern(std::vector<std::string> &words, std::string pattern)
{
    char pattern2word[26], word2pattern[26];
    std::vector<std::string> result;
    const int n = pattern.size();
    for (const auto &w : words)
    {
        std::memset(pattern2word, 0, sizeof(pattern2word));
        std::memset(word2pattern, 0, sizeof(word2pattern));
        if (w.size() == pattern.size())
        {
            bool same = true;
            for (int i = 0; same && (i < n); ++i)
            {
                const char cw = w[i] - 'a';
                const char cp = pattern[i] - 'a';
                if (pattern2word[cp] == '\0')
                    pattern2word[cp] = w[i];
                else same = pattern2word[cp] == w[i];
                if (word2pattern[cw] == '\0')
                    word2pattern[cw] = pattern[i];
                else same = same && word2pattern[cw] == pattern[i];
            }
            if (same) result.push_back(w);
        }
    }
    return result;
}

std::ostream& operator<<(std::ostream &out, const std::vector<std::string> &vec)
{
    out << '{';
    bool next = false;
    for (const auto &v : vec)
    {
        if (next) out << ", ";
        out << v;
        next = true;
    }
    out << '}';
    return out;
}

void test(std::vector<std::string> words, std::string pattern, std::vector<std::string> solution, unsigned int trials = 1)
{
    std::cout << "INPUT: " << words << '\n';
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = findAndReplacePattern(words, pattern);
    if (solution.size() == result.size())
    {
        bool same = true;
        std::sort(solution.begin(), solution.end());
        std::sort(result.begin(), result.end());
        const int n = solution.size();
        for (int i = 0; same && (i < n); ++i)
            same = solution[i] == result[i];
        if (same) std::cout << "[SUCCESS] ";
        else std::cout << "[FAILURE] ";
    }
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"abc", "deq", "mee", "aqq", "dkd", "ccc"}, "abb", {"mee", "aqq"}, trials);
    test({"a", "b", "c"}, "a", {"a", "b", "c"}, trials);
    return 0;
}

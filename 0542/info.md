=01 Matrix=

Given an `m x n` binary matrix `mat`, return *the distance of the nearest* `0` for each cell.

The distance between two adjacent cells is `1`.

**Example 1:**<br>
*Input:* `mat = [[0, 0, 0], [0, 1, 0], [0, 0, 0]]`<br>
*Output:* `[[0, 0, 0], [0, 1, 0], [0, 0, 0]]`<br>

**Example 2:**<br>
*Input:* `mat = [[0, 0, 0], [0, 1, 0], [1, 1, 1]]`<br>
*Output:* `[[0, 0, 0], [0, 1, 0], [1, 2, 1]]`<br>

**Constrains:**<br>
- `m == mat.length`
- `n == mat[i].length`
- `1 <= m, n <= 10^4`
- `1 <= m * n <= 10^4`
- `mat[i][j]` is either `0` or `1`.
- There is at least one `0` in mat.



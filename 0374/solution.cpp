#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

constexpr int null = -1000;

static int& pick(void)
{
    static int number = 0;
    return number;
}
int guess(int num)
{
    return (num != pick()) * (2 * (num < pick()) - 1);
}

int guessNumber(int n)
{
    if (n == 1) return 1;
    long result = n / 2;
    for (long begin = 1, end = n, g = guess(result); g; g = guess(result))
    {
        if (g < 0) end = result - 1;
        else begin = result + 1;
        result = (begin + end) / 2;
    }
    return result;
}

void test(int n, int solution, unsigned int trials = 1)
{
    int result;
    pick() = solution;
    for (unsigned int i = 0; i < trials; ++i)
        result = guessNumber(n);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test(10, 6, trials);
    test(1, 1, trials);
    test(2, 1, trials);
    test(2, 2, trials);
    test(2126753390, 1702766719, trials);
    return 0;
}

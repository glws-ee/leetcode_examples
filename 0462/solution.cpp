#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <algorithm>

using namespace std;

int minMoves2(vector<int>& nums)
{
    std::nth_element(nums.begin(), nums.begin() + nums.size() / 2, nums.end());
    int median = nums[nums.size() / 2];
    int moves = 0;
    for (auto n : nums)
        moves += std::abs(n - median);
    return moves;
}

void test(std::vector<int> nums, int solution, unsigned int trials = 1)
{
    int result;
    for (unsigned int i = 0; i < trials; ++i)
        result = minMoves2(nums);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({1, 2, 3}, 2, trials);
    test({1, 10, 2, 9}, 16, trials);
    test({1, 0, 0, 8, 6}, 14, trials);
    return 0;
}

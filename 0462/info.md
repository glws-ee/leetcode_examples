Given an integer array `nums` of size `n`, return the *minimum number of moves required to make all array elements equal*.

In one move, you can increment or decrement an element of the array by `1`.

 

**Example 1:**<br>
*Input:* `nums = [1,2,3]`<br>
*Output:* `2`<br>
*Explanation:*<br>
Only two moves are needed (remember each move increments or decrements one element):<br>
`[1,2,3]  =>  [2,2,3]  =>  [2,2,2]`<br>

**Example 2:**<br>
*Input:* `nums = [1,10,2,9]`<br>
*Output:* `16`<br>
 
**Constraints:**<br>
    - `n == nums.length`
    - `1 <= nums.length <= 10^5`
    - `-10^9 <= nums[i] <= 10^9`

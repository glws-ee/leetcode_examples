#include <vector>
#include <cstring>
#include <iostream>
#include <set>

using namespace std;

#if 1
std::vector<std::string> generateNumbers(const char * data, int length)
{
    if (length == 0) return {};
    else if (length == 1) return { std::string(&data[0], 1) };
    else
    {
        if (data[0] == '0')
        {
            if (data[length - 1] == '0') return {};
            return {"0." + std::string(&data[1], length - 1)};
        }
        else
        {
            std::vector<std::string> result{std::string(data, length)};
            if (data[length - 1] != '0')
                for (int i = 1; i < length; ++i)
                    result.push_back(std::string(data, i) + "." + std::string(&data[i], length - i));
            return result;
        }
    }
}

std::vector<std::string> ambiguousCoordinates(std::string s)
{
    std::vector<std::string> result;
    
    char numbers[s.size()];
    int idx = 0;
    for (const auto &c : s)
        if ((c >= '0') && (c <= '9'))
            numbers[idx++] = c;
    for (int i = 1; i < idx; ++i)
        for (const auto &left : generateNumbers(&numbers[0], i))
            for (const auto &right : generateNumbers(&numbers[i], idx - i))
                result.push_back("(" + left + ", " + right + ")");
    return result;
}
#else

std::vector<std::string> generateNumbers(const char * data, int length, int right_zeros)
{
    if (length == 1) return { std::string(&data[0], 1) };
    if (length - right_zeros >= 1)
    {
        if (right_zeros > 0)
            return {std::string(data, length)};
        std::vector<std::string> result{std::string(data, length)};
        for (int i = 1; i < length; ++i)
            result.push_back(std::string(data, i) + "." + std::string(&data[i], length - i));
        return result;
    }
    else return {};
}

std::vector<std::string> ambiguousCoordinates(std::string s)
{
    std::vector<std::string> result;
    char numbers[s.size()];
    int left_zeros = 0, right_zeros = 0, idx = 0;
    
    // Gather information.
    for (const auto &c : s)
        if ((c >= '0') && (c <= '9'))
            numbers[idx++] = c;
    for (left_zeros = 0; (left_zeros < idx) && (numbers[left_zeros] == '0'); ++left_zeros);
    if (left_zeros == idx)
    {
        if (idx == 2) return {"(0, 0)"};
        else return {};
    }
    for (right_zeros = idx - 1; (right_zeros >= 0) && (numbers[right_zeros] == '0'); --right_zeros);
    right_zeros = idx - right_zeros - 1;
    int nonzero = idx - left_zeros - right_zeros;
    
    if (left_zeros > 0)
    {
        if (left_zeros > 1)
        {
            if (right_zeros == 0) // When no right zeros are present, first possible result.
                result.push_back("(0, 0." + std::string(&numbers[2], idx - 2) + ")");
            
            if (nonzero == 1)
            {
                if (right_zeros > 0) // Only possible result with a single non-zero element.
                    result.push_back("(0." + std::string(&numbers[1], idx - 2) + ", 0)");
            }
            else
            {
                // Generate multiple responses.
                for (int i = 1; i < nonzero; ++i)
                {
                    std::string left = "0." + std::string(&numbers[1], left_zeros - 1 + i);
                    for (const auto &right : generateNumbers(&numbers[left_zeros + i], right_zeros + nonzero - i, right_zeros))
                        result.push_back("(" + left + ", " + right + ")");
                }
            }
        }
        else
        {
            for (int i = 0; i <= nonzero; ++i)
            {
                std::string left = (i == 0)?("0"):("0." + std::string(&numbers[1], i));
                std::cout << left << '\n';
                for (const auto &right : generateNumbers(&numbers[i + 1], idx - i - 1, right_zeros))
                    result.push_back("(" + left + ", " + right + ")");
            }
        }
    }
    else
    {
        for (int i = 1; i < idx; ++i)
            for (const auto &left : generateNumbers(&numbers[0], i, std::max(i - nonzero, 0)))
                for (const auto &right : generateNumbers(&numbers[i], idx - i, right_zeros))
                    result.push_back("(" + left + ", " + right + ")");
    }
    
    return result;
}
#endif

std::ostream& operator<<(std::ostream &out, const std::vector<std::string> &vs)
{
    out << '{';
    bool remaining = false;
    for (const auto &v : vs)
    {
        if (remaining) out << ", ";
        out << "\"" << v << "\"";
        remaining = true;
    }
    out << '}';
    return out;
}

void test(std::string s, std::vector<std::string> solution, unsigned int trials = 1)
{
    std::vector<std::string> result;
    for (unsigned int i = 0; i < trials; ++i)
        result = ambiguousCoordinates(s);
    if (std::set<string>(result.begin(), result.end()) == std::set<string>(solution.begin(), solution.end())) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("(123)", {"(1, 23)", "(12, 3)", "(1.2, 3)", "(1, 2.3)"}, trials);
    test("(00011)", {"(0.001, 1)", "(0, 0.011)"}, trials);
    test("(0123)", {"(0, 123)", "(0, 12.3)", "(0, 1.23)", "(0.1, 23)", "(0.1, 2.3)", "(0.12, 3)"}, trials);
    test("(100)", {"(10, 0)"}, trials);
    
    test("(0001)", {"(0, 0.01)"}, trials);
    test("(000123)", {"(0.001, 23)", "(0.001, 2.3)", "(0.0012, 3)", "(0, 0.0123)"}, trials);
    test("(00012300)", {"(0.001, 2300)", "(0.0012, 300)"}, trials);
    test("(0123)", {"(0, 123)", "(0, 12.3)", "(0, 1.23)", "(0.1, 23)", "(0.1, 2.3)", "(0.12, 3)"}, trials);
    test("(100)", {"(10, 0)"}, trials);
    test("(1000)", {"(100, 0)"}, trials);
    test("(010)", {"(0, 10)", "(0.1, 0)"}, trials);
    test("(0100)", {"(0, 100)"}, trials);
    test("(01230)", {"(0, 1230)", "(0.1, 230)", "(0.12, 30)", "(0.123, 0)"}, trials);
    test("(101)", {"(10, 1)", "(1, 0.1)"}, trials);
    return 0;
}


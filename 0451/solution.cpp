#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <unordered_set>
#include "testA.hpp"

using namespace std;

constexpr int null = -1000;

std::string frequencySort(std::string s)
{
    int histogram[256];
    std::memset(histogram, 0, sizeof(histogram));
    for (char c : s)
        ++histogram[c];
    unsigned int non_zero = 0;
    for (int i = 0; i < 256; ++i)
        if (histogram[i])
            ++non_zero;
    std::pair<int, char> histogram_pairs[non_zero];
    non_zero = 0;
    for (int i = 0; i < 256; ++i)
        if (histogram[i])
            histogram_pairs[non_zero++] = {histogram[i], static_cast<char>(i)};
    std::sort(&histogram_pairs[0], &histogram_pairs[non_zero]);
    for (int i = non_zero - 1, j = 0; i >= 0; --i)
        for (int k = 0; k < histogram_pairs[i].first; ++k, ++j)
            s[j] = histogram_pairs[i].second;
    return s;
}

std::ostream& operator<<(std::ostream &out, const std::string &s)
{
    out << '\'';
    if (s.size() < 15)
        std::operator<<(out, s);
    else
    {
        for (int i = 0; i < 5; ++i) out << s[i];
        out << "...";
        for (int i = s.size() - 1; i > s.size() - 6; --i) out << s[i];
    }
    out << '\'';
    return out;
}

std::ostream& operator<<(std::ostream &out, const std::vector<std::string> &vec)
{
    out << '{';
    bool next = false;
    for (const std::string &s : vec)
    {
        if (next) [[likely]] out << ", ";
        next = true;
        out << s;
    }
    out << '}';
    return out;
}

bool operator==(const std::vector<std::string> &left, const std::string &right)
{
    std::unordered_set<std::string> lut(left.begin(), left.end());
    return lut.find(right) != lut.end();
}

void test(std::string s, std::vector<std::string> solution, unsigned int trials = 1)
{
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = frequencySort(s);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test("tree", {"eert", "eetr"}, trials);
    test("cccaaa", {"aaaccc", "cccaaa"}, trials);
    test("Aabb", {"bbAa", "bbaA"}, trials);
    test(testA::s, testA::solution, trials);
    return 0;
}



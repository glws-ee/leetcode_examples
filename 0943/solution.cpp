#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>
#include <algorithm>
#include <limits>
#include <numeric>

using namespace std;

bool wordContained(const std::string &word_a, const std::string &word_b)
{
    if (word_a.size() > word_b.size()) return wordContained(word_b, word_a);
    else
    {
        const int na = word_a.size();
        const int n = word_b.size() - na;
        for (int i = 0; i <= n; ++i)
            if (word_a == word_b.substr(i, na))
                return true;
        return false;
    }
}

bool wordOverlap(const std::string &word_a, const std::string &word_b, int &offset_a, int &offset_b)
{
    if (wordContained(word_a, word_b)) return true;
    const int na = word_a.size();
    const int nb = word_b.size();
    const int n = std::min(na, nb);
    offset_a = nb;
    offset_b = na;
    for (int k = 1; k <= n; ++k)
    {
        if (word_a.substr(na - k) == word_b.substr(0, k))
            offset_a = nb - k;
        if (word_b.substr(nb - k) == word_a.substr(0, k))
            offset_b = na - k;
    }
    return false;
}

std::string shortestSuperstring(const std::vector<std::string> &words)
{
    // Special case: single element.
    if      (words.size() == 1) return words[0];
    
    // Compute the overlapping between all pair of words.
    int n = words.size();
    int graph[n][n];
    bool overlapped[n];
    std::memset(overlapped, false, sizeof(overlapped));
    for(int i = 0; i < n; ++i) graph[i][i] = 0;
    for(int i = 0; i < n - 1; ++i)
        for(int j = i + 1; j < n; ++j)
            overlapped[(words[i].size() > words[j].size())?j:i] |= wordOverlap(words[i], words[j], graph[i][j], graph[j][i]);
    int number_to_remove = std::accumulate(overlapped, overlapped + n, 0);
    int word_idx[n];
    if (number_to_remove)
    {
        for (int i = 0, ri = 0; i < n; ++i)
        {
            if (overlapped[i]) continue;
            word_idx[ri] = i;
            for (int j = 0, rj = 0; j < n; ++j)
            {
                if (overlapped[j]) continue;
                graph[ri][rj] = graph[i][j];
                ++rj;
            }
            ++ri;
        }
        n -= number_to_remove;
    }
    else
    {
        for (int i = 0; i < n; ++i)
            word_idx[i] = i;
    }
    const int N = 1 << n;
    
    // Compute the dynamic programing values.
    int parent[N][n], dp[N][n];
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            dp[i][j] = std::numeric_limits<int>::max() / 2;
            parent[i][j] = -1;
        }
    }
    
    for (int i = 0; i < n; ++i)
        dp[1 << i][i] = words[word_idx[i]].size();
    for (int s = 1; s < N; ++s)
    {
        for (int j = 0; j < n; ++j)
        {
            const int J = 1 << j;
            if (!s & J) continue;
            int ps = s & ~J;
            for (int i = 0; i < n; ++i)
            {
                if (dp[s][j] > dp[ps][i] + graph[i][j])
                {
                    dp[s][j] = dp[ps][i] + graph[i][j];
                    parent[s][j] = i;
                }
            }
        }
    }
    
    // Build the string.
    std::string result;
    int idx = 0;
    int min_value = dp[N - 1][0];
    for (int i = 1; i < n; ++i)
    {
        if (dp[N - 1][i] < min_value)
        {
            min_value = dp[N - 1][i];
            idx = i;
        }
    }
    for (int s = N - 1; s > 0; )
    {
        int i = parent[s][idx];
        const std::string &current = words[word_idx[idx]];
        result = ((i < 0)?current:current.substr(current.size() - graph[i][idx])) + result;
        s &= ~(1 << idx);
        idx = i;
    }
    return result;
}

std::ostream& operator<<(std::ostream &out, const std::vector<std::string> &words)
{
    out << '{';
    bool remaining = false;
    for (const auto &w : words)
    {
        if (remaining) out << ", ";
        remaining = true;
        out << w;
    }
    out << '}';
    return out;
}

void test(std::vector<std::string> words, std::string solution, unsigned int trials = 1)
{
    std::cout << "INPUT: " << words << '\n';
    std::string result;
    for (unsigned int i = 0; i < trials; ++i)
        result = shortestSuperstring(words);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10000000;
    //constexpr unsigned int trials = 1000000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({"alex", "loves", "leetcode"}, "leetcodelovesalex"/*"alexlovesleetcode"*/, trials);
    test({"catg", "ctaagt", "gcta", "ttca", "atgcatc"}, "gctaagttcatgcatc", trials);
    test({"dbsh", "dsbbhs", "hdsb", "ssdb", "bshdbsd"}, "hdsbbhssdbshdbsd", trials);
    test({"001", "01101", "010"}, "0011010", trials);
    test({"geeks", "quiz", "for"}, "forquizgeeks"/*"geeksquizfor"*/, trials);
    test({"barcelona", "rcel"}, "barcelona", trials);
    test({"barcelona", "agbar"}, "agbarcelona", trials);
    test({"agbar", "barcelona"}, "agbarcelona", trials);
    test({"dbsh", "dsbbhs", "hdbs", "hdsb", "ssdb", "bshdbsd"}, "hdsbbhssdbshdbsd", trials);
    return 0;
}

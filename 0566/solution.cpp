#include <vector>
#include <cstring>
#include <iostream>
#include <stack>
#include <queue>
#include <utility>

using namespace std;

std::vector<std::vector<int> > matrixReshape(std::vector<std::vector<int>> mat, int r, int c)
{
    const int n = mat.size();
    const int m = mat[0].size();
    if (n * m != r * c) return mat;
    std::vector<std::vector<int> > result(r);
    result[0].resize(c);
    for (int i = 0, ir = 0, ic = 0; i < n; ++i)
    {
        for (int j = 0; j < m; ++j, ++ic)
        {
            if (ic == c)
            {
                ic = 0;
                ++ir;
                result[ir].resize(c);
            }
            result[ir][ic] = mat[i][j];
        }
    }
    return result;
}

bool operator==(const std::vector<std::vector<int> > &left, const std::vector<std::vector<int> > &right)
{
    if (left.size() != right.size()) return false;
    const int n = left.size();
    for (int i = 0; i < n; ++i)
    {
        if (left[i].size() != right[i].size()) return false;
        const int m = right.size();
        for (int j = 0; j < m; ++j)
            if (left[i][j] != right[i][j]) return false;
    }
    return true;
}

std::ostream& operator<<(std::ostream &out, const std::vector<std::vector<int> > &mat)
{
    out << '{';
    bool next_out = false;
    for (const auto &row : mat)
    {
        if (next_out) [[likely]] out << ", ";
        next_out = true;
        bool next_in = false;
        out << '{';
        for (int v : row)
        {
            if (next_in) [[likely]] out << ", ";
            next_in = true;
            out << v;
        }
        out << '}';
    }
    out << '}';
    return out;
}

void test(std::vector<std::vector<int> > mat, int r, int c, std::vector<std::vector<int> > solution, unsigned int trials = 1)
{
    std::vector<std::vector<int> > result;
    for (unsigned int i = 0; i < trials; ++i)
        result = matrixReshape(mat, r, c);
    if (solution == result) std::cout << "[SUCCESS] ";
    else std::cout << "[FAILURE] ";
    std::cout << "expected " << solution << " and obtained " << result << ".\n";
}

int main(int, char **)
{
#if 0
    constexpr unsigned int trials = 10'000'000;
    //constexpr unsigned int trials = 1'000'000;
#else
    constexpr unsigned int trials = 1;
#endif
    test({{1, 2}, {3, 4}}, 1, 4, {{1, 2, 3, 4}}, trials);
    test({{1, 2}, {3, 4}}, 2, 4, {{1, 2}, {3, 4}}, trials);
    return 0;
}
